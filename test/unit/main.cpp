#include <cppunit/ui/text/TestRunner.h>

#include "la/test_matexp.h"
#include "la/test_real.h"

// Register unit tests (Can this be automated with some CMake magic?)
CPPUNIT_TEST_SUITE_REGISTRATION(MatExpTest);
CPPUNIT_TEST_SUITE_REGISTRATION(RealTest);

int main()
{
  CppUnit::TextUi::TestRunner runner;
  CppUnit::TestFactoryRegistry &registry = CppUnit::TestFactoryRegistry::getRegistry();
  runner.addTest( registry.makeTest() );
  bool wasSuccessful = runner.run( "", false );
  return wasSuccessful;
}
