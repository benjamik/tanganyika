// Copyright (C) 2011 Benjamin Kehlet
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library. If not, see <http://www.gnu.org/licenses/>.
//
// First added:  2011-09-30
// Last changed: 2011-10-04
//
// Unit tests for the la functionality


#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include <tanganyika/common/real.h>
#include <tanganyika/la/HighPrecision.h>

#include <tanganyika/log/LogStream.h>

using namespace tanganyika;

class MatExpTest : public CppUnit::TestFixture
{
  CPPUNIT_TEST_SUITE(MatExpTest);
  CPPUNIT_TEST(test_basic_properties);
  CPPUNIT_TEST(test_harmonic_osc);
  CPPUNIT_TEST_SUITE_END();

public:

  // Test basic properties of the matrix exponential
  void test_basic_properties()
  {
    const tanganyika::uint n = 3;

    real I[n*n];
    real_identity(n, I);

    real A[n*n];
    real B[n*n];
    real C[n*n];
    real_zero(n*n, A);
    real_zero(n*n, B);
    real_zero(n*n, C);

    // e^(0) = I
    //HighPrecision::real_mat_exp(n, B, A);
    //CPPUNIT_ASSERT( real_near(n*n, B, I) );

    // Need to check this. The precision seems to 
    // be only very close to machine precision (~1e-15) 
    // at the moment.
    // e^(X)*e^(-X) = I
    for (int i = 0; i < n*n; i++)
    {
      real a = i + 0.5 - n/2 ;
      A[i] = a;
      B[i] = -a;
    }
    

    // C = e^A
    HighPrecision::real_mat_exp(n, C, A);
    
    // A = e^B
    HighPrecision::real_mat_exp(n, A, B);

    // Solve e^(A) X = I
    // Then X should be e^(-A)
    HighPrecision::real_solve_mat(n, n, C, B, I, real_epsilon());

    // FIXME: Some precision lost here. Need to use sqrt(epsilon)
    CPPUNIT_ASSERT( real_near(n*n, B, A, real_sqrt(real_epsilon())) );

  }

  void test_harmonic_osc()
  {
    const tanganyika::uint n = 2;

    // This is appearantly legal C++ now since the compiler can see that 
    // n is given at compile time.
    real A[n*n];
    real B[n*n];
    real x[n];
    real y0[n];
    

    // The harmonic oscillator
    // A = [0 1; -1 0]
    // Then e^(A*t)*(0, 1) = (sin(t), cos(t))

    real_zero(n*n, A);
    A[1] = -real_pi();
    A[2] = real_pi();

    HighPrecision::real_mat_exp(n, B, A);

    real_zero(n, y0);
    y0[1] = 1.0;
    
    HighPrecision::real_mat_vector_prod(n, x, B, y0);

    CPPUNIT_ASSERT( real_near(x[0], 0.0 ) );
    CPPUNIT_ASSERT( real_near(x[1], -1.0) );
  }
};
