// Copyright (C) 2011 Benjamin Kehlet
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library. If not, see <http://www.gnu.org/licenses/>.
//
// First added:  2011-09-30
// Last changed: 2011-10-04
//
// Unit tests for the la functionality


#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include <tanganyika/common/real.h>
#include <tanganyika/la/HighPrecision.h>

using namespace tanganyika;

class RealTest : public CppUnit::TestFixture
{
  CPPUNIT_TEST_SUITE(RealTest);
  CPPUNIT_TEST(test_real_vectors);
  CPPUNIT_TEST(test_real_solve);
  CPPUNIT_TEST_SUITE_END();

public:

  void test_real_vectors() 
  {

    const int n = 9;
    real a[n];
    real b[n];

    for (tanganyika::uint i = 1; i <= n; i++)
    {
      a[i-1] = i;
    }

    CPPUNIT_ASSERT(real_near(real_max_abs(n, a), 9.0));
    //CPPUNIT_ASSERT(near(real_min(n, a), 1.0));
    
    real_set(n, b, 2.0);
    real_mult(n, b, 3.0);

    real_div(n, a, 4.0);
    
    CPPUNIT_ASSERT(real_near( real_inner(n, a, b), (1.0+n)*n*3.0/4.0));
      
  }

  void test_real_solve()
  {
    const int n = 10;

    real A[n*n];
    for (int i = 0; i < n*n; i++)
      A[i] = (i-n/2)%(n-1) + i/10.0;

    real b[n];
    for (int i = 0; i < n; i++)
      b[i] = (n-i)%(n-3);

    real A_inv[n*n];
    HighPrecision::real_invert(n, A_inv, A);

    real x_1[n];
    HighPrecision::real_mat_vector_prod(n, x_1, A_inv, b);

    real x_2[n];
    HighPrecision::real_solve(n, A, x_2, b, real_epsilon());

    CPPUNIT_ASSERT(real_near(n, x_1, x_2));
  }

};
