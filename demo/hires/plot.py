# Import solution
from solution import *

# Import matplotlib
from pylab import *

# Plot only approximately t = (0, 5)
end_index = (u.shape[0]*5)/321

# Plot solution
for i in range(8) :
  figure(i)
  plot(t[:end_index], u[:end_index, i])
  xlabel('t')
  ylabel('U(t)')
  title("Hires %d" % i)

show()
