// Copyright (C) 2002-2008 Anders Logg
//
// This file is part of DOLFIN.
//
// DOLFIN is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DOLFIN is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with DOLFIN.  If not, see <http://www.gnu.org/licenses/>.
//
// Modified by Benjamin Kehlet, 2011
//
// First added:  2002
// Last changed: 2011-10-27
//
// This demo solves the harmonic oscillator on
// the time interval (0, 4*pi) and computes the
// error for a set of methods and orders.

#include <tanganyika.h>

using namespace tanganyika;

class Harmonic : public ODE
{
public:

  Harmonic() : ODE(2, 4.0 * real_pi()), e(0.0) {}

  void u0(Array<real>& u)
  {
    u[0] = 0.0;
    u[1] = 1.0;
  }

  void f(const Array<real>& u, real t, Array<real>& y)
  {
    y[0] = u[1];
    y[1] = - u[0];
  }

  void JT(const Array<real>& dx, Array<real>& dy, const Array<real>& u, const real t)
  {
    dy[0] = -dx[1];
    dy[1] = dx[0];
  }

  bool update(const Array<real>& u, real t, bool end)
  {
    if ( !end )
      return true;

    real e0 = u[0] - 0.0;
    real e1 = u[1] - 1.0;
    e = real_max(real_abs(e0), real_abs(e1));

    return true;
  }

  real error()
  {
    return e;
  }

private:

  real e;

};

int main()
{
#ifdef HAS_GMP
  set_precision(128);
#endif


  for (int q = 1; q <= 5; q++)
  {
    set_log_active(false);

    Harmonic ode;

    ODESolver solver;
    solver.parameters["method"] = "cg";
    solver.parameters["order"] = q;
    solver.parameters("timeslab")["fixed_time_step"] = true;
    solver.parameters("timeslab")["discrete_tolerance"] = real_epsilon();

    solver.solve(ode);

    set_log_active(true);
    info("cG(%d): e = %.3e", q, to_preal(ode.error()));
  }

  cout << endl;

  for (int q = 0; q <= 5; q++)
  {
    set_log_active(false);

    Harmonic ode;

    ODESolver solver;
    solver.parameters["method"] = "dg";
    solver.parameters["order"] = q;
    solver.parameters("timeslab")["fixed_time_step"] = true;
    solver.parameters("timeslab")["discrete_tolerance"] = real_epsilon();

    solver.solve(ode);

    set_log_active(true);
    info("dG(%d): e = %.3e", q, to_preal(ode.error()));
  }

  // Solve the dual
  Harmonic ode;
  ODESolver solver;
  solver.parameters["method"] = "cg";
  solver.parameters["order"] = 5;
  solver.parameters("timeslab")["fixed_time_step"] = true;
  solver.parameters("timeslab")["discrete_tolerance"] = real_epsilon();

  ODESolution u;

  solver.solve(ode, u);

  Dual d(ode, u, 4.0*real_pi());
  solver.solve(d);

  return 0;
}
