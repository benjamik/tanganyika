// Copyright (C) 2013 Benjamin Kehlet
//
// This file is part of Tanganyika.
//
// Tanganyika is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DOLFIN is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Tanganyika.  If not, see <http://www.gnu.org/licenses/>.
//
// First added:  2013-09-26
// Last changed: 2013-10-01

#include <tanganyika.h>

using namespace tanganyika;

class VanDerPol : public ODE
{
public:

  VanDerPol() 
   : ODE(2, 2000.0), mu(1000)
  {
  }

  void u0(Array<real>& u)
  {
    u[0] = 2;
    u[1] = 0;
  }

  void f(const Array<real>& u, real t, Array<real>& y)
  {
    y[0] = u[1];
    y[1] = mu*(1 - u[0]*u[0])*u[1] - u[0];
  }

  void J(const Array<real>& x, Array<real>& du, const Array<real>& u, real t)
  {
    du[0] = x[1];
    du[1] = (-2*mu*u[0]*u[1]-1)*x[0] + mu*(1-u[0]*u[0])*x[1];
  }

  void JT(const Array<real>& x, Array<real>& du, const Array<real>& u, real t)
  {
    du[0] = (-2*mu*u[0]*u[1]-1)*x[1];
    du[1] = x[0] + mu*(1-u[0]*u[0])*x[1];
  }
private :
  const real mu;
};

int main()
{
  set_log_level(PROGRESS);

  VanDerPol vdpol;
  vdpol.parameters["number_of_samples"] = 500;
  vdpol.parameters["save_solution"] = true;

  ODESolution u;

  ODESolver solver;
  solver.parameters["method"] = "cg";
  solver.parameters["order"] = 5;
  solver.parameters("timeslab")["initial_time_step"] = 0.0001;
  solver.parameters("timeslab")["fixed_time_step"] = true;
  solver.parameters("timeslab")["discrete_tolerance"] = real_epsilon();
  solver.parameters("timeslab")["nonlinear_solver"] = "newton";
  solver.parameters("timeslab")["linear_solver"] = "direct";

  solver.solve(vdpol, u);

  Dual z(vdpol, u, vdpol.endtime());
  StabilityAnalysis S(z);
  Array<real> res;
  Array<real> data(4);
  data[0] = 1.0;
  data[1] = 0.0;

  data[2] = 0.0;
  data[3] = 1.0;
  S.analyze_integral2(0, vdpol.endtime(), res, data);

  return 0;
}
