# Import solution
from solution import *


# Import matplotlib
from pylab import *

# Plot solution
figure(1)
plot(t, u[:, 0], t, u[:, 1], t, u[:, 2])
xlabel('t')
ylabel('U(t)')
title('Lorenz')

# Plot stability factors


import stability_factors


figure(2)

for i in range(stability_factors.u.shape[1]) :
  semilogy(stability_factors.t, stability_factors.u[:, i])

xlabel('$T$')
ylabel('$S_C$')
title('Computational stability factors of the Lorenz system')

show()
