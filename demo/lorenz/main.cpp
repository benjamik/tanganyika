// Copyright (C) 2003-2008 Anders Logg
//
// This file is part of DOLFIN.
//
// DOLFIN is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DOLFIN is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with DOLFIN.  If not, see <http://www.gnu.org/licenses/>.
//
// Modified by Benjamin Kehlet, 2008-2011
//
// First added:  2003-07-02
// Last changed: 2011-10-27

#include <stdio.h>
#include <tanganyika.h>
#include <tanganyika/common/real.h>

using namespace tanganyika;

class Lorenz : public ODE
{
public:

  Lorenz() : ODE(3, 50)
  {
    // Parameters
    s = 10.0;
    b = 8.0 / 3.0;
    r = 28.0;
  }

  void u0(Array<real>& u)
  {
    u[0] = 1.0;
    u[1] = 0.0;
    u[2] = 0.0;
  }

  void f(const Array<real>& u, real t, Array<real>& y)
  {
    y[0] = s*(u[1] - u[0]);
    y[1] = r*u[0] - u[1] - u[0]*u[2];
    y[2] = u[0]*u[1] - b*u[2];
  }

  void J(const Array<real>& x, Array<real>& y, const Array<real>& u, real t)
  {
    y[0] = s*(x[1] - x[0]);
    y[1] = (r - u[2])*x[0] - x[1] - u[0]*x[2];
    y[2] = u[1]*x[0] + u[0]*x[1] - b*x[2];
  }

  void JT(const Array<real>& x, Array<real>& y, const Array<real>& u, real t)
  {
    y[0] = -x[0]*s + (r-u[2])*x[1] + u[1]*x[2];
    y[1] = s*x[0] - x[1] + u[0]*x[2];
    y[2] = -u[0]*x[1] - b*x[2];
  }

private:

  // Parameters
  real s;
  real b;
  real r;

};

int main()
{
  Lorenz lorenz;
  lorenz.parameters["number_of_samples"] = 500;
  lorenz.parameters["save_solution"] = true;

  ODESolution u;

  ODESolver solver;
  solver.parameters["method"] = "cg";
  solver.parameters["order"] = 5;
  solver.parameters("timeslab")["initial_time_step"] = 0.01;
  solver.parameters("timeslab")["fixed_time_step"] = true;
  solver.parameters("timeslab")["discrete_tolerance"] = 1e-10;
  solver.parameters("timeslab")["nonlinear_solver"] = "newton";
  solver.parameters("timeslab")["linear_solver"] = "direct";

  solver.solve(lorenz, u);
  lorenz.analyze_stability_computation(u);
  

  return 0;
}
