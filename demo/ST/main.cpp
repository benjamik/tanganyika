// Copyright (C) 2011 Benjamin Kehlet
//
// This file is part of DOLFIN.
//
// DOLFIN is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DOLFIN is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with DOLFIN.  If not, see <http://www.gnu.org/licenses/>.
//
// First added:  2011-09-24
// Last changed: 2011-09-26
//
// The demo deomnstrates the computation of stability factors as function of 
// time with a non-zero right hand side of the dual.
//
// The demo also verifies the implementation of computing the stability
// factors as function of time. For the (primal) problem 
//        0 1
// u' = (-1 0)u 
//
// the dual problem is 

//       0 -1
// z' = (1  0)z + g
// With g = (1,0) and initial data z(T) = (0,1) the stability factors as
// function of time can be approximated by
// S(T) = int( sqrt(5 - 4cos(t) )

#include <tanganyika.h>
#include <tanganyika/io/PythonFile.h>

using namespace tanganyika;

class ST : public ODE
{
public:

  ST() : ODE(2, 4.0 * real_pi()) {}

  void u0(Array<real>& u)
  {
    u[0] = 1.0;
    u[1] = 0.0;
  }

  void f(const Array<real>& u, real t, Array<real>& y)
  {
    y[0] = -u[1];
    y[1] = u[0];
  }

  void JT(const Array<real>& x, Array<real>& y, const Array<real>& /* u */, const real /* t */)
  {
    y[0] =  x[1];
    y[1] = -x[0];
  }
};

int main()
{
  set_log_level(PROGRESS);

  ST ode;

  ODESolution u;

  ODESolver solver;
  solver.parameters["method"] = "cg";
  solver.parameters["order"] = 5;
  solver.parameters("timeslab")["fixed_time_step"] = true;
  solver.parameters("timeslab")["discrete_tolerance"] = real_epsilon();

  solver.solve(ode, u);

  // To set the right hand side, we need to create the dual object explicitly
  Dual dual(ode, u, ode.endtime());

  Array<real> tmp(ode.size());

  //set initial for the dual
  tmp[0] = 0.0;
  tmp[1] = 1.0;
  
  dual.set_data(tmp);

  // set the right hand side
  // Note: Data is copied into the dual object, so it is safe
  // to reuse the array object.
  tmp[0] = 1.0;
  tmp[1] = 0.0;

  dual.set_right_hand_side(tmp);

  // Create the stability analysis with our dual
  StabilityAnalysis s(dual);
  s.parameters["scale_g"] = false;
  s.analyze_integral(0, ode.endtime());

  // Compute some control points
  PythonFile outfile("stability_factor_control_points.py");
  for (real T = 0; T < ode.endtime(); T += ode.endtime()/10)
  {
    //Array<real> S(1);
    ODESolution z;
    solver.solve(dual, z, 0.0, T);
    real S = z.compute_stability_factor(T/1000);
    outfile << std::make_pair(T, S);
  }

  return 0;
}
