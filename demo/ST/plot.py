import numpy as np
import pylab

# This script plots both the computed and the exact solution


# Import the computed solution
import stability_factors

# Plot computed stability factor
pylab.figure(1)
pylab.plot(stability_factors.t, stability_factors.u[:, 0], label="Mat exp approximation")
pylab.xlabel('$T$')
pylab.ylabel('$S_C$')

# Compute and plot (almost) exact solution
# (almost since the integration is not exact)
delta_t = 0.01
t = np.arange(0.0, stability_factors.t[-1], delta_t)
z = np.sqrt(5 - 4*np.cos(t))*delta_t
S = np.add.accumulate(z)
pylab.plot(t, S, label="Almost exact solution")

# Plot the computed control points
import stability_factor_control_points
pylab.plot(stability_factor_control_points.t, stability_factor_control_points.u, 'o', label="Control points by dual solver")

pylab.legend()
pylab.show()
