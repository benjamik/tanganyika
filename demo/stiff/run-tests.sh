#!/bin/sh
#
# Run all stiff test problems.

echo Running test problem 1
./demo_stiff 1 > log-1

echo Running test problem 2
./demo_stiff 2 > log-2

echo Running test problem 3
./demo_stiff 3 > log-3

echo Running test problem 4
./demo_stiff 4 > log-4

echo Running test problem 5
./demo_stiff 5 > log-5

echo Running test problem 6
./demo_stiff 6 > log-6

echo Running test problem 7
./demo_stiff 7 > log-7

echo Running test problem 8
./demo_stiff 8 > log-8

echo Running test problem 9
./demo_stiff 9 > log-9
