// Copyright (C) 2003-2011 Anders Logg
//
// This file is part of DOLFIN.
//
// DOLFIN is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DOLFIN is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with DOLFIN.  If not, see <http://www.gnu.org/licenses/>.
//
// Thanks to Jim Tilander for many helpful hints.
//
// Modified by Ola Skavhaug, 2007, 2009.
// Modified by Garth N. Wells, 2009.
//
// First added:  2003-03-13
// Last changed: 2011-04-11

#include "log.h"

#include <tanganyika/common/SubSystemsManager.h>
#include <tanganyika/common/types.h>
#include <tanganyika/common/constants.h>
#include <tanganyika/common/Variable.h>
#include <tanganyika/parameter/Parameters.h>
#include <tanganyika/common/MPI.h>
#include <boost/scoped_array.hpp>
#include <cstdarg>
#include <cstdlib>
#include <sstream>
#include <stdarg.h>
#include <stdio.h>

using namespace tanganyika;

static boost::scoped_array<char> buffer(0);
static unsigned int buffer_size= 0;

// Buffer allocation
void allocate_buffer(std::string msg)
{
  // va_list, start, end require a char pointer of fixed size so we
  // need to allocate the buffer here. We allocate twice the size of
  // the format string and at least DOLFIN_LINELENGTH. This should be
  // ok in most cases.
  unsigned int new_size = std::max(static_cast<unsigned int>(2*msg.size()),
                                   static_cast<unsigned int>(DOLFIN_LINELENGTH));
  //static_cast<unsigned int>(DOLFIN_LINELENGTH));
  if (new_size > buffer_size)
  {
    buffer.reset(new char[new_size]);
    buffer_size = new_size;
  }
}

// Macro for parsing arguments
#define read(buffer, msg) \
  allocate_buffer(msg); \
  va_list aptr; \
  va_start(aptr, msg); \
  vsnprintf(buffer, buffer_size, msg.c_str(), aptr); \
  va_end(aptr);

//-----------------------------------------------------------------------------
void tanganyika::info(std::string msg, ...)
{
  if (!SubSystemsManager::globals().logger().is_active())
    return; // optimization
  read(buffer.get(), msg);
  SubSystemsManager::globals().logger().log(buffer.get());
}
//-----------------------------------------------------------------------------
void tanganyika::syncronized_info(std::string msg, ...)
{
  static boost::scoped_array<char> local_buffer(0);
  static unsigned int local_buffer_size= 0;

  if (!SubSystemsManager::globals().logger().is_active())
    return; // optimization


  unsigned int new_size = std::max(static_cast<unsigned int>(2*msg.size()),
                                   static_cast<unsigned int>(DOLFIN_LINELENGTH));
  //static_cast<unsigned int>(DOLFIN_LINELENGTH));
  if (new_size > local_buffer_size)
  {
    local_buffer.reset(new char[new_size]);
    local_buffer_size = new_size;
  }

  va_list aptr;
  va_start(aptr, msg);
  vsnprintf(local_buffer.get(), local_buffer_size, msg.c_str(), aptr);
  va_end(aptr);

  #ifdef HAS_MPI
  for (uint i = 0; i < tanganyika::SubSystemsManager::globals().mpi().num_processes(); ++i)
  {
    if (i == tanganyika::SubSystemsManager::globals().mpi().process_number())
      tanganyika::info("(Process %d) %s", tanganyika::SubSystemsManager::globals().mpi().process_number(), local_buffer.get());

    tanganyika::SubSystemsManager::globals().mpi().barrier();
  }
  #else
  tanganyika::info(local_buffer.get());
  #endif
}
//-----------------------------------------------------------------------------
void tanganyika::info_once(std::string msg, ...)
{

  static boost::scoped_array<char> local_buffer(0);
  static unsigned int local_buffer_size= 0;

  if (!SubSystemsManager::globals().logger().is_active())
    return; // optimization


  if (SubSystemsManager::globals().mpi().process_number() != 0)
    return;

  unsigned int new_size = std::max(static_cast<unsigned int>(2*msg.size()),
                                   static_cast<unsigned int>(DOLFIN_LINELENGTH));
  //static_cast<unsigned int>(DOLFIN_LINELENGTH));
  if (new_size > local_buffer_size)
  {
    local_buffer.reset(new char[new_size]);
    local_buffer_size = new_size;
  }

  va_list aptr;
  va_start(aptr, msg);
  vsnprintf(local_buffer.get(), local_buffer_size, msg.c_str(), aptr);
  va_end(aptr);

  tanganyika::info(local_buffer.get());
}
//-----------------------------------------------------------------------------
void tanganyika::info(const Variable& variable, bool verbose)
{
  if (!SubSystemsManager::globals().logger().is_active())
    return; // optimization
  info(variable.str(verbose));
}
//-----------------------------------------------------------------------------
void tanganyika::info(const Parameters& parameters, bool verbose)
{
  // Need separate function for Parameters since we can't make Parameters
  // a subclass of Variable (gives cyclic dependencies)

  if (!SubSystemsManager::globals().logger().is_active())
    return; // optimization
  info(parameters.str(verbose));
}
//-----------------------------------------------------------------------------
void tanganyika::warning(std::string msg, ...)
{
  if (!SubSystemsManager::globals().logger().is_active())
    return; // optimization
  read(buffer.get(), msg);
  SubSystemsManager::globals().logger().warning(buffer.get());
}
//-----------------------------------------------------------------------------
void tanganyika::error(std::string msg, ...)
{
  read(buffer.get(), msg);
  SubSystemsManager::globals().logger().error(buffer.get());
}
//-----------------------------------------------------------------------------
void tanganyika::log(int log_level, std::string msg, ...)
{
  if (!SubSystemsManager::globals().logger().is_active())
    return; // optimization
  read(buffer.get(), msg);
  SubSystemsManager::globals().logger().log(buffer.get(), log_level);
}
//-----------------------------------------------------------------------------
void tanganyika::begin(std::string msg, ...)
{
  if (!SubSystemsManager::globals().logger().is_active())
    return; // optimization
  read(buffer.get(), msg);
  SubSystemsManager::globals().logger().begin(buffer.get());
}
//-----------------------------------------------------------------------------
void tanganyika::begin(int log_level, std::string msg, ...)
{
  if (!SubSystemsManager::globals().logger().is_active()) return; // optimization
  read(buffer.get(), msg);
  SubSystemsManager::globals().logger().begin(buffer.get(), log_level);
}
//-----------------------------------------------------------------------------
void tanganyika::end()
{
  if (!SubSystemsManager::globals().logger().is_active())
    return; // optimization
  SubSystemsManager::globals().logger().end();
}
//-----------------------------------------------------------------------------
void tanganyika::set_log_active(bool active)
{
  SubSystemsManager::globals().logger().set_log_active(active);
}
//-----------------------------------------------------------------------------
void tanganyika::logging(bool active)
{
  // FIXME: Remove this function before the release of 1.0
  warning("The function 'logging' is deprecated, use 'set_log_active'.");
  SubSystemsManager::globals().logger().set_log_active(active);
}
//-----------------------------------------------------------------------------
void tanganyika::set_log_level(int level)
{
  SubSystemsManager::globals().logger().set_log_level(level);
}
//-----------------------------------------------------------------------------
void tanganyika::set_output_stream(std::ostream& out)
{
  SubSystemsManager::globals().logger().set_output_stream(out);
}
//-----------------------------------------------------------------------------
int tanganyika::get_log_level()
{
  return SubSystemsManager::globals().logger().get_log_level();
}
//-----------------------------------------------------------------------------
void tanganyika::summary(bool reset)
{
  // Optimization
  if (!SubSystemsManager::globals().logger().is_active())
    return;

  SubSystemsManager::globals().logger().summary(reset);
}
//-----------------------------------------------------------------------------
preal tanganyika::timing(std::string task, bool reset)
{
  return SubSystemsManager::globals().logger().timing(task, reset);
}
//-----------------------------------------------------------------------------
void tanganyika::check_equal(uint value,
                         uint valid_value,
                         std::string task,
                         std::string value_name)
{
  if (value == valid_value)
    return;

  std::stringstream s;
  s << "Unable to " << task
    << ". Reason: Illegal value " << value
    << " for " << value_name
    << ", should be " << valid_value
    << ".";

  error(s.str());
}
//-----------------------------------------------------------------------------
void tanganyika::__debug(std::string file, unsigned long line,
                     std::string function, std::string format, ...)
{
  read(buffer.get(), format);
  std::ostringstream ost;
  ost << file << ":" << line << " in " << function << "()";
  std::string msg = std::string(buffer.get()) + " [at " + ost.str() + "]";
  SubSystemsManager::globals().logger().__debug(msg);
}
//-----------------------------------------------------------------------------
