// Copyright (C) 2003-2011 Anders Logg and Jim Tilander
//
// This file is part of DOLFIN.
//
// DOLFIN is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DOLFIN is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with DOLFIN.  If not, see <http://www.gnu.org/licenses/>.
//
// Modified by Ola Skavhaug, 2007, 2009.
//
// First added:  2003-03-13
// Last changed: 2011-04-11

#ifndef __LOG_H
#define __LOG_H

#include <string>
#include <cassert>
#include <tanganyika/common/types.h>
#include "LogLevel.h"

namespace tanganyika
{

  class Variable;
  class Parameters;

  /// The DOLFIN log system provides the following set of functions for
  /// uniform handling of log messages, warnings and errors. In addition,
  /// macros are provided for debug messages and assertions.
  ///
  /// Only messages with a debug level higher than or equal to the current
  /// log level are printed (the default being zero). Logging may also be
  /// turned off by calling set_log_active(false).

  /// Print message
  void info(std::string msg, ...);

  void info_once(std::string msg, ...);
  void syncronized_info(std::string msg, ...);

  /// Print parameter (using output of str() method)
  void info(const Parameters& parameters, bool verbose=false);

  /// Print variable (using output of str() method)
  void info(const Variable& variable, bool verbose=false);

  /// Print warning
  void warning(std::string msg, ...);

  /// Print error message and throw an exception
  void error(std::string msg, ...);

  /// Print message at given debug level
  void log(int debug_level, std::string msg, ...);

  /// Begin task (increase indentation level)
  void begin(std::string msg, ...);

  /// Begin task (increase indentation level)
  void begin(int debug_level, std::string msg, ...);

  /// End task (decrease indentation level)
  void end();

  /// Turn logging on or off (deprecated)
  void set_log_active(bool active=true);

  /// Turn logging on or off (deprecated, will be removed)
  void logging(bool active=true);

  /// Set log level
  void set_log_level(int level);

  /// Set output stream
  void set_output_stream(std::ostream& out);

  /// Get log level
  int get_log_level();

  /// Print summary of timings and tasks, optionally clearing stored timings
  void summary(bool reset=false);

  /// Return timing (average) for given task, optionally clearing timing for task
  preal timing(std::string task, bool reset=false);

  /// Check value and print an informative error message if invalid
  void check_equal(uint value, uint valid_value, std::string task, std::string value_name);

  // Helper function for tanganyika_debug macro
  void __debug(std::string file, unsigned long line, std::string function, std::string format, ...);

}

// The following two macros are the only "functions" in DOLFIN
// named tanganyika_foo. Other functions can be placed inside the
// DOLFIN namespace and therefore don't require a prefix.

// Debug macros (with varying number of arguments)
#define tanganyika_debug(msg)                  do { tanganyika::__debug(__FILE__, __LINE__, __FUNCTION__, msg); } while (false)
#define tanganyika_debug1(msg, a0)             do { tanganyika::__debug(__FILE__, __LINE__, __FUNCTION__, msg, a0); } while (false)
#define tanganyika_debug2(msg, a0, a1)         do { tanganyika::__debug(__FILE__, __LINE__, __FUNCTION__, msg, a0, a1); } while (false)
#define tanganyika_debug3(msg, a0, a1, a2)     do { tanganyika::__debug(__FILE__, __LINE__, __FUNCTION__, msg, a0, a1, a2); } while (false)
#define tanganyika_debug4(msg, a0, a1, a2, a3) do { tanganyika::__debug(__FILE__, __LINE__, __FUNCTION__, msg, a0, a1, a2, a3); } while (false)

// Not implemented error, reporting function name and line number
#define tanganyika_not_implemented() \
  do { \
    error("The function '%s' has not been implemented (in %s line %d).", \
          __FUNCTION__, __FILE__, __LINE__); \
  } while (false)

#endif
