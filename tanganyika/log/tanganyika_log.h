#ifndef __DOLFIN_LOG_H
#define __DOLFIN_LOG_H

/// DOLFIN log interface

#include <tanganyika/log/log.h>
#include <tanganyika/log/Event.h>
#include <tanganyika/log/LogStream.h>
#include <tanganyika/log/Progress.h>
#include <tanganyika/log/Table.h>
#include <tanganyika/log/LogLevel.h>

#endif
