// Copyright (C) 2009 Benjamin Kehlet
//
// This file is part of Tanganyika
//
// Tanganyika is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Tanganyika is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Tanganyika.  If not, see <http://www.gnu.org/licenses/>.
//
// First added:  2009-07-20
// Last changed: 2013-05-15

#include "StabilityAnalysis.h"
#include "Dual.h"
#include "cGqMethod.h"
#include "TimeSlab.h"
#include "TimeStepper.h"
#include <tanganyika/log/Logger.h>
#include <tanganyika/log/Progress.h>
#include <tanganyika/log/LogStream.h>
#include <tanganyika/io/PythonFile.h>
#include <tanganyika/common/real.h>
#include <tanganyika/common/Array.h>
#include <tanganyika/common/ThreadPool.h>
#include <tanganyika/common/utils.h>
#include <tanganyika/la/HighPrecision.h>
#include <tanganyika/common/SubSystemsManager.h>
#include <tanganyika/common/utils.h>
#include <tanganyika/common/MPI.h>
#include <vector>
#include <iostream>
#include <boost/scoped_array.hpp>
#include <boost/scoped_ptr.hpp>
#include <boost/tuple/tuple.hpp>

using namespace tanganyika;

#define DEBUG_OUTPUT 0

//-----------------------------------------------------------------------------
StabilityAnalysis::StabilityAnalysis(Dual& dual) :
  dual(dual)
{
  parameters = default_parameters();
}
//-----------------------------------------------------------------------------
StabilityAnalysis::~StabilityAnalysis()
{
  // Do nothing
}
//-----------------------------------------------------------------------------
void StabilityAnalysis::analyze_integral(uint q, real endtime)
{
  // Get the initial data from the dual object
  Array<real> zT(dual.size());
  dual.u0(zT);
  analyze_integral(q, endtime, zT);
}
//-----------------------------------------------------------------------------

//  A vector that spans initial data consisting of pairs of
//    - a real which the computed integral so far
//    - a vector with is the computed solution of the dual
typedef std::vector<std::pair<real, std::vector<real> > > LocalResults;


static void store_result(PythonFile &file, const std::vector<std::pair<real, LocalResults> > &result)
{
  cout << "Storing to file" << endl;
  for(std::vector<std::pair<real, LocalResults> >::const_iterator it = result.begin(); it != result.end(); ++it)
  {
    // copy data to Array
    Array<real> data(it->second.size());

    for (tanganyika::uint i = 0; i < it->second.size(); ++i)
      data[i] = it->second[i].first;

    file << std::make_pair(it->first, RealArrayRef(data));
  }
}
//-----------------------------------------------------------------------------
static void pass_results(const std::vector<std::pair<real, LocalResults> > &result)
{
  error("Not implemented yet");
}
//-----------------------------------------------------------------------------
void StabilityAnalysis::compute_local_operators(std::vector<real> &B_collected, 
                                                std::vector<real> &C_collected, 
                                                uint n,
                                                real delta_t,
                                                uint local_start,
                                                uint local_stop,
                                                bool rhs_nonzero) const
{
  const uint substeps = parameters["substeps"];

  cout << "Compute local operators(" << local_start << ", " << local_stop << ") with " << substeps << " substeps" << endl;

  // temporary storage for A
  boost::scoped_array<real> A(new real[n*n]);
  boost::scoped_array<real> tmp(new real[n*n]);
  boost::scoped_array<real> tmp2(new real[n*n]); 


  real sub_delta_t = delta_t/substeps;

  B_collected.resize(n*n*(local_stop-local_start));
  if (rhs_nonzero)
    C_collected.resize(n * n * (local_stop-local_start));

  // Get all jacobian matrices and compute local operators
  // for each timestep
  Progress mat_progress("Computing local operators", local_stop-local_start);
  for (uint i = local_start; i < local_stop; ++i)
  {
    const real t = (i+1)*delta_t;
    const uint offset = n * n * (i-local_start);
    real *B = &B_collected[offset];
    //cout << "t = " << t << ", B offset : " << offset << endl;

    real_identity(n, tmp2.get());

    for (uint j = 0; j < substeps; ++j)
    {
      const real substep_t = t - j*sub_delta_t;
      get_A(A.get(), substep_t);

      compute_B(B, sub_delta_t, A.get());

      HighPrecision::real_mat_prod(n, tmp.get(), tmp2.get(), B);
      tmp.swap(tmp2);


      if (rhs_nonzero)
      {
	real* C = &C_collected[offset];
	compute_C(C, A.get(), B);
      }
    }
    real_set(n*n, B, tmp2.get());
    mat_progress++;
  }
}
//-----------------------------------------------------------------------------
void StabilityAnalysis::compute_local_operators_cG(std::vector<real> &B_collected, 
						   std::vector<real> &C_collected, 
						   uint n,
						   real delta_t,
						   uint local_start,
						   uint local_stop,
						   bool rhs_nonzero) const
{
  cout << "Computing local operators using cG(1)" << endl;
  
  B_collected.resize(n*n*(local_stop-local_start));
  if (rhs_nonzero)
    C_collected.resize(n * n * (local_stop-local_start));

  cGqMethod method(10);
  Parameters timeslab_parameters = TimeSlab::default_parameters();
  timeslab_parameters["fixed_time_step"] = true;
  timeslab_parameters["initial_time_step"] = delta_t;
  timeslab_parameters["discrete_tolerance"] = 1e-7;
  timeslab_parameters["nonlinear_solver"] = "newton";
  timeslab_parameters["linear_solver"] = "direct";
  TimeStepper timestepper(method, dual, false, timeslab_parameters);

  boost::scoped_array<real> initial(new real[n]);

  // Get all jacobian matrices and compute local operators
  // for each timestep
  Progress mat_progress("Computing local operators", local_stop-local_start);
  for (uint i = local_start; i < local_stop; ++i)
  {
    const real t = (i+1)*delta_t;
    const uint offset = n * n * (i-local_start);
    //cout << "t = " << t << "(dual time = " << dual.time(t) << "), B offset : " << offset << endl;

    // Populate the operator column by column by solving 
    // the dual with the unity vectors as data
    for (uint j = 0; j < n; ++j)
    {
      real_zero(n, initial.get());
      initial[j] = 1.0;

      timestepper.set_state(initial.get());

      const real  newtime = timestepper.step(dual.time(t), dual.endtime());
      timestepper.get_state(&B_collected[offset + j*n]);
    }

    mat_progress++;
  }
}
//-----------------------------------------------------------------------------
void StabilityAnalysis::analyze_integral_naive(uint q, real endtime, Array<real> &result)
{
  // Get the initial data from the dual object
  Array<real> zT(dual.size());
  dual.u0(zT);
  analyze_integral_naive(q, endtime, result, zT);
}
//-----------------------------------------------------------------------------
void StabilityAnalysis::analyze_integral_naive(uint q, real endtime, Array<real> &result, const Array<real> &data)
{
  const uint n = dual.size();

  assert(data.size() % n == 0);
  
  result.resize(data.size() / n);

  begin("Computing stability factors (naive)");

  const bool scale_g = parameters["scale_g"];

  // Compute process local interval
  const uint timesteps = static_cast<uint>(to_preal(endtime/parameters["time_step"].get_real()));

  // Adjust delta t to match endtime/timesteps
  const real delta_t = endtime/timesteps;

  // std::vectors to collect the computed matrices
  std::vector<real> b_collected;
  std::vector<real> c_collected;

  // Get right hand side of dual (constant in time)
  Array<real> g(n);
  dual.get_right_hand_side(g);
  const bool rhs_nonzero = real_max_abs(n, g.data().get()) > real_epsilon();

  // Compute local B and C matrices for all timesteps
  compute_local_operators(b_collected, 
                          c_collected,
                          n, 
                          delta_t,
                          0, 
                          timesteps,
                          rhs_nonzero);

  const real* initial_data = data.data().get();
  const uint data_size = data.size()/n;

  PythonFile file("stability_factors.py");
  Progress p("Computing stability factors", data_size*timesteps*timesteps/2);

  // Step forward in time
  for (uint i = 0; i < timesteps; i++)
  {  
    const real T = (i+1)*delta_t;

    // Scale g
    boost::scoped_array<real> g_scaled(new real[n]);
    real_set(n, g_scaled.get(), g.data().get());
    if (scale_g)
      real_div(n, g_scaled.get(), T);

    result.zero();

    // Loop over initial data
    for (uint j = 0; j < data_size; ++j)
    {
      boost::scoped_array<real> z1(new real[n]);
      real_set(n, z1.get(), &initial_data[j*n]);

      boost::scoped_array<real> z2(new real[n]);

      // Step backwards
      for (int k = i; k >= 0; --k)
      {
        const real *B = &b_collected[k*n*n];

        // Multiply matrix with initial data
        HighPrecision::real_mat_vector_prod(n, z2.get(), B, z1.get());

        // Add the right hand side stuff. Solution is now in vec_tmp_1
        if (rhs_nonzero)
        {
          // use z1 as tmp storage
          const real* C = &c_collected[k*n*n];
          HighPrecision::real_mat_vector_prod(n, z1.get(), C, g_scaled.get());
          real_add(n, z2.get(), z1.get());
        }

        // Compute norm of (differentiated) dual solution
        const real norm = real_norm(n, z2.get());

        // Add to integral (for computing L^1 norm in time)
        result[j] += norm * real_abs(delta_t);

        // Add to integral (for computing L^2 norm in time)
        //integral += norm * norm * real_abs(t-prev);

        z1.swap(z2);
	p++;
      }
    }
    file.write(T, result);
  }
  
  // This should leave the computed result from the last 
  // timestep in the result array.
}
//-----------------------------------------------------------------------------
void StabilityAnalysis::analyze_integral2(uint q, real endtime, Array<real> &result)
{
  // Get the initial data from the dual object
  Array<real> zT(dual.size());
  dual.u0(zT);
  analyze_integral2(q, endtime, result, zT);
}
//-----------------------------------------------------------------------------
void StabilityAnalysis::analyze_integral2(uint q, real endtime, Array<real> &result, const Array<real> &data)
{
  const uint n = dual.size();

  begin("Computing stability factors (2)");

  const real delta_t = parameters["time_step"].get_real();
  const bool scale_g = parameters["scale_g"];
  const uint P = SubSystemsManager::globals().mpi().num_processes();
  const uint p = SubSystemsManager::globals().mpi().process_number();

  // Compute process local interval
  const uint total_timesteps = static_cast<uint>(to_preal(endtime/delta_t));

  // Be sure to ceil the answer
  uint local_timesteps = total_timesteps/P + (total_timesteps % P ? 1 : 0);

  const int local_start = local_timesteps*p;
  const int local_stop = std::min(local_start+local_timesteps, total_timesteps);

  // Adjust local timesteps incase we' re on the last processor (where the number might be lower)
  local_timesteps = local_stop - local_start;

  info_once("Total timesteps: %u, per process: %u", total_timesteps, local_timesteps);
  syncronized_info("Start: %d, end: %d", local_start, local_stop);

  // std::deques to collect the computed matrices
  std::vector<real> b_collected;
  std::vector<real> c_collected;

  //std::vector<real> b_tmp;

  // Get right hand side of dual (constant in time)
  Array<real> g(n);
  dual.get_right_hand_side(g);
  const bool rhs_nonzero = real_max_abs(n, g.data().get()) > real_epsilon();

  //Compute local B and C matrices for all timesteps
  if (parameters["use_cg"])
    compute_local_operators_cG(b_collected, 
			       c_collected,
			       n, 
			       delta_t,
			       local_start, 
			       local_stop,
			       rhs_nonzero);
  else 
    compute_local_operators(b_collected, 
			    c_collected,
			    n, 
			    delta_t,
			    local_start, 
			    local_stop,
			    rhs_nonzero);

  // now b_collected[i] = B(t_i, t_{i+1})


  // assert(b_collected.size() == local_timesteps);
  // assert(!rhs_nonzero || c_collected.size() == local_timesteps);

  // Vectors for MPI communication
  boost::scoped_ptr<std::vector<std::pair<real, LocalResults> > > 
    results_to_be_sent(new std::vector<std::pair<real, LocalResults> >);
  results_to_be_sent->reserve(local_timesteps);

  boost::scoped_ptr<std::vector<std::pair<real, LocalResults> > > 
    received_results(new std::vector<std::pair<real, LocalResults> >);

  cout << "Preparing initial data" << endl;

  const real* initial_data = data.data().get();
  const uint data_size = data.size()/n;

  for (uint i = 0; i < local_timesteps; i++)
  {  
    const real T = (local_start+i+1)*delta_t;

    //typedef std::vector<std::pair<real, std::vector<real> > > LocalResults;
    results_to_be_sent->push_back(std::make_pair(T, LocalResults()));
    LocalResults &l = results_to_be_sent->back().second;
    for (uint j = 0; j < data_size; j++)
    {
      l.push_back(std::make_pair(0.0, std::vector<real>()));
      std::vector<real> &initial = l.back().second;
      for (uint a = 0; a < n; a++)
      {
        initial.push_back(initial_data[j*n + a]);
      }
    }
  }

  boost::scoped_array<real> z1(new real[n]);
  boost::scoped_array<real> z2(new real[n]);
  boost::scoped_array<real> g_scaled(new real[n]);

  // Compute integrals starting on local interval
  Progress progress("Computing integrals", 
		    local_timesteps*local_timesteps/2);

  cout << "Steps: " << ((local_timesteps+1)*(local_timesteps)/2) << endl;
  uint count = 0;
  
  // Loop backwards over timesteps
  // This is more cache friendly since the big matrix B is
  // constant in the inner loops.
  for (int i = local_timesteps-1; i >= 0; --i)  
  {
    if (DEBUG_OUTPUT)
      cout << "Outer loop at " << ((i+local_start)*delta_t) << endl;

    const real *B = &b_collected[i*n*n];

    // cout << "B: " << endl;
    // real_print_mat(n, B);

    // Step in time
    for (int k = local_timesteps-1; k >= i; --k)
    {
      LocalResults &lr = (*results_to_be_sent)[k].second;
      const real T = (*results_to_be_sent)[k].first;
      
      if (DEBUG_OUTPUT){cout << "Inner loop" << endl << "T = " << T << endl;}
      
      // Scale g
      if (scale_g)
      {
	real_set(n, g_scaled.get(), g.data().get());
	real_div(n, g_scaled.get(), T);
      }

      // Loop over initial data
      for (LocalResults::iterator d_iterator = lr.begin();
	   d_iterator != lr.end(); ++d_iterator)
      {
	real &integral = d_iterator->first;
	std::vector<real> &z = d_iterator->second;

	// if (DEBUG_OUTPUT)
	// {
	//   cout << "Loop over initial data" << endl;
	//   real_print_vec(n, &z[0]);
	//   cout << "Integral: " << integral << endl;
	// }

        // Multiply matrix with initial data
        HighPrecision::real_mat_vector_prod(n, z2.get(), B, &z[0]);

	//if (DEBUG_OUTPUT) {cout << "B*z = " << endl; real_print_vec(n, z2.get());}

        // Add the right hand side stuff. Solution is now in vec_tmp_1
        if (rhs_nonzero)
        {
          // use z1 as tmp storage
          const real* C = &c_collected[k*n*n];
          // cout << "C" << endl;
          // real_print_mat(n, C);

          HighPrecision::real_mat_vector_prod(n, z1.get(), C, g_scaled.get());

          // cout << "C*g_scaled" << endl;
          // real_print_vec(n, z1.get());

          real_add(n, z2.get(), z1.get());
        }

        // cout << "Result" << endl;
        // real_print_vec(n, z2.get());

        // Compute norm of (differentiated) dual solution
        const real norm = real_norm(n, z2.get());

        // Add to integral (for computing L^1 norm in time)
        integral += norm * delta_t;

	//if (DEBUG_OUTPUT){cout << "Norm: " << norm << endl << "Integral: " << integral << endl;}

        // Add to integral (for computing L^2 norm in time)
        //integral += norm * norm * real_abs(t-prev);

	// copy result back to vectors to be communicated
	real_set(n, &z[0], z2.get());
      } // end loop over initial data

      progress++;
      count++;
    } // end loop over local timesteps      
  } // end outer loop over timesteps

  cout << "Count: " << count << endl;
  cout << "Done computing integrals" << endl;
  
  // Communication
  tanganyika::MPINonblocking mpi;

  // only create the output file on the process zero
  boost::scoped_ptr<PythonFile> file(NULL);
  if (SubSystemsManager::globals().mpi().process_number() == 0)
  {
    file.reset(new PythonFile("stability_factors.py"));

    // Store to file (on process 0)
    store_result(*file, *results_to_be_sent);
  }
  else
  {
    // Pass to process_number-1
    mpi.send(*results_to_be_sent, 0, SubSystemsManager::globals().mpi().process_number()-1);
  }

  // Advance the integral through this process part of the interval
  // and pass the result (or store if process 0)
  // Store the computed result or pass on to next process
  for (uint p = SubSystemsManager::globals().mpi().num_processes()-1; p > SubSystemsManager::globals().mpi().process_number(); --p)
  {
    cout << "Receiving" << endl;
    mpi.recv(*received_results, 0, SubSystemsManager::globals().mpi().process_number()+1);
    mpi.wait_all();

    cout << "Size of received results: " << received_results->size() << endl;


    // Temporary vectors
    boost::scoped_array<real> z1(new real[n]);
    boost::scoped_array<real> z2(new real[n]);

    Progress progress("Computing integrals", received_results->size()*local_timesteps*data_size);

    // Advance integral and dual solution backwards
    for (int i = local_timesteps-1; i >= 0; --i)  
    {
      if (DEBUG_OUTPUT)
	cout << "Outer loop at " << ((i+local_start)*delta_t) << endl;

      const real *B = &b_collected[i*n*n];

      for (std::vector<std::pair<real, LocalResults> >::iterator time_iterator = received_results->begin();
	   time_iterator != received_results->end(); time_iterator++)
      {
	assert(time_iterator->second.size() == data_size);

	const real T = time_iterator->first;
	LocalResults &ls = time_iterator->second;

	// Compute and store scaled g
	boost::scoped_array<real> g_scaled(NULL);
	if (scale_g)
	{ 
	  g_scaled.reset(new real[n]);
	  real_set(n, g_scaled.get(), g.data().get());
	  real_div(n, g_scaled.get(), T);
	}

	// Loop over initial data
	for (LocalResults::iterator ls_it = ls.begin(); ls_it != ls.end(); ++ls_it)
	{
	  // Get reference to integral
	  real &integral = ls_it->first;
      
	  // Copy initial state to tmp vectors
	  //real_set(n, z1.get(), &ls_it->second[0]);

	  // Multiply matrix with initial data
	  HighPrecision::real_mat_vector_prod(n, z2.get(), B, &ls_it->second[0]);

	  // Add the right hand side stuff.
	  if (rhs_nonzero)
	  {
	    // use z1 as tmp storage
	    const real* C = &c_collected[i*n*n];
	    HighPrecision::real_mat_vector_prod(n, z1.get(), C, g_scaled.get());

	    real_add(n, z2.get(), z1.get());
	  }

	  // Compute norm of (differentiated) dual solution
	  const real norm = real_norm(n, z2.get());

	  // cout << "Norm: " << norm << endl;

	  // Add to integral (for computing L^1 norm in time)
	  integral += norm * delta_t;

	  // Add to integral (for computing L^2 norm in time)
	  //integral += norm * norm * real_abs(t-prev);

	  //z1.swap(z2);

	  // copy result back to vectors to be communicated
	  real_set(n, &ls_it->second[0], z2.get());

	} // end backward integration loop over timesteps

        progress++;
      } // end loop over initial data
    } // end forward loop over timestep

    results_to_be_sent.swap(received_results);
    received_results->clear();    

    if(SubSystemsManager::globals().mpi().process_number() == 0)
    {
      // Store to file (on process 0)      
      store_result(*file, *results_to_be_sent);
    }
    else
    {
      // TODO: Free the B and C matrices to reduce memory usage 
      // before sending the data over MPI (as this is memory 
      // consuming)
      // if (last_iteration)
      // {
      //   std::vector<real>().swap(b_collected);
      //   std::vector<real().swap(c_collected);
      // }
      // Note: std::vector::clear() is not guaranteed to free the memory

      mpi.send(*results_to_be_sent, 0, 
	       SubSystemsManager::globals().mpi().process_number()-1);
    }
  }
}
//-----------------------------------------------------------------------------
void StabilityAnalysis::analyze_integral(uint q, real endtime, const Array<real> &data)
{
  const uint n = dual.size();

  begin("Computing stability factors (1)");


  const bool scale_g = parameters["scale_g"];

  // Compute process local interval
  const uint total_timesteps = static_cast<uint>(to_preal(endtime/parameters["time_step"].get_real()));
  const real delta_t = endtime/total_timesteps;

  // Be sure to ceil the answer
  uint local_timesteps = total_timesteps/SubSystemsManager::globals().mpi().num_processes();
  if (total_timesteps % SubSystemsManager::globals().mpi().num_processes()) ++local_timesteps;

  const uint local_start = local_timesteps*SubSystemsManager::globals().mpi().process_number();
  const uint local_stop = std::min(local_start+local_timesteps, total_timesteps);

  // Adjust local timesteps incase we are on the last processor (where the number is possibly be lower)
  local_timesteps = local_stop - local_start;

  info_once("Total timesteps: %u, per process: %u", total_timesteps, local_timesteps);
  syncronized_info("Start: %d, end: %d", local_start, local_stop);

  // std::vectors to collect the computed matrices
  std::vector<real> b_collected;
  std::vector<real> c_collected;

  // Get right hand side of dual (constant in time)
  Array<real> g(n);
  dual.get_right_hand_side(g);
  const bool rhs_nonzero = real_max_abs(n, g.data().get()) > real_epsilon();

  // Compute local B and C matrices for all timesteps
  compute_local_operators(b_collected, 
                          c_collected,
                          n, 
                          delta_t,
                          local_start, 
                          local_stop,
                          rhs_nonzero);

  // now b_collected[i] = B(t_i, t_{i+1})


  // Some temporary storage
  boost::scoped_array<real> vec_tmp_1(new real[n]);
  boost::scoped_array<real> vec_tmp_2(new real[n]);
  boost::scoped_array<real> vec_tmp_3(new real[n]);
  boost::scoped_array<real> mat_tmp(new real[n*n]);

  assert(b_collected.size()/(dual.size()*dual.size()) == local_timesteps);
  assert(!rhs_nonzero || c_collected.size()/(dual.size()*dual.size()) == local_timesteps);

  // Vectors for MPI communication
  boost::scoped_ptr<std::vector<std::pair<real, LocalResults> > > 
    results_to_be_sent(new std::vector<std::pair<real, LocalResults> >);
  results_to_be_sent->reserve(local_timesteps);

  boost::scoped_ptr<std::vector<std::pair<real, LocalResults> > > 
    received_results(new std::vector<std::pair<real, LocalResults> >);

  const real* initial_data = data.data().get();
  const uint data_size = data.size()/n;

  Progress p("Computing fundamental solutions", local_timesteps*local_timesteps/2);

  // Main outer loop
  for (uint a = 0; a < local_timesteps; ++a)
  {
    const real t = (local_start+a+1)*delta_t;

    {
      real *B = &b_collected[a*n*n];
      real *C = rhs_nonzero ? &c_collected[a*n*n] : NULL;

      // Compute fundamental solutions by multiplying matrices
      // Loop over all earlier matrices.
      // TODO: Parallelize with threads
      for (uint i = 0; i < a; ++i)
      {
        real* old_B = &b_collected[i*n*n];
        real* old_C = rhs_nonzero ? &c_collected[i*n*n] : NULL;

        // compute C = e^(A_i*delta_t) * A^(-1)(e^(k*A)-I),  i = 1, 2, ..., n
        if (rhs_nonzero)
          HighPrecision::real_mat_prod(n, mat_tmp.get(), old_B, C);

        // Multiply earlier matrices from right with B
        HighPrecision::real_mat_prod_inplace(n, old_B, B);

        // Add c to all vectors
        if (rhs_nonzero)
          real_add(n*n, old_C, mat_tmp.get());

        // Note entirely correct since there is another inner loop below
        p++;
      }
    }

    // We have now computed operator from t[a] to t[0..a-1]

    // Compute the integrals for each initial data

    // Save empty result
    // typedef std::vector<std::pair<real, std::vector<real> > > LocalResults;
    results_to_be_sent->push_back(std::make_pair(t, LocalResults()));
    LocalResults &ls = results_to_be_sent->back().second;
    ls.resize(data_size);

    // Initialize all integrals to zero (and allocate memory for z)
    for (LocalResults::iterator it = ls.begin(); it != ls.end(); ++it)
    {
      it->first = 0.0;
      it->second.resize(n);
    }

    // NOTE: We're using ls.z as temporary storage for z.
    // By counting i down to 0, we will leave z(t0) when 
    // the loop terminates.
    for (int i = a; i >= 0; --i)
    {
      const real* B = &b_collected[i*n*n];
      const real* C = rhs_nonzero ? &c_collected[i*n*n] : NULL;

      // Multiply the C matrix with g(T)
      if (rhs_nonzero)
      {
        real_set(n, vec_tmp_1.get(), g.data().get());
        if (scale_g)
          real_mult(n, vec_tmp_1.get(), 1.0/t);

        HighPrecision::real_mat_vector_prod(n, vec_tmp_2.get(), C, vec_tmp_1.get()); 
      }

      // Compute integral for each given data vector
      for (uint j = 0; j < data_size; ++j)
      {
        const real *initial = &initial_data[j*n];

        real &integral = ls[j].first;
        std::vector<real> &z = ls[j].second;

        // Multiply matrix with initial data
        HighPrecision::real_mat_vector_prod(n, &z[0], B, initial);

        // Add the right hand side stuff. Solution is now in vec_tmp_1
        if (rhs_nonzero)
          real_add(n, &z[0], vec_tmp_2.get());

        // Compute norm of (differentiated) dual solution
        const real norm = real_norm(n, &z[0]);
      
        // Add to integral (for computing L^1 norm in time)
        integral += norm * real_abs(delta_t);

        // Add to integral (for computing L^2 norm in time)
        //integral += norm * norm * real_abs(t-prev);
      }
    }
  }

  tanganyika::MPINonblocking mpi;

  // only create the output file on the process zero
  boost::scoped_ptr<PythonFile> file(NULL);
  if (SubSystemsManager::globals().mpi().process_number() == 0)
    file.reset(new PythonFile("stability_factors.py"));

  if(SubSystemsManager::globals().mpi().process_number() == 0)
  {
    // Store to file (on process 0)
    store_result(*file, *results_to_be_sent);
  }
  else
  {
    // Pass to process_number-1
    mpi.send(*results_to_be_sent, 0, SubSystemsManager::globals().mpi().process_number()-1);
  }

  // Store the computed result or pass on to next process
  for (uint p = SubSystemsManager::globals().mpi().num_processes()-1; p > SubSystemsManager::globals().mpi().process_number(); --p)
  {
    cout << "Receiving" << endl;
    mpi.recv(*received_results, 0, SubSystemsManager::globals().mpi().process_number()+1);
    mpi.wait_all();

    cout << "Size of received results: " << received_results->size() << endl;
    cout << "Data size : " << data_size << endl;

    Progress progress("Computing integrals", received_results->size());

    // Compute integral
    for (std::vector<std::pair<real, LocalResults> >::iterator time_iterator = received_results->begin();
         time_iterator != received_results->end(); time_iterator++)
    {
      assert(time_iterator->second.size() == data_size);

      const real t = time_iterator->first;

      LocalResults &ls = time_iterator->second;

      // Accumulate integral for each local timestep
      for (int i = local_timesteps-1; i >= 0; --i)
      {
        const real* B = &b_collected[i*n*n];
        const real* C = rhs_nonzero ? &c_collected[i*n*n] : NULL;      

        // Compute integral for each given data vector
        for (uint j = 0; j < data_size; ++j)
        {
          const real *initial = &(ls[j].second[0]);
          real &integral = ls[j].first;

          // Multiply matrix with initial data
          HighPrecision::real_mat_vector_prod(n, vec_tmp_1.get(), B, initial);

          // Add the right hand side stuff.
          // Multiply the C matrix with g(T)
          if (rhs_nonzero)
          {
            real_set(n, vec_tmp_2.get(), g.data().get());
            if (scale_g)
              real_mult(n, vec_tmp_2.get(), 1.0/t);

            HighPrecision::real_mat_vector_prod(n, vec_tmp_3.get(), C, vec_tmp_2.get());
            
            // Compute z in vec_tmp_1
            real_add(n, vec_tmp_1.get(), vec_tmp_3.get());
          }

          // Compute norm of (differentiated) dual solution
          const real norm = real_norm(n, vec_tmp_1.get());
      
          // Add to integral (for computing L^1 norm in time)
          integral += norm * real_abs(delta_t);

          // Add to integral (for computing L^2 norm in time)
          //integral += norm * norm * real_abs(t-prev);

          // if this is the last timestep, save solution (to be passed on to next process)
          if (i == 0)
          {
            real_set(n, &(ls[j].second[0]), vec_tmp_1.get());
          }
        }
      }
      progress++;
    }

    results_to_be_sent.swap(received_results);
    received_results->clear();

    if(SubSystemsManager::globals().mpi().process_number() == 0)
    {
      // Store to file (on process 0)      
      store_result(*file, *results_to_be_sent);
    }
    else
    {
      mpi.send(*results_to_be_sent, 0, SubSystemsManager::globals().mpi().process_number()-1);
    }
  }

  if (SubSystemsManager::globals().mpi().num_processes() > 1)
    mpi.wait_all();


  // TODO
  // Update progress
  
  end();
}
//-----------------------------------------------------------------------------
void StabilityAnalysis::analyze_endpoint(real endtime)
{

  const bool scale_g = parameters["scale_g"];
  real delta_t = parameters["time_step"].get_real();
  uint n = dual.size();
  PythonFile file("stability_factors.py");

  // Saved matrix with the e^(A_i*delta_t) product
  boost::scoped_array<real> saved_product(new real[n*n]);
  real_identity(n, saved_product.get());

  // Save sum of rhs 
  boost::scoped_array<real> saved_sum(new real[n*n]);
  real_zero(n, saved_sum.get());


  // Get right hand side of dual (constant in time)
  Array<real> g(n);
  dual.get_right_hand_side(g);

  // Get initial data from dual object
  Array<real> z_T(n);
  dual.u0(z_T);

  begin("Computing stability factor");

  Progress p("Computing stability factors with respect to initial data");

  // Some temporary storage
  boost::scoped_array<real> B(new real[n*n]);
  boost::scoped_array<real> C(new real[n*n]);
  boost::scoped_array<real> tmp_mat(new real[n*n]);
  boost::scoped_array<real> tmp_vec_1(new real[n]);
  boost::scoped_array<real> tmp_vec_2(new real[n]);
  

 
  for (real t = 0.0; t < endtime; t += delta_t)
  {
    get_A(tmp_mat.get(), t);
    compute_B(B.get(), delta_t, tmp_mat.get());
    compute_C(C.get(), tmp_mat.get(), B.get());
    

    /*********************************  Compute the saved sum *************************/
    
    // compute C = e^(A_i*delta_t) * A^(-1)(e^(k*A)-I),  i = 1, 2, ..., n
    HighPrecision::real_mat_prod(n, C.get(), saved_product.get(), tmp_mat.get());

    // save the new product
    real_add(n*n, saved_sum.get(), C.get());

    /*************************** Compute the saved product *******************************/

    //Multiply from right
    HighPrecision::real_mat_prod_inplace(n, saved_product.get(), B.get());

    /*************************** Compute the next value of S(T) **************************/

    // S(t) = new_product*z(T) + saved_sum*g

    // First compute saved_sum*g (since it needs two temporary vectors)
    real_set(n, tmp_vec_1.get(), g.data().get());

    if (scale_g)
      real_mult(n, tmp_vec_1.get(), 1.0/t);

    HighPrecision::real_mat_vector_prod(n, tmp_vec_2.get(), saved_sum.get(), tmp_vec_1.get());

    // then compute saved_product*z(T)
    HighPrecision::real_mat_vector_prod(n, tmp_vec_1.get(), saved_product.get(), z_T.data().get());

    real_add(n, tmp_vec_1.get(), tmp_vec_2.get());

    file << std::pair<real, real>(t, real_norm(n, tmp_vec_1.get()));

    p = to_preal(t/endtime);

  }

  end();
}
//-----------------------------------------------------------------------------
void StabilityAnalysis::solve_dual_naive(real endtime)
{
  const uint n = dual.size();

  begin("Solving dual problem");

  const real delta_t = parameters["time_step"].get_real();
  const bool scale_g = parameters["scale_g"];

  // Compute process local interval
  const uint num_timesteps = static_cast<uint>(to_preal(endtime/delta_t));

  // Get right hand side of dual (constant in time)
  Array<real> g_scaled(n);
  dual.get_right_hand_side(g_scaled);
  const bool rhs_nonzero = real_max_abs(n, g_scaled.data().get()) > real_epsilon();

  // Scale g if requested
  if (rhs_nonzero && scale_g)
  {
    real_div(n, g_scaled.data().get(), endtime);
  }


  // Initialize state vector
  Array<real> current_z(n);
  dual.u0(current_z);

  // Some temporary storage
  boost::scoped_array<real> tmp(new real[n]);

  Progress("Computing dual solution", num_timesteps);

  PythonFile file("dual_naive.py");
  file.write(endtime, current_z);

  // Step backwards in time
  for (real t = endtime-delta_t; t >= 0; t -= delta_t)
  {
    boost::scoped_array<real> A(new real[n*n]);
    boost::scoped_array<real> B(new real[n*n]);

    get_A(A.get(), t);
    compute_B(B.get(), delta_t, A.get());

    // Multiply matrix with initial data
    HighPrecision::real_mat_vector_prod(n, tmp.get(), B.get(), current_z.data().get());

    // Add the right hand side stuff. Solution is now in tmp
    if (rhs_nonzero)
    {
      boost::scoped_array<real> C(new real[n*n]);
      compute_C(C.get(), A.get(), B.get());

      HighPrecision::real_mat_vector_prod(n, current_z.data().get(), C.get(), g_scaled.data().get());

      real_add(n, tmp.get(), current_z.data().get());
    }

    real_set(n, current_z.data().get(), tmp.get());

    file.write(t, current_z);
  } // end outer loop over timesteps
}
//-----------------------------------------------------------------------------
void StabilityAnalysis::solve_dual(real endtime)
{
  const uint n = dual.size();

  begin("Solving dual problem");

  // Compute process local interval
  const uint num_timesteps = static_cast<uint>(to_preal(endtime/parameters["time_step"].get_real()));
  const real delta_t = endtime/num_timesteps;

  const bool scale_g = parameters["scale_g"];

  // std::deques to collect the computed matrices
  std::vector<real> b_collected;
  std::vector<real> c_collected;

  // Get right hand side of dual (constant in time)
  Array<real> g_scaled(n);
  dual.get_right_hand_side(g_scaled);
  const bool rhs_nonzero = real_max_abs(n, g_scaled.data().get()) > real_epsilon();

  // Scale g if requested
  if (rhs_nonzero && scale_g)
  {
    real_div(n, g_scaled.data().get(), endtime);
  }


  //Compute local B and C matrices for all timesteps
  compute_local_operators(b_collected, 
                          c_collected,
                          n, 
                          delta_t,
                          0, 
                          num_timesteps,
                          rhs_nonzero);

  // now b_collected[i] = B(t_i, t_{i+1})

  // Initialize state vector
  Array<real> current_z(n);
  dual.u0(current_z);

  PythonFile file("dual.py");
  file.write(endtime, current_z);

  // Some temporary storage
  Array<real> tmp(n);

  Progress("Computing dual solution", num_timesteps);

  // Step backwards in time
  for (int i = num_timesteps-1; i >= 0; --i)
  {
    const uint offset = i*n*n;

    const real *B = &b_collected[offset];

    // Multiply matrix with initial data
    HighPrecision::real_mat_vector_prod(n, tmp.data().get(), B, current_z.data().get());

    // Add the right hand side stuff. Solution is now in tmp
    if (rhs_nonzero)
    {
      const real* C = &c_collected[offset];
      HighPrecision::real_mat_vector_prod(n, current_z.data().get(), C, g_scaled.data().get());

      real_add(n, tmp.data().get(), current_z.data().get());
    }

    current_z = tmp;

    file.write(i*delta_t, current_z);
  } // end outer loop over timesteps
}
//-----------------------------------------------------------------------------
void StabilityAnalysis::compute_B(real* B, const real delta_t, const real* A) const
{
  // Computes 
  // B = e^(A(t)*delta_t)

  const uint n = dual.size();

  boost::scoped_array<real> tmp(new real[n*n]);

  // Copy A into tmp
  real_set(n*n, tmp.get(), A);

  // Set tmp = A*delta_t
  real_mult(n*n, tmp.get(), delta_t);

  /// compute B = e^(JT*delta_t)
  HighPrecision::real_mat_exp(n, B, tmp.get());
}
//-----------------------------------------------------------------------------
void StabilityAnalysis::compute_C(real *C, const real *A, const real *B) const
{
  // Computes
  // C = A^(-1)*(e^(A(t)*delta_t) - I)

  const uint n = dual.size();

  // TODO: Is this allocation too expensive?
  boost::scoped_array<real> tmp(new real[n*n]);

  // Compute tmp =  e^(JT*delta_t)-I = B-I
  real_set(n*n, tmp.get(), B);

  for (uint i = 0; i < n; i++)
    tmp[i + i*n] -= 1.0;

  // Compute C = A^(-1)*tmp
  HighPrecision::real_solve_mat(n, n, A, C, tmp.get());
}
//-----------------------------------------------------------------------------
void StabilityAnalysis::get_A(real* JT, const real& t) const
{
  // NOTE: We need to flip the t axis here with time() since the dual implements
  // the reversed dual.  

  // A proper fix would be to allow the solver to step backward in time so it
  // don't need to flip the axis as this is very error prone.
  const real t_flipped = dual.time(t);

  const uint n = dual.size();

  // Note that matrices are stored column-oriented in the real_* family of functions

  Array<real> e(n);
  // Declare array to wrap the columns of JT.
  Array<real> JT_array(n, JT);
  for (uint i = 0; i < n; ++i)
  {
    // Fill out each column of A
    e.zero();
    e[i] = 1.0;
    JT_array.update(n, &JT[i*n]);

    dual.get_A(e, t_flipped, JT_array);
  }
}
//-----------------------------------------------------------------------------
// Print the aggregated operator B from timestep b to timestep a 
// (for debugging purposes)
boost::shared_array<real> StabilityAnalysis::get_B(uint a, uint b, real delta_t)
{
  const uint n = dual.size();
  boost::shared_array<real> B(new real[n*n]);
  real_identity(n, B.get());

  boost::scoped_array<real> B_tmp(new real[n*n]);
  boost::scoped_array<real> A(new real[n*n]);

  for (uint i = a; i < b; ++i)
  {
    const real t = (i+1)*delta_t;
    get_A(A.get(), t);
    compute_B(B_tmp.get(), delta_t, A.get());
    
    HighPrecision::real_mat_prod_inplace(n, B.get(), B_tmp.get());
  }
  
  return B;
}
//-----------------------------------------------------------------------------
// Print the aggregated operator C from timestep b to timestep a 
// (for debugging purposes)
boost::shared_array<real> StabilityAnalysis::get_C(uint a, uint b, real delta_t)
{
  const uint n = dual.size();
  boost::shared_array<real> C(new real[n*n]);
  real_set(n*n, C.get(), 0.0);

  for (uint i = a; i < b; ++i)
  {
    const real t = (i+1)*delta_t;
    boost::shared_array<real> B = get_B(a, i, delta_t);

    //cout << "Got B(" << a << ", " << i << ")" << endl;
    real_print_mat(n, B.get());

    boost::scoped_array<real> A(new real[n*n]);
    boost::scoped_array<real> A_inv(new real[n*n]);
    boost::scoped_array<real> A_exp(new real[n*n]);
    
    get_A(A.get(), t);

    HighPrecision::real_invert(n, A_inv.get(), A.get());

    real_mult(n*n, A.get(), delta_t);
    HighPrecision::real_mat_exp(n, A_exp.get(), A.get());

    // subtract identity
    for (uint j = 0; j < n; j++)
      A_exp[j*n+j] -= 1.0;

    // put the teporary result in A
    HighPrecision::real_mat_prod(n, A.get(), B.get(), A_inv.get());
    HighPrecision::real_mat_prod_inplace(n, A.get(), A_exp.get());

    // cout << " C_" << i << endl;
    // real_print_mat(n, A.get());

    real_add(n*n, C.get(), A.get());
  }
  
  return C;
}
