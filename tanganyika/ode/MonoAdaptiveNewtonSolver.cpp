// Copyright (C) 2005-2009 Anders Logg
//
// This file is part of DOLFIN.
//
// DOLFIN is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DOLFIN is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with DOLFIN.  If not, see <http://www.gnu.org/licenses/>.
//
// Modified by Benjamin Kehlet, 2011
//
// First added:  2005-01-28
// Last changed: 2011-10-19

#include <tanganyika/common/real.h>
#include <tanganyika/common/constants.h>
#include <tanganyika/log/tanganyika_log.h>
#include <tanganyika/math/tanganyika_math.h>
#include <tanganyika/la/uBLASSparseMatrix.h>
#include <tanganyika/la/uBLASKrylovSolver.h>
#include <tanganyika/la/HighPrecision.h>
#include "Alloc.h"
#include "ODE.h"
#include "Method.h"
#include "MonoAdaptiveTimeSlab.h"
#include "MonoAdaptiveNewtonSolver.h"
#include <tanganyika/common/timing.h>

using namespace tanganyika;

//-----------------------------------------------------------------------------
MonoAdaptiveNewtonSolver::MonoAdaptiveNewtonSolver
(MonoAdaptiveTimeSlab& timeslab, bool implicit)
  : TimeSlabSolver(timeslab), implicit(implicit),
    piecewise(timeslab.ode.parameters["matrix_piecewise_constant"]),
    ts(timeslab), A(timeslab, implicit, piecewise), Mu0(ts.N),
    krylov(0)
{

  const uint N = timeslab.ode.size()*timeslab.method.nsize();
  b = new real[N];
  dx = new real[N];


  // Initialize product M*u0 for implicit system
//   if (implicit)
//   {
//     Mu0 = new real[ts.N];
//     real_zero(ts.N, Mu0);
//   }

  // Choose linear solver
  chooseLinearSolver();
}
//-----------------------------------------------------------------------------
MonoAdaptiveNewtonSolver::~MonoAdaptiveNewtonSolver()
{
  delete [] dx;
  delete [] b;
  delete krylov;
}
//-----------------------------------------------------------------------------
void MonoAdaptiveNewtonSolver::start()
{
  // Initialize computation of Jacobian
  A.init();

  // Precompute product M*u0
  if (implicit)
    timeslab.ode.M(ts.u0, Mu0, ts.u0, ts.starttime());
}
//-----------------------------------------------------------------------------
real MonoAdaptiveNewtonSolver::iteration(const real& tol, uint iter,
                                         const real& d0, const real& d1)
{
  const uint N = timeslab.ode.size()*timeslab.method.nsize();

  // Evaluate b = -F(x) at current x
  Feval(b);

  // Solve linear system
  if (krylov)
  {
    const preal r = to_preal(real_max_abs(N, b) + real_epsilon() );
    real_div(N, b, r);
    // FIXME!!
    //num_local_iterations += krylov->solve(A, dx, b);
    real_mult(N, dx, r);
  }
  else
  {
    // FIXME: Implement a better check
    if (d1 >= 0.5*d0)
      A.update();

    //A.matrix().solve(dx, b);
    HighPrecision::real_solve(N, A.array(), dx, b);

  }

  // Save norm of old solution
  xnorm = real_max_abs(ts.nj, ts.x);

  // Update solution x <- x + dx (note: b = -F)
  for (uint j = 0; j < ts.nj; j++)
    ts.x[j] += dx[j];

  // Return maximum increment
  return real_max_abs(N, dx);
}
//-----------------------------------------------------------------------------
tanganyika::uint MonoAdaptiveNewtonSolver::size() const
{
  return ts.nj;
}
//-----------------------------------------------------------------------------
void MonoAdaptiveNewtonSolver::Feval(real* F)
{
  if (implicit)
    FevalImplicit(F);
  else
    FevalExplicit(F);
}
//-----------------------------------------------------------------------------
void MonoAdaptiveNewtonSolver::FevalExplicit(real* F)
{
  // Compute size of time step
  const real k = ts.length();

  // Evaluate right-hand side at all quadrature points
  for (uint m = 0; m < timeslab.method.qsize(); m++)
    ts.feval(m);

  // Update the values at each stage
  for (uint n = 0; n < timeslab.method.nsize(); n++)
  {
    const uint noffset = n * ts.N;

    // Reset values to initial data
    for (uint i = 0; i < ts.N; i++)
      F[noffset + i] = ts.u0[i];

    // Add weights of right-hand side
    for (uint m = 0; m < timeslab.method.qsize(); m++)
    {
      const real tmp = k * timeslab.method.nweight(n, m);
      const uint moffset = m * ts.N;
      for (uint i = 0; i < ts.N; i++)
        F[noffset + i] += tmp * ts.fq[moffset + i];
    }
  }

  // Subtract current values
  real_sub(ts.nj, F, ts.x);
}
//-----------------------------------------------------------------------------
void MonoAdaptiveNewtonSolver::FevalImplicit(real* F)
{
  // Use vectors from Jacobian for storing multiplication
  Array<real> xx(ts.N, A.xx.data());
  Array<real> yy(ts.N, A.yy.data());

  // Compute size of time step
  const real a = ts.starttime();
  const real k = ts.length();

  // Evaluate right-hand side at all quadrature points
  for (uint m = 0; m < timeslab.method.qsize(); m++)
    ts.feval(m);

  // Update the values at each stage
  for (uint n = 0; n < timeslab.method.nsize(); n++)
  {
    const uint noffset = n * ts.N;

    // Reset values to initial data
    for (uint i = 0; i < ts.N; i++)
      F[noffset + i] = Mu0[i];

    // Add weights of right-hand side
    for (uint m = 0; m < timeslab.method.qsize(); m++)
    {
      const real tmp = k * timeslab.method.nweight(n, m);
      const uint moffset = m * ts.N;
      for (uint i = 0; i < ts.N; i++)
        F[noffset + i] += tmp * ts.fq[moffset + i];
    }
  }

  // Subtract current values
  for (uint n = 0; n < timeslab.method.nsize(); n++)
  {
    const uint noffset = n * ts.N;

    // Copy values to xx
    ts.copy(ts.x, noffset, xx.data().get(), 0, ts.N);

    // Do multiplication
    if ( piecewise )
    {
      timeslab.ode.M(xx, yy, ts.u0, a);
    }
    else
    {
      const real t = a + timeslab.method.npoint(n) * k;
      ts.copy(ts.x, noffset, ts.u);
      timeslab.ode.M(xx, yy, ts.u, t);
    }

    // Copy values from yy
    for (uint i = 0; i < ts.N; i++)
      F[noffset + i] -= yy[i];
  }
}
//-----------------------------------------------------------------------------
void MonoAdaptiveNewtonSolver::chooseLinearSolver()
{
  const std::string linear_solver = timeslab.parameters["linear_solver"];

  // First determine if we should use a direct solver
  bool direct = false;
  if ( linear_solver == "direct" )
    direct = true;
  else if ( linear_solver == "iterative" )
    direct = false;
  else if ( linear_solver == "auto" )
  {
    /*
    const uint ode_size_threshold = ode.parameters("size_threshold");
    if ( ode.size() > ode_size_threshold )
      direct = false;
    else
      direct = true;
    */

    // FIXME: Seems to be a bug (check stiff demo)
    // so we go with the iterative solver for now
    direct = false;
  }

  // Initialize linear solver
  if ( direct )
  {
    info("Using uBLAS LU (direct) solver.");
  }
  else
  {
    info("Using uBLAS Krylov solver with no preconditioning.");
    const preal ktol = timeslab.parameters["discrete_krylov_tolerance_factor"];
    const preal _tol = to_preal(tol);

    // FIXME: Check choice of tolerances
    krylov = new uBLASKrylovSolver("default", "none");
    krylov->parameters["report"] = monitor;
    krylov->parameters["relative_tolerance"] = ktol;
    //Note: Precision lost if working with GMP types
    krylov->parameters["absolute_tolerance"] = ktol*_tol;

    info("Using BiCGStab Krylov solver for matrix Jacobian.");
    // krylov_g = new KrylovSolver("bicgstab", "ilu");
    // krylov_g->parameters["report"] = monitor;
    // krylov_g->parameters["relative_tolerance"] = ktol;
    // krylov_g->parameters["absolute_tolerance"] = ktol*_tol;
  }
}
//-----------------------------------------------------------------------------
void MonoAdaptiveNewtonSolver::debug()
{
  const uint n = ts.nj;
  uBLASSparseMatrix B(n, n);
  ublas_sparse_matrix& _B = B.mat();
  real* F1 = new real[n];
  real* F2 = new real[n];
  real_zero(n, F1);
  real_zero(n, F2);

  // Iterate over the columns of B
  for (uint j = 0; j < n; j++)
  {
    const real xj = ts.x[j];
    real dx = real_max(DOLFIN_SQRT_EPS, DOLFIN_SQRT_EPS * real_abs(xj));

    ts.x[j] -= 0.5*dx;
    Feval(F1);

    ts.x[j] = xj + 0.5*dx;
    Feval(F2);

    ts.x[j] = xj;

    for (uint i = 0; i < n; i++)
    {
      real df_dx = (F1[i] - F2[i]) / dx;
      if ( real_abs(df_dx) > real_epsilon() )
        _B(i, j) = to_preal(df_dx);
    }
  }

  delete [] F1;
  delete [] F2;

  info(B);
}
//-----------------------------------------------------------------------------
