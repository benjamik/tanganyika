// Copyright (C) 2008-2015 Benjamin Kehlet
//
// This file is part of Tanganyika.
//
// DOLFIN is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DOLFIN is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with DOLFIN.  If not, see <http://www.gnu.org/licenses/>.
//
// Modified by Anders Logg, 2008.
// Modified by Garth N. Wells, 2009.
//
// First added:  2008-06-11
// Last changed: 2015-01-13

#include "ODESolution.h"
#include <tanganyika/log/Logger.h>
#include <tanganyika/common/SubSystemsManager.h>
#include <iostream>
#include <ios>
#include <fstream>
#include <iomanip>
#include <string>
#include <limits>

using namespace tanganyika;

#define DEBUG_OUTPUT 1

//-----------------------------------------------------------------------------
ODESolution::ODESolution() :
  trial(0),
  N(0),
  nodal_size(0),
  T(0.0),
  T0(std::numeric_limits<real>::max()),
  no_timeslabs(0),
  quadrature_weights(0),
  initialized(false),
  read_mode(false),
  data_on_disk(false),
  dirty(false),
  filename("odesolution"),
  buffer_index_cache(0)
{
  use_exact_interpolation = SubSystemsManager::globals().parameters()["exact_interpolation"];

  // Add starttime to data vector
  data.push_back(0);
}
//-----------------------------------------------------------------------------
ODESolution::ODESolution(std::string filename, uint number_of_files) :
  trial(0),
  N(0),
  nodal_size(0),
  T(0.0),
  T0(std::numeric_limits<real>::max()),
  no_timeslabs(0),
  initialized(false),
  data_on_disk(true),
  dirty(false),
  filename(filename),
  buffer_index_cache(0)
{
  use_exact_interpolation = SubSystemsManager::globals().parameters()["exact_interpolation"];

  std::ifstream file;
  uint timeslabs_in_file = open_and_read_header(file, 0u);
  file.close();

  //collect number of timeslabs and first t value from all files
  for (uint i=0; i < number_of_files; i++)
  {
    std::ifstream file;
    timeslabs_in_file = open_and_read_header(file, i);
    real t;
    file >> t;
    file_table.push_back( std::pair<real, uint>(t, no_timeslabs) );
    T0 = real_min(T0, t);

    no_timeslabs += timeslabs_in_file;

    // if this is the last file, read the last line to extract the
    // endtime value
    if (i == number_of_files-1)
    {
      // seek backwards from the end of the file to find the last
      // newline
      // FIXME: Is there a better way to do this? Some library function
      // doing the same as the command `tail`
      char buf[1001];
      file.seekg (0, std::ios::end);

      // Note: Use long to be able to handle (fairly) big files.
      //unsigned long pos = file.tellg();
      //pos -= 1001;
      file.seekg(-1001, std::ios::cur);

      while (true)
      {
        file.read(buf, 1000);
        file.seekg(-1000, std::ios::cur);
        buf[1000] = '\0';
        

        std::string buf_string(buf);
        size_t newline_pos =  buf_string.find_last_of("\n");

        // Check if we found a newline
        if (newline_pos != std::string::npos)
        {
          // Read a and k from the timeslab
          file.seekg(newline_pos, std::ios::cur);
          real max_a;
          real k;
          file >> max_a;
          file >> k;

          T = max_a + k;
          break;
        }
        else
        {
          file.seekg(-1000, std::ios::cur);
        }
      }
    }
    file.close();
  }

  // save an invalid fileno and dummy timeslabs to trigger
  // file reading on first eval()
  fileno_in_memory = file_table.size() + 1;

  // the dummy timeslab spans (-2,-1)
  //cout << "Size: " << data.size() << endl;
  data.push_back(-2);
  for (std::size_t i = 0; i < size_of_timeslab()-1; i++)
  {
    //cout << "Pushing" << endl;
    data.push_back(-1);
  }

  data.push_back(-1);
  //cout << "Size: " << data.size() << endl;

  read_mode = true;

  Array<real> dummy(csize());
  eval(endtime()/2, dummy);
}
//-----------------------------------------------------------------------------
ODESolution::~ODESolution()
{
  if (initialized)
  {
    for (uint i = 0; i < cache_size; ++i)
      delete [] cache[i].second;

    delete [] cache;
  }

  if (quadrature_weights) delete [] quadrature_weights;
  if (trial) delete trial;
}
//-----------------------------------------------------------------------------
void ODESolution::init(uint N, const Lagrange& trial_space, const real* quad_weights)
{
  if (initialized)
    error("ODESolution initialized twice");

  this->N = N;

  trial = new Lagrange(trial_space);
  nodal_size = trial->size();
  max_timeslabs = max_filesize/(real_decimal_prec()*(nodal_size*csize() + 2));

  quadrature_weights = new real[nodal_size];
  real_set(nodal_size, quadrature_weights, quad_weights);

  // Initalize cache
  cache_size = nodal_size+1;
  cache = new std::pair<real, real*>[cache_size];
  for (uint i = 0; i < cache_size; ++i)
  {
    cache[i].first = -1;
    cache[i].second = new real[N];
  }
  ringbufcounter = 0;

  initialized = true;
}
//-----------------------------------------------------------------------------
void ODESolution::add_timeslab(const real& a, const real& b, const real* nodal_values)
{
   //Public method. Does some checks and calls add_data
  if (!initialized)
    error("ODE Solution not initialized");
  if (read_mode)
    error("ODE Solution in read mode");
  assert(b-a > 0);
  assert(real_abs(T-a) < real_epsilon());

  if (data.size() > max_timeslabs)
  {
    save_to_file();
    data.clear();
    data.push_back(a);
  }

  add_data(a, b, nodal_values);

  dirty = true;
  no_timeslabs++;
  T = b;

  // TODO: This is not optimal as it will be set only on 
  // the first timeslab, but ok for now
  T0 = std::min(T0, a);
}
//-----------------------------------------------------------------------------
void ODESolution::flush()
{
  if (read_mode)
    error("Cannot flush. ODESolution already in read mode");
  if (data_on_disk)
    save_to_file();

  read_mode = true;
}
//-----------------------------------------------------------------------------
void ODESolution::eval(const real& t, Array<real>& y)
{
  if (!read_mode)
    error("Can not evaluate solution");
  if(t > T + real_epsilon())
    error("Requested t > T. t=%.5f, T=%.5f", to_preal(t), to_preal(T));

  if(t < T0 - real_epsilon())
    error("Requested t < T0. t=%.5f, T=%.5f", to_preal(t), to_preal(T0));

  bool cached = false;

  // Scan the cache
  for (uint i = 0; i < cache_size; ++i)
  {
    // Empty position, skip
    if (cache[i].first < 0)
      continue;

    // Copy values
    if (cache[i].first == t)
    {
      real_set(csize(), y.data().get(), cache[i].second);
      cached = true;
    }
  }

  if (!cached)
  {

    // Read data from disk if necesary.
    if (data_on_disk && (t <= a_in_memory()-real_epsilon() || t >= b_in_memory()+real_epsilon()))
    {
      read_file(get_file_index(t));

      assert( (data.size()-1)%size_of_timeslab() == 0 );
      assert( t >= data[0] - real_epsilon() );
      assert( t <= data[data.size()-1] + real_epsilon() );


      // set the hint to the midpoint
      buffer_index_cache = (data.size()-1)/(2*size_of_timeslab())*size_of_timeslab();
    }

    buffer_index_cache = eval_hinted(t, y, buffer_index_cache);

    // store in cache
    cache[ringbufcounter].first = t;
    real_set(csize(), cache[ringbufcounter].second, y.data().get());
    ringbufcounter = (ringbufcounter + 1) % cache_size;
  }
}
//-----------------------------------------------------------------------------
tanganyika::uint ODESolution::eval_hinted(const real& t, Array<real>& y, tanganyika::uint buffer_index_hint) const
{
  assert(t <= T+DOLFIN_EPS);
  assert(t >= T0-DOLFIN_EPS);

  // Find the right timeslab in buffer
  const tanganyika::uint timeslab_index = get_buffer_index(t, buffer_index_hint);

  assert(t >= data[timeslab_index] - real_epsilon());
  assert(t <= data[timeslab_index + size_of_timeslab()] + real_epsilon());

  const real k = data[timeslab_index+size_of_timeslab()] - data[timeslab_index];
  const real tau = (t-data[timeslab_index])/k;

  if (use_exact_interpolation)
    interpolate_exact(y, timeslab_index, tau);
  else
    interpolate_linear(y, timeslab_index, tau);

  return timeslab_index;
}
//-----------------------------------------------------------------------------
real ODESolution::compute_stability_factor(real timestep)
{
  real stability_factor = 0.0;
  uint count = 0;

  if (DEBUG_OUTPUT)
    cout << "Computing stability factor from dual solution on [0, " << endtime() << "]"<< endl;

  Array<real> tmp(N);

  real T = 0;
  Progress p("Computing stability factor");

  while (T < endtime())
  {
    eval(T, tmp);
    stability_factor += real_norm(N, tmp.data().get())*timestep;
    T += timestep;
    p = to_preal(T/endtime());
  }

  return stability_factor;
}
//-----------------------------------------------------------------------------
real ODESolution::compute_stability_factor_better()
{
  real stability_factor = 0.0;
  const real* weights = get_weights();

  // These could have been declared inside the loop, but if real is a class,
  // thene this would lead to constructors and destructors being called in every
  // iteration.
  real time_integral;
  real space_integral;

  for (std::size_t offset = 0; offset < data.size()-1; offset += size_of_timeslab())
  {
    time_integral = 0.0;

    // Loop over quadrature points
    for (std::size_t j = 0; j < nsize(); ++j)
    {
      space_integral = 0.0;

      // Loop over components
      for (std::size_t i = 0; i < csize(); ++i)
      {
        // Computing L2 norm in space
        const real point_evaluation = data[offset + i*nodal_size + j + 1];
        space_integral += point_evaluation*point_evaluation;
      }

      // Computing time integral
      time_integral += weights[j] * real_sqrt(space_integral);
    }
    assert(time_integral >= 0);

    // Add time integral over timeslab to stability factor
    const real k = data[offset+size_of_timeslab()] - data[offset];
    assert(k > 0);
    stability_factor += time_integral*k;
  }

  return stability_factor;
}
//-----------------------------------------------------------------------------
void ODESolution::interpolate_exact(Array<real>& y, std::size_t timeslab_index, const real tau) const
{
  assert(timeslab_index % size_of_timeslab() == 0);
  for (uint i = 0; i < csize(); ++i)
  {
    y[i] = 0.0;

    // Evaluate each Lagrange polynomial
    for (uint j = 0; j < nodal_size; j++)
    {
      y[i] += data[timeslab_index + i*nodal_size + j + 1] * trial->eval(j, tau);
    }
  }
}
//-----------------------------------------------------------------------------
void ODESolution::interpolate_linear(Array<real>& y, std::size_t timeslab_index, const real tau) const
{
  // Make a guess of the nodal point
  uint index_a = std::min(trial->size()-2, (uint) to_preal(tau*nsize()));

  int delta = trial->point(index_a) > tau ? -1 : 1;

  // Search for the right nodal points
  const real k = data[timeslab_index + size_of_timeslab()] - data[timeslab_index];
  while (tau < trial->point(index_a)-(real_epsilon()/k) ||
         tau > trial->point(index_a + 1)+(real_epsilon()/k))
  {
    index_a += delta;
  }

  //Do the linear interpolation
  const real a = trial->point(index_a);
  const real b = trial->point(index_a+1);

  for (uint i = 0; i < csize(); ++i)
  {
    const real y_a = data[timeslab_index + i*nodal_size + index_a + 1];
    const real y_b = data[timeslab_index + i*nodal_size + index_a + 1 +1];
    const real slobe = (y_b-y_a)/(b-a);

    y[i] = y_a + (tau-a)*slobe;
  }
}
//-----------------------------------------------------------------------------
// ODESolutionData& ODESolution::get_timeslab(uint index)
// {
//   if (index >= no_timeslabs)
//     error("Requested timeslabs %u out of range %u", index, no_timeslabs);

//   if ( data_on_disk && (index > b_index_in_memory() || index < a_index_in_memory()))
//   {
//     // Scan the cache
//     uint file_no = file_table.size()-1;
//     while (file_table[file_no].second > index) file_no--;

//     read_file(file_no);
//   }

//   assert(index-a_index_in_memory() < data.size());

//   return data[index - a_index_in_memory()];
// }
//-----------------------------------------------------------------------------
const real* ODESolution::get_weights() const
{
  return quadrature_weights;
}
//-----------------------------------------------------------------------------
void ODESolution::set_filename(std::string filename)
{
  if (data_on_disk)
    error("Filename cannot be changed after data is written to file");

  this->filename = filename;
}
//-----------------------------------------------------------------------------
void ODESolution::save_to_file()
{
  if (!dirty)
    return;

  std::stringstream f(filename, std::ios_base::app | std::ios_base::out);
  if (file_table.size() > 0)
    f << "_" << ( file_table.size());

  file_table.push_back( std::pair<real, uint> (a_in_memory(), no_timeslabs - data.size()) );

  std::ofstream file(f.str().c_str());
  if (!file.is_open())
    error("Unable to open file: %s", f.str().c_str());

  file << std::setprecision(real_decimal_prec());

  const std::size_t num_timeslabs = (data.size()-1)/size_of_timeslab();

  // write number of timeslabs, size of system, the number of nodal points and the decimal precision to the file
  file  << num_timeslabs << " " << csize() << " " << nodal_size << " " << real_decimal_prec() << std::endl;

  // write the nodal points
  for (uint i = 0; i < nodal_size; ++i)
    file << trial->point(i) << " ";
  file << std::endl;

  // write the nodal weights
  for (uint i = 0; i < nodal_size; ++i)
    file << quadrature_weights[i] << " ";
  file << std::endl;

  file << "end_of_header" << std::endl;

  std::size_t count = 0;

  // then write the timeslab data
  for (std::size_t offset = 0; offset < data.size()-1; offset += size_of_timeslab())
  {
    const real k = data[offset+size_of_timeslab()] - data[offset];
    file << std::setprecision(real_decimal_prec()) << data[offset] << " " << k << " ";
    for (uint i = 0; i < csize()*nodal_size; ++i)
      file << data[offset + i + 1] << " ";
    file << std::endl;
    count++;
  }
  file.close();

  if (count != num_timeslabs)
  {
    cout << "Warning: Inconsistent number of timeslabs written: " << num_timeslabs << " vs " << count << endl;
  }

  data_on_disk = true;
  fileno_in_memory = file_table.size()-1;
  dirty = false;

  // Note: Don't clear data from memory. Caller should take care of this
}
//-----------------------------------------------------------------------------
tanganyika::uint ODESolution::open_and_read_header(std::ifstream& file, uint filenumber)
{
  std::stringstream f(filename, std::ios_base::app | std::ios_base::out);

  if (filenumber > 0)
    f << "_" << filenumber;

  file.open(f.str().c_str());
  if (!file.is_open())
    error("Unable to read file: %s", f.str().c_str());

  uint timeslabs;
  file >> timeslabs;

  uint tmp;
  real tmp_real;

  if (initialized)
  {
    file >> tmp;
    if (tmp != csize())
      error("Wrong N size of system in file: %s", f.str().c_str());
    file >> tmp;
    if (tmp != nodal_size)
      error("Wrong nodal size in file: %s", f.str().c_str());
    file >> tmp;

    // skip nodal points and quadrature weights
    for (uint i=0; i < nodal_size*2; ++i)
      file >> tmp_real;
  }
  else
  {
    uint _N;
    file >> _N;
    file >> nodal_size;
    file >> tmp;

    // read nodal points
    Lagrange l(nodal_size-1);
    for (uint i=0; i < nodal_size; ++i)
    {
      file >> tmp_real;
      l.set(i, tmp_real);
    }

    Array<real> q_weights(nodal_size);
    for (uint i=0; i < nodal_size; ++i)
    {
      file >> q_weights[i];
    }

    init(_N, l, &q_weights[0]);
  }

  std::string marker;
  file >> marker;
  if (marker != "end_of_header")
    error("in file %s: End of header marker: %s", f.str().c_str(), marker.c_str());

  return timeslabs;
}
//-----------------------------------------------------------------------------
tanganyika::uint ODESolution::get_file_index(const real& t)
{
  //Scan the file table
  int index = file_table.size()-1;
  while (file_table[index].first > t) index--;

  assert(index >= 0);

  return static_cast<tanganyika::uint>(index);
}
//-----------------------------------------------------------------------------
tanganyika::uint ODESolution::get_buffer_index(const real& t, uint buffer_index_hint) const
{
  // Use the cached index as initial guess, since we very often evaluate
  // at points close to the previous one
  // (in the const case the cache value is initialized to the midpoint)


  // The correct file should be loaded at this point
  assert( t >= data[0] - real_epsilon() );
  assert( t <= data[data.size()-1] + real_epsilon() );


  // cout << "get_buffer_index, t= " << t << ", hint: " << buffer_index_hint <<  ", t[hint] = " << data[buffer_index_hint] << endl;
  // cout << "Size of timeslab: " << size_of_timeslab() << ", size of data: " << data.size() << endl;
  // cout << "Current data: (" << data[0] << ", " << data[data.size()-1] << ")" << endl;
  assert(buffer_index_hint % size_of_timeslab() == 0);

  uint range_start = buffer_index_hint/size_of_timeslab();
  uint range_end   = range_start;

  std::size_t count = 0;

  // Expand interval to left if needed until it includes target t
  while (data[range_start*size_of_timeslab()] > t + real_epsilon())
  {
    const uint expansion = range_end - range_start + 1;
    range_start = expansion > range_start ? 0 : range_start-expansion;
    assert(count++ < 1000);
  }

  count = 0;

  // Expand interval to right if needed until it includes target t
  while (data[(range_end+1)*size_of_timeslab()] < t - real_epsilon())
  {
    const uint expansion = range_end - range_start + 1;
    range_end = (range_end+expansion)*size_of_timeslab() >= data.size() ? (data.size()-1)/size_of_timeslab() - 1 : range_end+expansion;
    assert(count++ < 1000);
  }

  assert(data[range_start*size_of_timeslab()] <= t + real_epsilon());
  assert(data[(range_end+1)*size_of_timeslab()] >= t - real_epsilon());

  count = 0;
  while( range_end != range_start)
  {
    const uint midpoint = (range_start+range_end)/2 + 1;

    if (t >= data[midpoint*size_of_timeslab()])
      range_start = midpoint;
    else
      range_end = midpoint-1;

    assert(data[range_start*size_of_timeslab()] <= t + real_epsilon());
    assert(data[(range_end+1)*size_of_timeslab()] >= t - real_epsilon());

    // Assert termination
    ++count;
    assert(count < 1000);
  }

  //cout << "get_buffer_index endl (" << data[range_end*size_of_timeslab()] << ", " << data[(range_end+1)*size_of_timeslab()] << ")" << endl;

  return range_end*size_of_timeslab();
}
//-----------------------------------------------------------------------------
void ODESolution::read_file(uint file_number)
{
  log(PROGRESS,  "ODESolution: Reading file %d", file_number);

  data.clear();

  // Open file and read the header
  std::ifstream file;
  const uint timeslabs = open_and_read_header(file, file_number);

  data.reserve(timeslabs*size_of_timeslab()+1);

  uint count = 0;
  std::stringstream ss("Reading ODESolution file", std::ios_base::app | std::ios_base::out);
  if (file_table.size() > 1)
  {
    ss << " " << file_number;
  }

  Progress p(ss.str(), timeslabs);

  real a;
  real k;
  real b;

  Array<real> values(nodal_size*csize());

  // Read start of first timestep into data array
  file >> a;
  data.push_back(a);

  while (true)
  {
    count++;
    p++;

    file >> k;
    assert(!file.eof());

    for (uint i = 0; i < csize()*nodal_size; ++i)
    {
      file >> values[i];
      assert(!file.eof());
    }


    // Attempt to read start of next timeslab
    file >> b;

    // If we reached end of file, then add last timeslab
    // and quit
    if (file.eof())
    {
      add_data(a, a+k, &values[0]);
      break;
    }
    else
    {
      //assert(real_near(a+k, b));
      add_data(a, b, &values[0]);
      a = b;
    }
  }

  file.close();

  if (count != timeslabs)
  {
    error("File data in file %u inconsistent with header. Header said: %d, read %d",
          file_number, timeslabs, count);
  }
  fileno_in_memory = file_number;

  // Try to set the initial guess
  buffer_index_cache = buffer_index_cache > (int) data.size()/2 ? data.size()-1 : 0;

  log(PROGRESS, "  Done reading file %d", file_number);
}
//-----------------------------------------------------------------------------
void ODESolution::add_data(const real& a, const real& b, const real* nodal_values)
{
  //Private method. Called from either add_timeslab or read_file

  // Note: a is already pushed as b in previous timeslab
  //data.push_back(a);

  assert(real_near(a, data[data.size()-1]));
  assert( (data.size()-1)%size_of_timeslab() == 0 );

  for (std::size_t i = 0; i < csize(); i++)
  {
    for (std::size_t j = 0; j < nodal_size; j++)
    {
      data.push_back(nodal_values[i*nodal_size+j]);
    }
  }
  data.push_back(b);

  assert( (data.size()-1)%size_of_timeslab() == 0 );
}
//-----------------------------------------------------------------------------
std::string ODESolution::str(bool verbose) const
{
  std::stringstream s;

  if (!initialized)
    s << "ODESolution: Not initialized";
  else
  {
    if (verbose)
    {
      s << "Size = " << csize() << std::endl;
      s << "T = " << T << std::endl;
      s << "Number of nodal points = " << nodal_size << std::endl;
      s << "Nodal points: ";
      for (uint i = 0; i < nodal_size; i++)
        s << " " << trial->point(i);
      s << std::endl;
      s << "Number of timeslabs = " << (uint) data.size() << std::endl;
    }
    else
      s << "<ODESolution of size" << N << " on interval [0,"<< endtime() << "]>";
  }

  return s.str();
}
//-----------------------------------------------------------------------------
// ODESolution::iterator ODESolution::begin()
// {
//   return ODESolution::iterator(*this);
// }
// //-----------------------------------------------------------------------------
// ODESolution::iterator ODESolution::end()
// {
//   return ODESolution::iterator(*this, no_timeslabs);
// }
// //-----------------------------------------------------------------------------
