// Copyright (C) 2011 Benjamin Kehlet
//
// This file is part of Tanganyika
//
// DOLFIN is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DOLFIN is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with DOLFIN.  If not, see <http://www.gnu.org/licenses/>.
//
// First added:  2011-09-02
// Last changed: 2011-09-02


#include "ODESolutionCache.h"

using namespace tanganyika;

//-----------------------------------------------------------------------------
ODESolutionCache::ODESolutionCache(const ODESolution& solution, int cache_size)
  : solution(solution), tmp(solution.csize())
//-----------------------------------------------------------------------------
{ 

  //if (file_table.size() > 1)
  //  error("Can not do const eval() when solution is stored in more than one file");

  
  if (cache_size < 0)
    size = solution.nsize()+1;
  else 
    size = cache_size;

  cache = new std::pair<real, real*>[size];
  for (uint i = 0; i < size; ++i)
  {
    cache[i].first = -1;
    cache[i].second = new real[solution.csize()];
  }
  ringbufcounter = 0;

  buffer_index_cache = solution.size()/2;
  buffer_index_cache -= buffer_index_cache%solution.size_of_timeslab();
}


//-----------------------------------------------------------------------------
ODESolutionCache::~ODESolutionCache()
//-----------------------------------------------------------------------------
{
  for (uint i = 0; i < size; ++i)
    delete [] cache[i].second;
  
  delete [] cache;
}
//-----------------------------------------------------------------------------
const Array<real>& ODESolutionCache::eval(real t)
//-----------------------------------------------------------------------------
{
  // Scan the cache
  for (uint i = 0; i < size; ++i)
  {
    // Empty position, skip
    if (cache[i].first < 0)
      continue;

    // Copy values
    if (cache[i].first == t)
    {
      real_set(solution.csize(), tmp.data().get(), cache[i].second);
      return tmp;
    }
  }

  buffer_index_cache = solution.eval_hinted(t, tmp, buffer_index_cache);


  // store in cache
  cache[ringbufcounter].first = t;
  real_set(solution.csize(), cache[ringbufcounter].second, tmp.data().get());
  ringbufcounter = (ringbufcounter + 1) % size;
  return tmp;
}
