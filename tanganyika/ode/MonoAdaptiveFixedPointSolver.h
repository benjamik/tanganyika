// Copyright (C) 2005-2006 Anders Logg
//
// This file is part of DOLFIN.
//
// DOLFIN is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DOLFIN is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with DOLFIN.  If not, see <http://www.gnu.org/licenses/>.
//
// First added:  2005-01-28
// Last changed: 2006-08-08

#ifndef __MONO_ADAPTIVE_FIXED_POINT_SOLVER_H
#define __MONO_ADAPTIVE_FIXED_POINT_SOLVER_H

#include <tanganyika/common/types.h>
#include <tanganyika/common/real.h>
#include "TimeSlabSolver.h"
#include "MonoAdaptiveTimeSlab.h"

namespace tanganyika
{
  class MonoAdaptiveTimeSlab;

  #ifdef TANGANYIKA_THREADED_FP
  class StageUpdater
  {
   public:
    StageUpdater(uint start, uint end, real& alpha, MonoAdaptiveTimeSlab& ts)
      : start(start), end(end), alpha(alpha), ts(ts){}

    void operator()()
    {
      //std::cout << start << ", " << end << ": Updating solution" << std::endl;
      //std::cout << "TS: " << (&ts) << std::endl;

      // Get size of time step
      const real k = ts.length();

      // Update the values at each stage
      for (uint n = start; n < end; n++)
      {
        const uint noffset = n * ts.N;

        // Reset values to initial data
        for (uint i = 0; i < ts.N; i++)
          ts.x[noffset + i] += alpha*(ts.u0[i] - ts.x[noffset+i]);
      
        // Add weights of right-hand side
        for (uint m = 0; m < ts.method.qsize(); m++)
        {
          const real tmp = k * ts.method.nweight(n, m);
          const uint moffset = m * ts.N;
          for (uint i = 0; i < ts.N; i++)
            ts.x[noffset + i] += alpha*tmp*ts.fq[moffset + i];
        }
      }
    }
    
   private :
    const uint start;
    const uint end;
    real&  alpha;
    MonoAdaptiveTimeSlab& ts;
  };

  class Fevaler
  {
   public:
    Fevaler(uint start, uint end, MonoAdaptiveTimeSlab& ts)
      : start(start), end(end), ts(ts) {}

    void operator()()
    {
      for (uint m = start; m < end; m++)
      {
        ts.feval(m);
      }
    }

   private:
    const uint start;
    const uint end;
    MonoAdaptiveTimeSlab& ts;
  };
  #endif

  /// This class implements fixed-point iteration on mono-adaptive
  /// time slabs. In each iteration, the solution is updated according
  /// to the fixed-point iteration x = g(x).

  class MonoAdaptiveFixedPointSolver : public TimeSlabSolver
  {
  public:

    /// Constructor
    MonoAdaptiveFixedPointSolver(MonoAdaptiveTimeSlab& timeslab);

    /// Destructor
    ~MonoAdaptiveFixedPointSolver();

  protected:

    // Make an iteration
    real iteration(const real& tol, uint iter, const real& d0, const real& d1);

    /// Size of system
    uint size() const;

  private:

    // The time slab
    MonoAdaptiveTimeSlab& ts;

    // Old values at right end-point used to compute the increment
    real* xold;

    // Damping (alpha = 1.0 for no damping)
    real alpha;

    // Stabilization
    bool stabilize;

    // Stabilization parameters

    // Number of stabilizing iterations
    uint mi;

    // Number of ramping iterations
    uint li;

    // Ramping coefficient
    real ramp;

    // Ramping factor
    real rampfactor;

    #ifdef TANGANYIKA_THREADED_FP
    ThreadPool<StageUpdater> updater_threads;
    ThreadPool<Fevaler> feval_threads;
    friend class StageUpdater;
    #endif
  };

}

#endif
