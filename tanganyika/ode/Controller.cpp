// Copyright (C) 2005 Anders Logg
//
// This file is part of Tanganyika.
//
// Tanganyika is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Tanganyika is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Tanganyika.  If not, see <http://www.gnu.org/licenses/>.
//
// Modified by Benjamin Kehlet, 2015
//
// First added:  2005-11-02
// Last changed: 2015-03-07

#include "Controller.h"
#include <tanganyika/log/tanganyika_log.h>
#include <cmath>
#include <iostream>

using namespace tanganyika;

//-----------------------------------------------------------------------------
Controller::Controller()
{
  init(0.0, 0.0, 0, 0.0);
}
//-----------------------------------------------------------------------------
Controller::Controller(real k, real tol, uint p, real kmax)
{
  init(k, tol, p, kmax);
}
//-----------------------------------------------------------------------------
Controller::~Controller()
{
  // Do nothing
}
//-----------------------------------------------------------------------------
void Controller::init(real k, real tol, uint p, real kmax)
{
  k0 = k;
  k1 = k;
  e0 = tol;
  this->p = p;
  this->kmax = kmax;
}
//-----------------------------------------------------------------------------
void Controller::reset(real k)
{
  k0 = k;
  k1 = k;
}
//-----------------------------------------------------------------------------
real Controller::updateH0211(real e, real tol)
{
  // const preal _e   = to_preal(e);
  // const preal _tol = to_preal(tol);

  // Compute new time step
  //preal k = k1*std::pow(_tol/_e, 1.0/(2.0*p))*std::pow(_tol/e0, 1.0/(2.0*p))/std::sqrt(k1/k0);
  real k = k1*real_nth_root(tol/e, 2*p) * real_nth_root(tol/e0, 2*p) / real_sqrt(k1/k0);

  // Choose kmax if error is too small (should also catch nan or inf)
  if ( !(k <= kmax) )
    k = 2.0*k1*kmax / (k1 + kmax);

  // Update history (note that e1 == e)
  k0 = k1; k1 = k;
  e0 = e;

  return k;
}
//-----------------------------------------------------------------------------
real Controller::updateH211PI(real e, real tol)
{
  // const preal _e   = to_preal(e);
  // const preal _tol = to_preal(tol);

  // Compute new time step
  // preal k = k1*std::pow(_tol/_e, 1.0/(6.0*p))*std::pow(_tol/e0, 1.0/(6.0*p));
  real k = k1*real_nth_root(tol/e, 6*p) * real_nth_root(tol/e0, 6*p);

  // Choose kmax if error is too small (should also catch nan or inf)
  if ( !(k <= kmax) )
    k = 2.0*k1*kmax / (k1 + kmax);

  // Update history (note that e1 == e)
  k0 = k1; k1 = k;
  e0 = e;

  return k;
}
//-----------------------------------------------------------------------------
real Controller::update_simple(real e, real tol)
{
  // const preal _e   = to_preal(e);
  // const preal _tol = to_preal(tol);

  // Compute new time step
  real k = k1*real_nth_root(tol/e, p);

  // Choose kmax if error is too small (should also catch nan or inf)
  if ( !(k <= kmax) )
    k = 2.0*k1*kmax / (k1 + kmax);

  // Update history (note that e1 == e)
  k0 = k1; k1 = k;
  e0 = e;

  return k;
}
//-----------------------------------------------------------------------------
real Controller::update_harmonic(real e, real tol)
{
  // const preal _e   = to_preal(e);
  // const preal _tol = to_preal(tol);


  // Compute new time step
  real k = k1*real_nth_root(tol/e, p);

  // Choose kmax if error is too small (should also catch nan or inf)
  if ( !(k <= kmax) )
    k = 2.0*k1*kmax / (k1 + kmax);

  // Take harmonic mean value with weight
  const real w = 5.0;
  k = (1.0 + w)*k1*k / (k1 + w*k);

  // Update history (note that e1 == e)
  k0 = k1; k1 = k;
  e0 = e;

  return k;
}
//-----------------------------------------------------------------------------
real Controller::update_harmonic(real knew, real kold, real kmax)
{
  const real w = 5.0;
  return real_min(kmax, (1.0 + w)*kold*knew / (kold + w*knew));
}
//-----------------------------------------------------------------------------
