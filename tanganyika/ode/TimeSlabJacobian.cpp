// Copyright (C) 2005-2008 Anders Logg
//
// This file is part of DOLFIN.
//
// DOLFIN is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DOLFIN is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with DOLFIN.  If not, see <http://www.gnu.org/licenses/>.
//
// Modified by Benjamin Kehlet, 2011
//
// First added:  2005-01-28
// Last changed: 2011-10-19

#include "ODE.h"
#include "Method.h"
#include "TimeSlab.h"
#include "TimeSlabJacobian.h"

#include <boost/scoped_array.hpp>

using namespace tanganyika;

//-----------------------------------------------------------------------------
TimeSlabJacobian::TimeSlabJacobian(TimeSlab& timeslab)
  : ode(timeslab.ode), method(timeslab.method)
{
  const uint N = ode.size()*method.nsize();
  A_array = new real[N*N];
}
//-----------------------------------------------------------------------------
TimeSlabJacobian::~TimeSlabJacobian()
{
  delete [] A_array;
}
//-----------------------------------------------------------------------------
void TimeSlabJacobian::init()
{
  // Do nothing
}
//-----------------------------------------------------------------------------
void TimeSlabJacobian::update()
{
  log(TRACE, "Recomputing Jacobian");

  // Initialize matrix if not already done
  const uint N = method.nsize()*ode.size();
//   A.resize(N, N);
//   ej.resize(N);
//   Aj.resize(N);

  // Reset unit vector
  //ej.zero();


  boost::scoped_array<real> ej(new real[N]);
  real_zero(N, ej.get());

  // Compute columns of Jacobian
  for (uint j = 0; j < N; j++)
  {
    ej[j] = 1.0;

    // Compute product Aj = Aej
    //mult(ej, Aj);
    mult(ej.get(), &A_array[j*N]);

    // Set column of A
    //column(A.mat(), j) = Aj.vec();

    ej[j] = 0.0;
  }
}
//-----------------------------------------------------------------------------
const uBLASDenseMatrix& TimeSlabJacobian::matrix() const
{
  return A;
}
//-----------------------------------------------------------------------------
const real* TimeSlabJacobian::array() const
{
  return A_array;
}
//-----------------------------------------------------------------------------
