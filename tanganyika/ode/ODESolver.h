// Copyright (C) 2003-2009 Johan Jansson and Anders Logg
//
// This file is part of DOLFIN.
//
// DOLFIN is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DOLFIN is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with DOLFIN.  If not, see <http://www.gnu.org/licenses/>.
//
// Modified by Benjamin Kehlet 2008
//
// First added:  2003
// Last changed: 2011-10-24

#ifndef __ODE_SOLVER_H
#define __ODE_SOLVER_H

#include <tanganyika/common/Variable.h>
#include "Method.h"

#include "TimeSlab.h"

namespace tanganyika
{

  class ODE;
  class ODESolution;

  /// Solves a given ODE of the form
  ///
  ///     u'(t) = f(u(t), t) on [0, T],
  ///
  ///     u(0)  = u0,
  ///
  /// where u(t) is a vector of length N.

  class ODESolver : public Variable
  {
  public:

    /// Create ODE solver
    ODESolver();

    /// Destructor
    ~ODESolver();

    // Solve given ODE on [0, T]
    void solve(ODE& ode);

    // Solve given ODE on [0, T] and save compute solution in u
    void solve(ODE& ode, ODESolution& u);

    void solve(ODE&, real a, real b);
    void solve(ODE& ode, ODESolution& u, real a, real b);

    const Method& get_method() const { return *method; }
    

    /// Default parameter values
    static Parameters default_parameters()
    {
      Parameters p("ode_solver");

      p.add("method", "cg");
      p.add("order", 5);
      p.add("multiadaptive", false);

      // Import parameters from TimeSlab
      p.add(TimeSlab::default_parameters());
      
      return p;
    }

  private:
    Method* method;
    bool initialized;
    void init();

  };

}

#endif
