// Copyright (C) 2009 Benjamin Kehlet
//
// This file is part of DOLFIN.
//
// DOLFIN is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DOLFIN is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with DOLFIN.  If not, see <http://www.gnu.org/licenses/>.
//
// First added:  2009-07-20
// Last changed: 2013-05-15

#ifndef __STABILITYFACTORS_H
#define __STABILITYFACTORS_H

#include <tanganyika/common/types.h>
#include <tanganyika/common/real.h>
#include <tanganyika/common/Variable.h>
#include <tanganyika/common/Array.h>
#include <boost/shared_array.hpp>

namespace tanganyika
{

  /// This class computes the stabilityfactors as a function of time
  /// S(t). As the stabilityfactors are defined it should solve the dual
  /// for each timestep. However, we can take advantage of the dual
  /// being linear.

  class ODESolution;
  class ODE;
  class Dual;

  class StabilityAnalysis : public Variable
  {
  public:

    /// Constructor
    StabilityAnalysis(Dual& dual);

    /// Destructor
    ~StabilityAnalysis();

    /// Compute the integral of the q'th derivative of the dual as function of (primal) endtime T
    /// Use the data given as initial data for the dual
    void analyze_integral(uint q, real endtime);
    void analyze_integral(uint q, real endtime, const Array<real> &data);

    /// do not compute fundamental solutions
    /// optimize parallel performance by communicating operators between processes
    /// Complexity is O(N^2 * K^2 * D/2) where
    /// N is the size of the system
    /// K is the number of timesteps
    /// D is the number of initial data vectors
    /// The communication overhead increases as O(N^2)
    void analyze_integral2(uint q, real endtime, Array<real> &result);
    void analyze_integral2(uint q, real endtime,  Array<real> &result, const Array<real> &data);

    /// Mostly for debugging
    void analyze_integral_naive(uint q, real endtime, Array<real> &result);
    void analyze_integral_naive(uint q, real endtime, Array<real> &result, const Array<real> &data);

    // Print the aggregated operator B from timestep b to timestep a 
    // (for debugging purposes)
    boost::shared_array<real> get_B(uint a, uint b, real delta_t);

    // Print the aggregated operator C from timestep b to timestep a 
    // (for debugging purposes)
    boost::shared_array<real> get_C(uint a, uint b, real delta_t);

    /// Compute z(0) (the endpoint of the dual) as function of (primal) endtime T
    void analyze_endpoint(real endtime);

    /// Compute dual solution using the matexp to step
    /// Mostly for debugging
    void solve_dual(real endtime);

    /// Compute dual solution using matexp to step
    /// without precomputing the linear operators
    /// Mostly for debugging
    void solve_dual_naive(real endtime);

    /// Default parameter values
    static Parameters default_parameters()
    {
      Parameters p("stability_analysis");
      p.add("time_step", real(0.01));
      p.add("mat_exp_p", 6);
      p.add("scale_g", true);
      p.add("substeps", 10);
      p.add("use_cg", false);

      return p;
    }


  private:
    // Computes B = e^(A*delta_t)
    void compute_B(real* B, const real delta_t, const real* A) const;

    // Computes C = A^(-1)*(e^(A(t)*delta_t) - I)
    void compute_C(real* C, const real* A, const real* B) const;

    // Get A (Jacobian transposed of primal problem)
    void get_A(real* JT, const real& t) const;

    void  compute_local_operators(std::vector<real> &B, 
                                  std::vector<real> &C, 
                                  uint n,
                                  real delta_t,
                                  uint local_start,
                                  uint local_stop,
                                  bool rhs_nonzero) const;

    void compute_local_operators_cG(std::vector<real> &B_collected, 
				    std::vector<real> &C_collected, 
				    uint n,
				    real delta_t,
				    uint local_start,
				    uint local_stop,
				    bool rhs_nonzero) const;


    Dual& dual;
  };
}

#endif
