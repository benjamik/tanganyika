// Copyright (C) 2003-2009 Johan Jansson and Anders Logg
//
// This file is part of DOLFIN.
//
// DOLFIN is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DOLFIN is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with DOLFIN.  If not, see <http://www.gnu.org/licenses/>.
//
// Modified by Benjamin Kehlet 2008
//
// First added:  2003
// Last changed: 2011-03-17

#include <tanganyika/log/tanganyika_log.h>
#include <tanganyika/common/timing.h>
#include <tanganyika/parameter/GlobalParameters.h>
#include "ODE.h"
#include "TimeStepper.h"
#include "ODESolver.h"
#include "ODESolution.h"
#include "dGqMethod.h"
#include "cGqMethod.h"

using namespace tanganyika;

//------------------------------------------------------------------------
ODESolver::ODESolver() 
  : method(0), initialized(false)
{
  parameters = default_parameters();
}
//------------------------------------------------------------------------
ODESolver::~ODESolver()
{
  // Do nothing
}
//------------------------------------------------------------------------
void ODESolver::solve(ODE& ode)
{
  solve(ode, 0.0, ode.endtime());
}
//------------------------------------------------------------------------
void ODESolver::solve(ODE& ode, ODESolution& u)
{
  solve(ode, u, 0.0, ode.endtime());
}
//------------------------------------------------------------------------
void ODESolver::solve(ODE& ode, real a, real b)
{
  if (!initialized)
    init();


  begin("Solving ODE over the time interval [%g, %g]", to_preal(a), to_preal(b));

  // Start timing
  preal tt = time();

  // Solve primal problem
  TimeStepper timestepper(*method, 
                          ode, 
                          static_cast<bool>(parameters["multiadaptive"]),
                          parameters("timeslab"));

  timestepper.solve(a, b);

  // Report elapsed time
  tt = time() - tt;
  log(PROGRESS, "ODE solution computed in %.3f seconds.", tt);

  end();
}
//------------------------------------------------------------------------
void ODESolver::solve(ODE& ode, ODESolution& u, real a, real b)
{
  if (!initialized)
    init();

  begin("Solving ODE over the time interval [%g, %g]", to_preal(a), to_preal(b));

  // Start timing
  preal tt = time();

  // Solve primal problem
  TimeStepper timestepper(*method, 
                          ode, 
                          u, 
                          static_cast<bool>(parameters["multiadaptive"]),
                          parameters("timeslab"));
  
  timestepper.solve(a, b);

  u.flush();

  // Report elapsed time
  tt = time() - tt;
  log(PROGRESS, "ODE solution computed in %.3f seconds.", tt);

  end();
}
//------------------------------------------------------------------------
void ODESolver::init()
{
  assert(!initialized);

  // Choose method
  std::string m = parameters["method"];
  int q = parameters["order"];
  if ( m == "cg" || m == "mcg" )
  {
    if ( q < 1 )
      error("Minimal order is q = 1 for continuous Galerkin.");
    method = new cGqMethod(q);
  }
  else if ( m == "dg" || m == "mdg" )
  {
    if ( q < 0 )
      error("Minimal order is q = 0 for discontinuous Galerkin.");
    method = new dGqMethod(q);
  }
  else
    error("Unknown ODE method: %s", m.c_str());

  initialized = true;
}
//------------------------------------------------------------------------
