// Copyright (C) 2003-2009 Johan Jansson and Anders Logg
//
// This file is part of DOLFIN.
//
// DOLFIN is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DOLFIN is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with DOLFIN.  If not, see <http://www.gnu.org/licenses/>.
//
// Modified by Benjamin Kehlet 2008
//
// First added:  2003
// Last changed: 2009-09-08

#include <cmath>
#include <string>
#include <tanganyika/common/constants.h>
#include "ODE.h"
#include "Sample.h"
#include "MonoAdaptiveTimeSlab.h"
#include "MultiAdaptiveTimeSlab.h"
#include "TimeStepper.h"

using namespace tanganyika;

//--------------------------------------------------------------------------
TimeStepper::TimeStepper(const Method& method, 
                         ODE& ode, 
                         bool multiadaptive,
                         Parameters timeslab_parameters) :
  ode(ode),
  timeslab(0),
  file(ode.parameters["solution_file_name"]),
  t(0),
  _stopped(false),
  save_solution(ode.parameters["save_solution"]),
  adaptive_samples(ode.parameters["adaptive_samples"]),
  num_samples(ode.parameters["number_of_samples"]),
  sample_density(ode.parameters["sample_density"]),
  save_to_odesolution(false),
  method(method),
  u(0)
{

  // Create time slab
  if (multiadaptive)
  {
    timeslab = new MultiAdaptiveTimeSlab(method, ode, timeslab_parameters);
  }
  else
  {
    timeslab = new MonoAdaptiveTimeSlab(method, ode, timeslab_parameters);
  }
}
//-----------------------------------------------------------------------
TimeStepper::TimeStepper(const Method& method, 
                         ODE& ode, 
                         ODESolution& u, 
                         bool multiadaptive,
                         Parameters timeslab_parameters) :
  ode(ode),
  timeslab(0),
  file(ode.parameters["solution_file_name"]),
  t(0),
  _stopped(false),
  save_solution(ode.parameters["save_solution"]),
  adaptive_samples(ode.parameters["adaptive_samples"]),
  num_samples(ode.parameters["number_of_samples"]),
  sample_density(ode.parameters["sample_density"]),
  save_to_odesolution(true),
  method(method),
  u(&u)
{
  // Create time slab
  if (multiadaptive)
  {
    timeslab = new MultiAdaptiveTimeSlab(method, ode, timeslab_parameters);
  }
  else
  {
    timeslab = new MonoAdaptiveTimeSlab(method, ode, timeslab_parameters);
  }

  // initialize ODESolution object
  u.init(ode.size(),
         timeslab->get_trial(),
         timeslab->get_quadrature_weights());

}

//-----------------------------------------------------------------------
TimeStepper::~TimeStepper()
{
  if (timeslab)
    delete timeslab;
}
//----------------------------------------------------------------------
void TimeStepper::solve()
{
  solve(0.0, ode.endtime());
}
//----------------------------------------------------------------------
void TimeStepper::solve(real t0, real t1)
{
  begin("Time-stepping over the time interval [%g, %g]",
        to_preal(t0), to_preal(t1));

  // Progress bar
  Progress p("Timestepping");


  // Do time-stepping on [t0, t1]
  t = t0;
  while (!at_end(t, t1) && !_stopped)
  {
    // Make time step
    t = step();

    // Update progress
    p = to_preal(t / ode.endtime());
  }

  end();
}
//-------------------------------------------------------------------------
real TimeStepper::step()
{
  return step(t, ode.endtime());
}
//-------------------------------------------------------------------------
real TimeStepper::step(real t0, real t1)
{
  // FIXME: Change type of time slab if solution does not converge

  // Check if this is the first time step
  const bool first = t < real_epsilon();

  // Iterate until solution is accepted
  _stopped = false;
  while (true)
  {
    _stopped = true;
    // TODO: Make maxiter here a parameter
    for (uint convergence_attempts = 0; convergence_attempts < 5; convergence_attempts++)
    {
      // Build time slab
      t = timeslab->build(t0, t1);

      // Try to solve time slab system
      if (!timeslab->solve())
      {
        t1 = t0 + (t-t0)/2.0;
        warning("Time slab solver did not converge at t = %g. Reducing time step to %tg", to_preal(t), to_preal(t1-t0));

        //_stopped = true;
        //break;
      }
      else
      {
        _stopped = false;
        break;
      }
    }

    // Check if solution can be accepted
    if (timeslab->check(first))
      break;

    info("Rejecting time slab K = %.3e (t=%.6f), trying again.", to_preal(timeslab->length()), to_preal(t));
  }

  // Save solution
  save();

  // Update for next time slab
  if (!timeslab->shift(at_end(t, ode.endtime())))
  {
    info("ODE solver stopped on user's request at t = %g.", to_preal(t));
    _stopped = true;
  }

  return t;
}
//-----------------------------------------------------------------------------
void TimeStepper::set_state(const real* u)
{
  assert(timeslab);
  timeslab->set_state(u);
}
//-----------------------------------------------------------------------------
void TimeStepper::get_state(real* u)
{
  assert(timeslab);
  timeslab->get_state(u);
}
//-----------------------------------------------------------------------------
void TimeStepper::save()
{
  //save to ODESolution object
  if (save_to_odesolution)
    timeslab->save_solution(*u);


  // Check if we should save the solution
  if (!save_solution)
    return;

  // Choose method for saving the solution
  if (adaptive_samples)
    save_adaptive_samples();
  else
    save_fixed_samples();
}
//-----------------------------------------------------------------------------
void TimeStepper::save_fixed_samples()
{
  // Get start time and end time of time slab
  real t0 = timeslab->starttime();
  real t1 = timeslab->endtime();

  // Save initial value
  if (t0 < real_epsilon())
    save_sample(0.0);

  // Compute distance between samples
  real K = ode.endtime() / static_cast<real>(num_samples);
  real t = floor(t0 / K - 0.5) * K;

  // Save samples
  while (true)
  {
    t += K;

    if (t < t0 + real_epsilon())
      continue;

    if (t > t1 + real_epsilon())
      break;

    if (real_abs(t - t1) < real_epsilon())
      t = t1;

    save_sample(t);
  }

  // Save final value (if not saved already)
  if (at_end(t1, ode.endtime()) && !real_near(t1, file.last()))
    save_sample(ode.endtime());
}
//-----------------------------------------------------------------------------
void TimeStepper::save_adaptive_samples()
{
  // Get start time and end time of time slab
  real t0 = timeslab->starttime();
  real t1 = timeslab->endtime();

  // Save initial value
  if (t0 < real_epsilon())
    save_sample(0.0);

  // Compute distance between samples
  assert(sample_density >= 1);
  real k = (t1 - t0) / static_cast<real>(sample_density);

  // Save samples
  for (uint n = 0; n < sample_density; ++n)
  {
    real t = t0 + static_cast<real>(n + 1)*k;
    if (n == (sample_density - 1))
      t = t1;
    save_sample(t);
  }
}
//-----------------------------------------------------------------------------
void TimeStepper::save_sample(real t)
{
  // Don't save if the solution has already been saved at the time level
  // (probably due to round off errors when sample time os at the end of
  // the time step)
  if (real_near(t, file.last()))
      return;

  // Create sample
  Sample sample(*timeslab, t, "u", "ODE solution");

  // Save to file
  file << sample;

  // Let user save sample (optional)
  ode.save(sample);
}
//-----------------------------------------------------------------------------
bool TimeStepper::at_end(real t, real T) const
{
  return T - t < real_epsilon();
}
//-----------------------------------------------------------------------------
