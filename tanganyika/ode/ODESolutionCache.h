// Copyright (C) 2011 Benjamin Kehlet
//
// This file is part of Tanganyika
//
// Tanganyika is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DOLFIN is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Tanganyika.  If not, see <http://www.gnu.org/licenses/>.
//
// First added:  2011-09-02
// Last changed: 2011-09-02


#ifndef __ODESOLUTIONCACHE_H
#define __ODESOLUTIONCACHE_H

#include "ODESolution.h"

namespace tanganyika 
{

  // This is a simple wrapper that holds a const ODESolution instance It caches
  // results and queries the ODESolution upon cache misses, but without occupying as
  // memory for the full solution.

  class ODESolutionCache
  {
   public:
    ODESolutionCache(const ODESolution& solution, int cache_size=-1);

    ~ODESolutionCache();

    /// Evaluate (interpolate) value of solution at given time
    const Array<real>& eval(real t);

    const ODESolution& get_solution() const { return solution; }

   private:
    const ODESolution& solution;

    uint buffer_index_cache;

    uint size;
    std::pair<real, real*>* cache;
    uint ringbufcounter;

    Array<real> tmp;
  };

} //end namespace tanganyika


#endif
