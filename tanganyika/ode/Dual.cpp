// Copyright (C) 2003-2008 Anders Logg
//
// This file is part of DOLFIN.
//
// DOLFIN is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DOLFIN is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with DOLFIN.  If not, see <http://www.gnu.org/licenses/>.
//
// Modified by Benjamin Kehlet
//
// First added:  2003-11-28
// Last changed: 2013-05-16

#include <tanganyika/log/tanganyika_log.h>
#include <tanganyika/math/tanganyika_math.h>
#include "Dual.h"

using namespace tanganyika;

//------------------------------------------------------------------------
Dual::Dual(ODE& primal, const ODESolution& u, real T, uint c)
  : ODE(primal.size(), T),
    primal(primal), u(u),
    z_T(N),
    rhs(N)
{
  // Get parameters from primal
  parameters.update(primal.parameters);

  parameters["solution_file_name"] = "solution_dual.py";

  // set default initial data
  real data = 1.0 / sqrt(static_cast<real>(N));
  z_T = data;

  // set default right hand side
  rhs.zero();
}
//------------------------------------------------------------------------
Dual::~Dual()
{
  // Do nothing
}
//------------------------------------------------------------------------
void Dual::u0(Array<real>& psi)
{
  // copy the stored initial data
  psi = z_T;
}
//------------------------------------------------------------------------
void Dual::f(const Array<real>& phi, real t, Array<real>& y)
{
  get_A(phi, t, y);
  
  // Add right hand side
  // TODO: Do optimization here if rhs is zero
  real_add(size(), y.data().get(), rhs.data().get());
}
//------------------------------------------------------------------------
void Dual::J(const Array<real>& dx, Array<real>& dy, const Array<real>& /*u*/, real t)
{
  get_A(dx, t, dy);
}
//------------------------------------------------------------------------

real Dual::time(real t) const
{
  return endtime() - t;
}
//------------------------------------------------------------------------
void Dual::set_data(const Array<real>& data)
{
  z_T = data;
}
//------------------------------------------------------------------------
void Dual::set_right_hand_side(const Array<real>& g)
{
  rhs = g;
}
//------------------------------------------------------------------------
void Dual::get_A(const Array<real>& phi, real t, Array<real>& y)
{
  const real flipped_t = time(t);
  // cout << "Evaluating dual at " << flipped_t << endl;
  // cout << "phi = " << phi << endl;


  //Array<real> tmp(N);
  //tmp.zero();

  // Evaluate primal at T - t
  const Array<real>& tmp = u.eval(flipped_t);

  // Evaluate right-hand side
  primal.JT(phi, y, tmp, flipped_t);
}
//------------------------------------------------------------------------
void Dual::get_right_hand_side(Array<real>& g) const
{
  g = rhs;
}
