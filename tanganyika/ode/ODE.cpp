// Copyright (C) 2003-2011 Johan Jansson and Anders Logg
//
// This file is part of DOLFIN.
//
// DOLFIN is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DOLFIN is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with DOLFIN.  If not, see <http://www.gnu.org/licenses/>.
//
// Modified by Benjamin Kehlet 2009
//
// First added:  2003-10-21
// Last changed: 2011-10-27

#include "ODE.h"
#include "ODESolver.h"
#include "TimeStepper.h"
#include "Dual.h"
#include "StabilityAnalysis.h"
#include <tanganyika/log/log.h>
#include <tanganyika/common/Array.h>
#include <tanganyika/common/constants.h>
#include <tanganyika/math/tanganyika_math.h>
#include <tanganyika/log/LogStream.h>

using namespace tanganyika;

//-----------------------------------------------------------------------------
ODE::ODE(uint N, real T)
  : N(N), T(T), dependencies(N), transpose(N),
    not_impl_f("Warning: consider implementing mono-adaptive ODE::f() to improve efficiency."),
    not_impl_M("Warning: multiplication with M not implemented, assuming identity."),
    not_impl_J("Warning: consider implementing Jacobian ODE::J() to improve efficiency."),
    not_impl_JT("Warning: consider implementing Jacobian transpose ODE::JT() to improve efficiency")
{
  log(TRACE, "Creating ODE of size %d.", N);
  parameters = default_parameters();

  #ifdef HAS_GMP
  if (!_real_initialized)
      warning("Extended precision not initialized. Use set_precision(uint decimal_prec) before declaring any real variables and instansiating ODE.");
  #endif
}
//-----------------------------------------------------------------------------
ODE::~ODE()
{
  //delete [] tmp0;
  //delete [] tmp1;
  //delete time_stepper;
}
//-----------------------------------------------------------------------------
void ODE::f(const Array<real>& u, real t, Array<real>& y)
{
  // If a user of the mono-adaptive solver does not supply this function,
  // then call f_i() for each component.

  // Display a warning, more efficiently if implemented
  not_impl_f();

  // Call f for each component
  for (uint i = 0; i < N; i++)
    y[i] = this->f(u, t, i);
}
//-----------------------------------------------------------------------------
real ODE::f(const Array<real>& u, real t, uint i)
{
  error("Right-hand side for ODE not supplied by user.");
  return 0.0;
}
//-----------------------------------------------------------------------------
void ODE::M(const Array<real>& dx, Array<real>& dy, const Array<real>& u, real t)
{
  // Display a warning, implicit system but M is not implemented
  not_impl_M();

  // Assume M is the identity if not supplied by user: y = x
  real_set(N, dy.data().get(), dx.data().get());
}
//-----------------------------------------------------------------------------
void ODE::J(const Array<real>& dx, Array<real>& dy, const Array<real>& u, real t)
{
  // If a user does not supply J, then compute it by the approximation
  //
  //     Jx = ( f(u + hx) - f(u - hx) ) / 2h

  // FIXME: Maybe we should move this somewhere else?

  // Display a warning, more efficiently if implemented
  not_impl_J();

  // Small change in u
  real umax = 0.0;
  for (uint i = 0; i < N; i++)
    umax = real_max(umax, real_abs(u[i]));
  const real h = real_max(real_epsilon_sqrt(), real_epsilon_sqrt() * umax);

  // We are not allowed to change u, but we restore it afterwards,
  // so maybe we can cheat a little...
  Array<real> uu(N);
  uu = u;
  

  Array<real> tmp(N);
  tmp.zero();

  // Evaluate at u + hx
  real_axpy(N, uu.data().get(), h, dx.data().get());
  f(uu, t, dy);

  // Evaluate at u - hx
  real_axpy(N, uu.data().get(), -2.0*h, dx.data().get());
  f(uu, t, tmp);

  // Reset u
  real_axpy(N, uu.data().get(), h, dx.data().get());

  // Compute product dy = J dx
  real_sub(N, dy.data().get(), tmp.data().get());
  real_mult(N, dy.data().get(), 0.5/h);
}
//------------------------------------------------------------------------
void ODE::JT(const Array<real>& dx, Array<real>& dy, const Array<real>& u, real t)
{
  // Display warning
  not_impl_JT();

  // Small change in u
  real umax = 0.0;
  for (uint i = 0; i < N; i++)
    umax = real_max(umax, real_abs(u[i]));
  const real h = real_max(real_epsilon_sqrt(), real_epsilon_sqrt() * umax);

  // We are not allowed to change u, so we have to copy.
  Array<real> uu(u.size());
  uu = u;

  Array<real> tmp0(u.size());
  Array<real> tmp1(u.size());

  // Initialize temporary arrays if necessary
  //if (!tmp0) tmp0 = new real[N];
  //real_zero(N, tmp0);
  tmp0.zero();
  //if (!tmp1) tmp1 = new real[N];
  //real_zero(N, tmp1);
  tmp1.zero();

  // Compute action of transpose of Jacobian
  for (uint i = 0; i < N; ++i)
  {
    uu[i] += h;
    f(uu, t, tmp0);

    uu[i] -= 2*h;
    f(uu, t, tmp1);

    uu[i] += h;

    real_sub(N, tmp0.data().get(), tmp1.data().get());
    real_mult(N, tmp0.data().get(), 0.5/h);

    dy[i] = real_inner(N, tmp0.data().get(), dx.data().get());
  }
}
//------------------------------------------------------------------------
real ODE::dfdu(const Array<real>& u, real t, uint i, uint j)
{
  // Compute Jacobian numerically if dfdu() is not implemented by user

  // FIXME: Maybe we should move this somewhere else?

  // We are not allowed to change u, but we restore it afterwards,
  // so maybe we can cheat a little...
  Array<real>& uu = const_cast<Array<real>&>(u);

  // Save value of u_j
  real uj = uu[j];

  // Small change in u_j
  real h = real_max(DOLFIN_SQRT_EPS, DOLFIN_SQRT_EPS * real_abs(uj));

  // Compute F values
  uu[j] -= 0.5 * h;
  real f1 = f(uu, t, i);

  uu[j] = uj + 0.5*h;
  real f2 = f(uu, t, i);

  // Reset value of uj
  uu[j] = uj;

  // Compute derivative
  if (real_abs(f1 - f2) < real_epsilon() * real_max(real_abs(f1), real_abs(f2)))
    return 0.0;

  return (f2 - f1) / h;
}
//-----------------------------------------------------------------------------
real ODE::timestep(real t, real k0) const
{
  // Keep old time step by default when "fixed time step" is set
  // and user has not overloaded this function
  return k0;
}
//-----------------------------------------------------------------------------
real ODE::timestep(real t, uint i, real k0) const
{
  // Keep old time step by default when "fixed time step" is set
  // and user has not overloaded this function
  return k0;
}
//-----------------------------------------------------------------------------
bool ODE::update(const Array<real>& u, real t, bool end)
{
  return true;
}
//-----------------------------------------------------------------------------
void ODE::save(Sample& sample)
{
  // Do nothing
}
//-----------------------------------------------------------------------------
tanganyika::uint ODE::size() const
{
  return N;
}
//-----------------------------------------------------------------------------
real ODE::time(real t) const
{
  return t;
}
//-----------------------------------------------------------------------------
const real ODE::endtime() const
{
  return T;
}
//-----------------------------------------------------------------------------
void ODE::sparse()
{
  dependencies.detect(*this);
}
//-----------------------------------------------------------------------------
Dependencies& ODE::get_dependencies()
{
  return dependencies;
}
//-----------------------------------------------------------------------------
Dependencies& ODE::get_transpose_dependencies()
{
  return transpose;
}
//-----------------------------------------------------------------------------
void ODE::analyze_stability( uint q, ODESolution& u) {
  Dual d(*this, u, endtime());
  StabilityAnalysis S(d);

  S.analyze_integral(q, endtime());
}
//-----------------------------------------------------------------------------
void ODE::analyze_stability_discretization(ODESolution& u) {
  Dual d(*this, u, endtime());
  StabilityAnalysis S(d);

  S.analyze_integral(parameters["order"], endtime());
}
//-----------------------------------------------------------------------------
void ODE::analyze_stability_computation(ODESolution& u) {
  Dual d(*this, u, endtime());
  StabilityAnalysis S(d);
  
  S.analyze_integral(0, endtime());
}
//-----------------------------------------------------------------------------
void ODE::analyze_stability_initial(ODESolution& u) {
  Dual d(*this, u, endtime());
  StabilityAnalysis S(d);
  S.analyze_endpoint(endtime());
}
//-----------------------------------------------------------------------------
void ODE::store_f(ODESolution &u, std::string filename, uint num_samples, real x_start, real x_end)
{
  PythonFile outfile(filename);
  if (x_end < 0)
    x_end = u.endtime();

  const real delta_t = (x_end-x_start)/(num_samples-1);

  Array<real> u_sample(u.csize());
  Array<real> f_sample(u.csize());
  Progress p("Storing f", num_samples);
  for (uint i = 0; i < num_samples; ++i)
  {
    const real t = x_start + i*delta_t;
    u.eval(t, u_sample);

    f(u_sample, t, f_sample);
    outfile.write(t, f_sample);
    p++;
  }
}
//-----------------------------------------------------------------------------
void ODE::store_jacobian(ODESolution &u, std::string filename, uint num_samples, real x_start, real x_end)
{
  PythonFile outfile(filename);
  if (x_end < 0)
    x_end = u.endtime();
  const real delta_t = (x_end-x_start)/(num_samples-1);

  Array<real> u_sample(u.csize());
  Array<real> jacobian_sample(u.csize()*u.csize());
  Array<real> x(u.csize());
  Progress p("Storing jacobian", num_samples);
  for (uint i = 0; i < num_samples; ++i)
  {
    const real t = x_start + i*delta_t;
    u.eval(t, u_sample);

    for (uint j = 0; j < u.csize(); ++j)
    {
      x.zero();
      x[j] = 1.0;
      Array<real> jacobian_(u.csize(), &jacobian_sample[u.csize()*j]);
      J(x, jacobian_, u_sample, t);
    }

    outfile.write(t, jacobian_sample);
    p++;
  }

}

//-----------------------------------------------------------------------------
real ODE::test_J()
{
  Array<real> u(size()); 
  Array<real> x(size()); 
  Array<real> du_numeric(size());
  Array<real> du_exact(size());
  const real t = 5.0;

  real max_norm = 0.0;

  // Initialize u with some numbers
  for (uint i = 0; i < size(); ++i)
    u[i] = (i+1)*.01;

  // Set one component of x to non-zero value
  for (uint i = 0; i < size(); ++i)
  {
    x.zero();
    x[i] = 1.0;
    J(x, du_exact, u, t);
    ODE::J(x, du_numeric, u, t);

    // cout << "Numeric: " << du_numeric << endl;
    // cout << "Exact:   " << du_exact << endl;

    real_sub(size(), &du_exact[0], &du_numeric[0]);
    max_norm = real_max(max_norm, real_max_abs(size(), &du_exact[0]));
  }

  for (uint i = 0; i < 10; ++i)
  {
    for (uint j = 0; j < size(); ++j)
    {
      x[j] = sin(static_cast<preal>(j));
    }

    x[i] = 1.0;
    J(x, du_exact, u, t);
    ODE::J(x, du_numeric, u, t);

    real_sub(size(), &du_exact[0], &du_numeric[0]);
    max_norm = real_max(max_norm, real_max_abs(size(), &du_exact[0]));
    cout << "Loop" << endl;
  }

  return max_norm;
}
//-----------------------------------------------------------------------------
real ODE::test_JT()
{
  Array<real> u(size()); 
  Array<real> x(size()); 
  Array<real> du_numeric(size());
  Array<real> du_exact(size());
  const real t = 5.0;

  real max_norm = 0.0;

  // Initialize u with some numbers
  for (uint i = 0; i < size(); ++i)
    u[i] = i*.01;

  // Set one component of x to non-zero value
  for (uint i = 0; i < size(); ++i)
  {
    x.zero();
    x[i] = .5;
    JT(x, du_exact, u, t);
    ODE::JT(x, du_numeric, u, t);

    real_sub(size(), &du_exact[0], &du_numeric[0]);
    max_norm = real_max(max_norm, real_max_abs(size(), &du_exact[0]));
  }

  // Set all components of x to non-zero value
  for (uint i = 0; i < size(); ++i)
    x[i] = 1.1 + i*.2;

  JT(x, du_exact, u, t);
  ODE::JT(x, du_numeric, u, t);
  real_sub(size(), &du_exact[0], &du_numeric[0]);
  max_norm = real_max(max_norm, real_max_abs(size(), &du_exact[0]));

  return max_norm;
}
// //-----------------------------------------------------------------------------
// void ODE::set_state(const Array<real>& u)
// {
//   // Create time stepper if not created before
//   if (!time_stepper)
//     time_stepper = new TimeStepper(*this);

//   // Set state
//   time_stepper->set_state(u.data().get());
// }
// //-----------------------------------------------------------------------------
// void ODE::get_state(Array<real>& u)
// {
//   // Create time stepper if not created before
//   if (!time_stepper)
//     time_stepper = new TimeStepper(*this);

//   // Get state
//   time_stepper->get_state(u.data().get());
// }
// //-----------------------------------------------------------------------------
