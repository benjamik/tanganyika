// Copyright (C) 2005-2008 Anders Logg
//
// This file is part of DOLFIN.
//
// DOLFIN is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DOLFIN is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with DOLFIN.  If not, see <http://www.gnu.org/licenses/>.
//
// First added:  2005-01-27
// Last changed: 2009-09-08

#include <tanganyika/log/tanganyika_log.h>
#include "Alloc.h"
#include "ODE.h"
#include "Method.h"
#include "MultiAdaptiveTimeSlab.h"
#include "MultiAdaptiveFixedPointSolver.h"

using namespace tanganyika;

//-----------------------------------------------------------------------------
MultiAdaptiveFixedPointSolver::MultiAdaptiveFixedPointSolver
(MultiAdaptiveTimeSlab& timeslab)
  : TimeSlabSolver(timeslab), ts(timeslab), f(0),
    num_elements(0), num_elements_mono(0),
    maxiter_local(timeslab.parameters["maximum_local_iterations"]),
    diagonal_newton_damping(timeslab.ode.parameters["diagonal_newton_damping"]), 
    dfdu(0)
{
  // Initialize local array for quadrature
  f = new real[timeslab.method.qsize()];
  for (uint i = 0; i < timeslab.method.qsize(); i++)
    f[i] = 0.0;

  // Initialize diagonal of Jacobian df/du for diagonal Newton damping
  if ( diagonal_newton_damping )
  {
    dfdu = new real[ts.N];
    for (uint i = 0; i < ts.N; i++)
      dfdu[i] = 0.0;
  }
}
//-----------------------------------------------------------------------------
MultiAdaptiveFixedPointSolver::~MultiAdaptiveFixedPointSolver()
{
  // Compute multi-adaptive efficiency index
  if ( num_elements > 0 )
  {
    const real alpha = num_elements_mono / static_cast<real>(num_elements);
    info("Multi-adaptive efficiency index: %.3f", to_preal(alpha));
  }

  // Delete local array
  if ( f ) delete [] f;

  // Delete diagonal of Jacobian
  if ( dfdu ) delete [] dfdu;
}
//-----------------------------------------------------------------------------
bool MultiAdaptiveFixedPointSolver::retry()
{
  // If we're already using damping, then we don't know what to do
  if ( diagonal_newton_damping )
    return false;

  // Otherwise, use damping
  assert(dfdu == 0);
  diagonal_newton_damping = true;
  dfdu = new real[ts.N];
  for (uint i = 0; i < ts.N; i++)
    dfdu[i] = 0.0;

  // Reset system
  ts.reset();

  info("Direct fixed-point iteration does not converge.");
  info("Trying diagonally damped fixed-point iteration.");
  return true;
}
//-----------------------------------------------------------------------------
void MultiAdaptiveFixedPointSolver::start()
{
  // Update diagonal of Jacobian if used
  if ( diagonal_newton_damping )
  {
    for (uint i = 0; i < ts.N; i++)
      dfdu[i] = ts.ode.dfdu(ts.u0, ts._a, i, i);
  }
}
//-----------------------------------------------------------------------------
void MultiAdaptiveFixedPointSolver::end()
{
  // Count the number of elements
  num_elements += ts.ne;
  num_elements_mono += ts.length() / ts.kmin * static_cast<real>(ts.ode.size());
}
//-----------------------------------------------------------------------------
real MultiAdaptiveFixedPointSolver::iteration(const real& tol, uint iter,
					      const real& d0, const real& d1)
{
  // Reset dof
  uint j = 0;

  // Reset elast
  for (uint i = 0; i < ts.N; i++)
    ts.elast[i] = -1;

  // Save norm of old solution
  xnorm = 0.0;
  for (uint j = 0; j < ts.nj; j++)
    xnorm = real_max(xnorm, real_abs(ts.jx[j]));

  // Reset maximum increment
  real increment_max = 0.0;

  // Keep track of the number of local iterations
  real num_local = 0.0;

  // Iterate over all sub slabs
  uint e0 = 0;
  uint e1 = 0;
  for (uint s = 0; s < ts.ns; s++)
  {
    // Cover all elements in current sub slab
    e1 = ts.cover_slab(s, e0);

    // Get data for sub slab
    const real a = ts.sa[s];
    const real b = ts.sb[s];
    const real k = b - a;

    // Save current dof
    uint j0 = j;

    // Iterate on each sub slab
    real increment_local = 0.0;
    for (uint iter_local = 0; iter_local < maxiter_local; iter_local++)
    {
      // Reset current dof
      j = j0;

      // Iterate over all elements in current sub slab
      for (uint e = e0; e < e1; e++)
      {
	// Get element data
	const uint i = ts.ei[e];

	// Save old end-point value
	const real x1 = ts.jx[j + timeslab.method.nsize() - 1];

	// Get initial value for element
	const int ep = ts.ee[e];
	const real x0 = ( ep != -1 ? ts.jx[ep*timeslab.method.nsize() + timeslab.method.nsize() - 1] : ts.u0[i] );

	// Evaluate right-hand side at quadrature points of element
	if ( timeslab.method.type() == Method::cG )
	  ts.cg_feval(f, s, e, i, a, b, k);
	else
	  ts.dg_feval(f, s, e, i, a, b, k);
	//cout << "f = "; Alloc::display(f, timeslab.method.qsize());

	// Update values on element using fixed-point iteration
	if ( diagonal_newton_damping )
	{
	  // FIXME: Parameter 0.5 most suited for cG(1)
	  const real alpha = 1.0 / (1.0 - 0.5*k*dfdu[i]);
	  timeslab.method.update(x0, f, k, ts.jx + j, alpha);
	}
	else
	{
	  timeslab.method.update(x0, f, k, ts.jx + j);
	}
	//cout << "x = "; Alloc::display(ts.jx + j, timeslab.method.nsize());

	// Compute increment
	const real increment = real_abs(ts.jx[j + timeslab.method.nsize() - 1] - x1);

	// Update sub slab increment
	increment_local = real_max(increment_local, increment);

	// Update maximum increment
	increment_max = real_max(increment_max, increment);

	// Update dof
	j += timeslab.method.nsize();
      }

      // Update counter of local iterations
      num_local += static_cast<real>(e1 - e0) / static_cast<real>(ts.ne);

      // Check if we are done
      if ( increment_local < tol )
      {

	break;
      }
    }

    // Step to next sub slab
    e0 = e1;
  }

  // Add to common counter of local iterations
  //A little silly, but GMP don't define casting from mpf_class to uint
  num_local_iterations += static_cast<uint>(to_preal(num_local) + 0.5);

  return increment_max;
}
//-----------------------------------------------------------------------------
tanganyika::uint MultiAdaptiveFixedPointSolver::size() const
{
  return ts.nj;
}
//-----------------------------------------------------------------------------
