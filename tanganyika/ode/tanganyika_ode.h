#ifndef __DOLFIN_ODE_H
#define __DOLFIN_ODE_H

// DOLFIN ode interface

#include <tanganyika/ode/Sample.h>
#include <tanganyika/ode/ODE.h>
#include <tanganyika/ode/ODESolver.h>
#include <tanganyika/ode/ODECollection.h>
#include <tanganyika/ode/ComplexODE.h>
#include <tanganyika/ode/Method.h>
#include <tanganyika/ode/cGqMethod.h>
#include <tanganyika/ode/dGqMethod.h>
#include <tanganyika/ode/ODESolution.h>
#include <tanganyika/ode/Dual.h>
#include <tanganyika/ode/StabilityAnalysis.h>

#endif
