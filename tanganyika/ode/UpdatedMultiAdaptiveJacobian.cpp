// Copyright (C) 2005-2008 Anders Logg
//
// This file is part of DOLFIN.
//
// DOLFIN is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DOLFIN is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with DOLFIN.  If not, see <http://www.gnu.org/licenses/>.
//
// First added:  2005-01-27
// Last changed: 2008-04-22

#include <tanganyika/common/constants.h>
#include <tanganyika/math/tanganyika_math.h>
#include <tanganyika/la/uBLASVector.h>
#include "ODE.h"
#include "Method.h"
#include "MultiAdaptiveTimeSlab.h"
#include "MultiAdaptiveNewtonSolver.h"
#include "UpdatedMultiAdaptiveJacobian.h"

using namespace tanganyika;

//-----------------------------------------------------------------------------
UpdatedMultiAdaptiveJacobian::UpdatedMultiAdaptiveJacobian
(MultiAdaptiveNewtonSolver& newton, MultiAdaptiveTimeSlab& timeslab)
  : TimeSlabJacobian(timeslab), newton(newton), ts(timeslab),  h(0)
{
  // Do nothing
}
//-----------------------------------------------------------------------------
UpdatedMultiAdaptiveJacobian::~UpdatedMultiAdaptiveJacobian()
{
  // Do nothing
}
//-----------------------------------------------------------------------------
tanganyika::uint UpdatedMultiAdaptiveJacobian::size(uint dim) const
{
  return ts.nj;
}
//-----------------------------------------------------------------------------
void UpdatedMultiAdaptiveJacobian::mult(const real* x, real* y) const
{
  // Compute product by the approximation y = J(u) x = (F(u + hx) - F(u)) / h.
  // Since Feval() compute -F rather than F, we compute according to
  //
  //     y = J(u) x = (-F(u - hx) - (-F(u))) / h

  uBLASVector xx(ts.nj);
  uBLASVector yy(ts.nj);

  for (uint i = 0; i < ts.nj; i++)
    xx[i] = to_preal(x[i]);


  // Update values, u <-- u - hx
  for (uint j = 0; j < ts.nj; j++)
    ts.jx[j] -= h*xx[j];

  // Compute -F(u - hx)
  newton.Feval(yy);

  // Restore values, u <-- u + hx
  for (uint j = 0; j < ts.nj; j++)
    ts.jx[j] += h*xx[j];

  // Compute difference, using already computed -F(u)
  yy -= newton.b;
  yy /= h;

  for (uint i = 0; i < yy.size(); i++)
    y[i] = yy[i];
}
//-----------------------------------------------------------------------------
void UpdatedMultiAdaptiveJacobian::init()
{
  // Compute size of increment
  preal umax = 0.0;
  for (uint i = 0; i < ts.N; i++)
  {
    //NOTE: to_preal() here
    // umax = std::max(umax, std::abs(to_preal(ts.u0[i])));
    umax = std::max(umax, abs(to_preal(ts.u0[i])));
  }
  h = std::max(DOLFIN_SQRT_EPS, DOLFIN_SQRT_EPS * umax);
}
//-----------------------------------------------------------------------------
