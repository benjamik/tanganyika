// Copyright (C) 2005-2008 Anders Logg
//
// This file is part of DOLFIN.
//
// DOLFIN is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DOLFIN is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with DOLFIN.  If not, see <http://www.gnu.org/licenses/>.
//
// First added:  2005-01-28
// Last changed: 2008-04-08

#ifndef __TIME_SLAB_JACOBIAN_H
#define __TIME_SLAB_JACOBIAN_H

#include <tanganyika/log/tanganyika_log.h>
#include <tanganyika/la/uBLASDenseMatrix.h>
#include <tanganyika/la/uBLASVector.h>
#include <tanganyika/la/uBLASKrylovMatrix.h>
//#include <tanganyika/la/Matrix.h>

namespace tanganyika
{

  class ODE;
  class Method;
  class TimeSlab;

  /// This is the base class for Jacobians defined on mono- or
  /// multi-adaptive time slabs.

  class TimeSlabJacobian //: public uBLASKrylovMatrix
  {
  public:

    /// Constructor
    TimeSlabJacobian(TimeSlab& timeslab);

    /// Destructor
    ~TimeSlabJacobian();

    /// Return number of rows (dim = 0) or columns (dim = 1)
    virtual uint size(uint dim) const = 0;

    /// Compute product y = Ax
    virtual void mult(const real* x, real* y) const = 0;

    /// (Re-)initialize computation of Jacobian
    virtual void init();

    /// Update dense copy of Jacobian
    void update();

    /// Return dense copy of Jacobian
    const uBLASDenseMatrix& matrix() const;

    /// Return dense (possibly high precision) copy of Jacobian
    const real* array() const;

  protected:

    // The ODE
    ODE& ode;

    // Method, mcG(q) or mdG(q)
    const Method& method;

    // Dense copy of the Jacobian
    uBLASDenseMatrix A;

    real* A_array;

    // Vectors used to compute dense copy of the Jacobian
    //uBLASVector ej, Aj;


  };

}

#endif
