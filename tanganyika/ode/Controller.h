// Copyright (C) 2005 Anders Logg
//
// This file is part of Tanganyika.
//
// Tanganyika is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Tanganyika is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Tanganyika.  If not, see <http://www.gnu.org/licenses/>.
//
// Modified by Benjamin Kehlet, 2015
//
// First added:  2005-11-02
// Last changed: 2015-03-07

#ifndef __CONTROLLER_H
#define __CONTROLLER_H

#include <tanganyika/common/types.h>
#include <tanganyika/common/real.h>

namespace tanganyika
{

  /// Controller for adaptive time step selection, based on the list
  /// of controllers presented in "Digital Filters in Adaptive
  /// Time-Stepping" by Gustaf Soderlind (ACM TOMS 2003).

  class Controller
  {
  public:

    /// Create uninitialized controller
    Controller();

    /// Create controller with given initial state
    Controller(real k, real tol, uint p, real kmax);

    /// Destructor
    ~Controller();

    /// Initialize controller
    void init(real k, real tol, uint p, real kmax);

    /// Reset controller
    void reset(real k);

    /// Controller H0211
    real updateH0211(real e, real tol);

    /// Controller H211PI
    real updateH211PI(real e, real tol);

    /// No control, simple formula
    real update_simple(real e, real tol);

    /// Control by harmonic mean value
    real update_harmonic(real e, real tol);

    /// Control by harmonic mean value (no history supplied)
    static real update_harmonic(real knew, real kold, real kmax);

    /// Default controller
    inline real update(real e, real tol) { return updateH211PI(e, tol); }

  private:

    // Time step history
    real k0, k1;

    // Error history
    real e0;

    // Asymptotics: e ~ k^p
    std::size_t p;

    // Maximum time step
    real kmax;

  };

}

#endif
