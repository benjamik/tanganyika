// Copyright (C) 2006-2008 Garth N. Wells
//
// This file is part of DOLFIN.
//
// DOLFIN is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DOLFIN is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with DOLFIN.  If not, see <http://www.gnu.org/licenses/>.
//
// Modified by Anders Logg 2006-2010.
// Modified by Kent-Andre Mardal 2008.
// Modified by Martin Sandve Alnes 2008.
//
// First added:  2006-04-04
// Last changed: 2011-01-14

#include "uBLASVector.h"
#include <algorithm>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/vector_expression.hpp>
#include <boost/unordered_set.hpp>

#include <tanganyika/log/tanganyika_log.h>
#include <tanganyika/common/Array.h>


using namespace tanganyika;

//-----------------------------------------------------------------------------
uBLASVector::uBLASVector(): x(new ublas_vector(0))
{
  // Do nothing
}
//-----------------------------------------------------------------------------
uBLASVector::uBLASVector(uint N): x(new ublas_vector(N))
{
  // Set all entries to zero
  x->clear();
}
//-----------------------------------------------------------------------------
uBLASVector::uBLASVector(const uBLASVector& x): x(new ublas_vector(*(x.x)))
{
  // Do nothing
}
//-----------------------------------------------------------------------------
uBLASVector::uBLASVector(std::shared_ptr<ublas_vector> x) : x(x)
{
  // Do nothing
}
//-----------------------------------------------------------------------------
uBLASVector::~uBLASVector()
{
  // Do nothing
}
//-----------------------------------------------------------------------------
uBLASVector* uBLASVector::copy() const
{
  return new uBLASVector(*this);
}
//-----------------------------------------------------------------------------
void uBLASVector::resize(uint N)
{
  if (x->size() == N)
    return;
  x->resize(N, false);

  // Set vector to zero to prevent random numbers entering the vector.
  // Fixes this bug: https://bugs.launchpad.net/tanganyika/+bug/594954
  x->clear();
}
//-----------------------------------------------------------------------------
void uBLASVector::resize(std::pair<uint, uint> range)
{
  if (range.first != 0)
    error("uBLASVector does not support distributed vectors.");

  resize(range.second - range.first);
}
//-----------------------------------------------------------------------------
void uBLASVector::resize(std::pair<uint, uint> range,
                    const std::vector<uint>& ghost_indices)
{
  if (range.first != 0)
    error("uBLASVector does not support distributed vectors.");

  if (ghost_indices.size() != 0)
    error("uBLASVector does not support ghost entries.");

  resize(range.second - range.first);
}
//-----------------------------------------------------------------------------
tanganyika::uint uBLASVector::size() const
{
  return x->size();
}
//-----------------------------------------------------------------------------
// std::pair<tanganyika::uint, tanganyika::uint> uBLASVector::local_range() const
// {
//   return std::make_pair(0, size());
// }
// //-----------------------------------------------------------------------------
// bool uBLASVector::owns_index(uint i) const
// {
//   if (i < size())
//     return true;
//   else
//     return false;
// }
// //-----------------------------------------------------------------------------
// void uBLASVector::get_local(preal* block, uint m, const uint* rows) const
// {
//   for (uint i = 0; i < m; i++)
//     block[i] = (*x)(rows[i]);
// }
// //-----------------------------------------------------------------------------
// void uBLASVector::get_local(Array<preal>& values) const
// {
//   values.resize(size());
//   for (uint i = 0; i < size(); i++)
//     values[i] = (*x)(i);
// }
// //-----------------------------------------------------------------------------
// void uBLASVector::set_local(const Array<preal>& values)
// {
//   assert(values.size() == size());
//   for (uint i = 0; i < size(); i++)
//     (*x)(i) = values[i];
// }
// //-----------------------------------------------------------------------------
// void uBLASVector::add_local(const Array<preal>& values)
// {
//   assert(values.size() == size());
//   for (uint i = 0; i < size(); i++)
//     (*x)(i) += values[i];
// }
//-----------------------------------------------------------------------------
// void uBLASVector::gather(GenericVector& x, const Array<uint>& indices) const
// {
//   const uint _size = indices.size();
//   assert(this->size() >= _size);

//   x.resize(_size);
//   ublas_vector& _x = x.down_cast<uBLASVector>().vec();
//   for (uint i = 0; i < _size; i++)
//     _x(i) = (*this->x)(indices[i]);
// }
//-----------------------------------------------------------------------------
// void uBLASVector::gather(Array<preal>& x, const Array<uint>& indices) const
// {
//   const uint _size = indices.size();
//   x.resize(_size);
//   assert(x.size() == _size);
//   for (uint i = 0; i < _size; i++)
//     x[i] = (*this->x)(indices[i]);
// }
// //-----------------------------------------------------------------------------
// void uBLASVector::set(const preal* block, uint m, const uint* rows)
// {
//   for (uint i = 0; i < m; i++)
//     (*x)(rows[i]) = block[i];
// }
// //-----------------------------------------------------------------------------
// void uBLASVector::add(const preal* block, uint m, const uint* rows)
// {
//   for (uint i = 0; i < m; i++)
//     (*x)(rows[i]) += block[i];
// }
// //-----------------------------------------------------------------------------
// void uBLASVector::apply(std::string mode)
// {
//   // Do nothing
// }
//-----------------------------------------------------------------------------
void uBLASVector::zero()
{
  x->clear();
}
//-----------------------------------------------------------------------------
preal uBLASVector::norm(std::string norm_type) const
{
  if (norm_type == "l1")
    return norm_1(*x);
  else if (norm_type == "l2")
    return norm_2(*x);
  else if (norm_type == "linf")
    return norm_inf(*x);
  else
    error("Requested vector norm type for uBLASVector unknown");

  return 0.0;
}
//-----------------------------------------------------------------------------
preal uBLASVector::min() const
{
  preal value = *std::min_element(x->begin(), x->end());
  return value;
}
//-----------------------------------------------------------------------------
preal uBLASVector::max() const
{
  preal value = *std::max_element(x->begin(), x->end());
  return value;
}
//-----------------------------------------------------------------------------
preal uBLASVector::sum() const
{
  return ublas::sum(*x);
}
//-----------------------------------------------------------------------------
preal uBLASVector::sum(const Array<uint>& rows) const
{
  boost::unordered_set<uint> row_set;
  preal _sum = 0.0;
  for (uint i = 0; i < rows.size(); ++i)
  {
    const uint index = rows[i];
    assert(index < size());
    if (row_set.find(index) == row_set.end())
    {
      _sum += (*x)[index];
      row_set.insert(index);
    }
  }
  return _sum;
}
//-----------------------------------------------------------------------------
// void uBLASVector::axpy(preal a, const GenericVector& y)
// {
//   if (size() != y.size())
//     error("Vectors must be of same size.");

//   (*x) += a * y.down_cast<uBLASVector>().vec();
// }
//-----------------------------------------------------------------------------
// void uBLASVector::abs()
// {
//   assert(x);
//   const uint size = x->size();
//   for (uint i = 0; i < size; i++)
//     (*x)[i] = std::abs((*x)[i]);
// }
//-----------------------------------------------------------------------------
// preal uBLASVector::inner(const GenericVector& y) const
// {
//   return ublas::inner_prod(*x, y.down_cast<uBLASVector>().vec());
// }
//-----------------------------------------------------------------------------
// const GenericVector& uBLASVector::operator= (const GenericVector& y)
// {
//   *x = y.down_cast<uBLASVector>().vec();
//   return *this;
// }
//-----------------------------------------------------------------------------
const uBLASVector& uBLASVector::operator= (const uBLASVector& y)
{
  *x = y.vec();
  return *this;
}
//-----------------------------------------------------------------------------
const uBLASVector& uBLASVector::operator= (preal a)
{
  x->ublas_vector::assign(ublas::scalar_vector<preal> (x->size(), a));
  return *this;
}
//-----------------------------------------------------------------------------
const uBLASVector& uBLASVector::operator*= (const preal a)
{
  (*x) *= a;
  return *this;
}
//-----------------------------------------------------------------------------
// const uBLASVector& uBLASVector::operator*= (const GenericVector& y)
// {
//   *x = ublas::element_prod(*x,y.down_cast<uBLASVector>().vec());
//   return *this;
// }
//-----------------------------------------------------------------------------
const uBLASVector& uBLASVector::operator/= (const preal a)
{
  (*x) /= a;
  return *this;
}
//-----------------------------------------------------------------------------
// const uBLASVector& uBLASVector::operator+= (const GenericVector& y)
// {
//   *x += y.down_cast<uBLASVector>().vec();
//   return *this;
// }
//-----------------------------------------------------------------------------
// const uBLASVector& uBLASVector::operator-= (const GenericVector& y)
// {
//   *x -= y.down_cast<uBLASVector>().vec();
//   return *this;
// }
const uBLASVector& uBLASVector::operator-= (const uBLASVector& y)
{
  *x -= y.vec();
  return *this;
}

//-----------------------------------------------------------------------------
std::string uBLASVector::str(bool verbose) const
{
  std::stringstream s;

  if (verbose)
  {
    s << str(false) << std::endl << std::endl;

    s << "[";
    for (ublas_vector::const_iterator it = x->begin(); it != x->end(); ++it)
    {
      std::stringstream entry;
      entry << std::setiosflags(std::ios::scientific);
      entry << std::setprecision(16);
      entry << *it << " ";
      s << entry.str() << std::endl;
    }
    s << "]";
  }
  else
  {
    s << "<uBLASVector of size " << size() << ">";
  }

  return s.str();
}
//-----------------------------------------------------------------------------
