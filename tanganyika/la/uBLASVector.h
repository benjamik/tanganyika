// Copyright (C) 2006-2010 Garth N. Wells
//
// This file is part of DOLFIN.
//
// DOLFIN is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DOLFIN is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with DOLFIN.  If not, see <http://www.gnu.org/licenses/>.
//
// Modified by Anders Logg, 2006-2010.
// Modified by Kent-Andre Mardal, 2008.
// Modified by Ola Skavhaug, 2008.
// Modified by Martin Alnæs, 2008.
//
// First added:  2006-03-04
// Last changed: 2011-01-14

#ifndef __UBLAS_VECTOR_H
#define __UBLAS_VECTOR_H

#include "ublas.h"
#include <memory>

namespace tanganyika
{

  namespace ublas = boost::numeric::ublas;

  template<class T> class Array;

  /// This class provides a simple vector class based on uBLAS.
  /// It is a simple wrapper for a uBLAS vector implementing the
  /// GenericVector interface.
  ///
  /// The interface is intentionally simple. For advanced usage,
  /// access the underlying uBLAS vector and use the standard
  /// uBLAS interface which is documented at
  /// http://www.boost.org/libs/numeric/ublas/doc/index.htm.

  class uBLASVector //: public GenericVector
  {
  public:

    /// Create empty vector
    uBLASVector();

    /// Create vector of size N
    explicit uBLASVector(uint N);

    /// Copy constructor
    uBLASVector(const uBLASVector& x);

    /// Construct vector from a ublas_vector
    explicit uBLASVector(std::shared_ptr<ublas_vector> x);

    // Create vector from given uBLAS vector expression
    //template <class E>
    //explicit uBLASVector(const ublas::vector_expression<E>& x) : x(new ublas_vector(x)) {}

    /// Destructor
    virtual ~uBLASVector();

    //--- Implementation of the GenericTensor interface ---

    /// Create copy of tensor
    virtual uBLASVector* copy() const;

    /// Set all entries to zero and keep any sparse structure
    virtual void zero();

    /// Finalize assembly of tensor
    // virtual void apply(std::string mode);

    /// Return informal string representation (pretty-print)
    virtual std::string str(bool verbose) const;

    //--- Implementation of the GenericVector interface ---

    /// Resize vector to size N
    virtual void resize(uint N);

    /// Resize vector with given ownership range
    virtual void resize(std::pair<uint, uint> range);

    /// Resize vector with given ownership range and with ghost values
    virtual void resize(std::pair<uint, uint> range,
                        const std::vector<uint>& ghost_indices);

    /// Return size of vector
    virtual uint size() const;

    /// Replace all entries in the vector by their absolute values
    //virtual void abs();

    /// Return inner product with given vector
    //virtual preal inner(const GenericVector& x) const;

    /// Compute norm of vector
    virtual preal norm(std::string norm_type) const;

    /// Return minimum value of vector
    virtual preal min() const;

    /// Return maximum value of vector
    virtual preal max() const;

    /// Return sum of values of vector
    virtual preal sum() const;

    /// Return sum of selected rows in vector. Repeated entries are only summed once.
    virtual preal sum(const Array<uint>& rows) const;

    /// Multiply vector by given number
    virtual const uBLASVector& operator*= (preal a);

    /// Multiply vector by another vector pointwise
    // virtual const uBLASVector& operator*= (const GenericVector& x);

    /// Divide vector by given number
    virtual const uBLASVector& operator/= (preal a);

    /// Add given vector
    // virtual const uBLASVector& operator+= (const GenericVector& x);

    /// Subtract given vector
    //virtual const uBLASVector& operator-= (const GenericVector& x);
    virtual const uBLASVector& operator-= (const uBLASVector& x);

    /// Assignment operator
    //virtual const GenericVector& operator= (const GenericVector& x);

    /// Assignment operator
    virtual const uBLASVector& operator= (preal a);

    /// Return pointer to underlying data (const version)
    virtual const preal* data() const
    { return &x->data()[0]; }

    /// Return pointer to underlying data
    virtual preal* data()
    { return &x->data()[0]; }

    //--- Special uBLAS functions ---

    /// Return reference to uBLAS vector (const version)
    const ublas_vector& vec() const { return *x; }

    /// Return reference to uBLAS vector (non-const version)
    ublas_vector& vec() { return *x; }

    /// Access value of given entry (const version)
    virtual preal operator[] (uint i) const { return (*x)(i); };

    /// Access value of given entry (non-const version)
    preal& operator[] (uint i) { return (*x)(i); };

    /// Assignment operator
    const uBLASVector& operator= (const uBLASVector& x);

  private:

    // Smart pointer to uBLAS vector object
    std::shared_ptr<ublas_vector> x;

  };

}

#endif
