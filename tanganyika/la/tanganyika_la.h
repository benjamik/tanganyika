#ifndef __DOLFIN_LA_H
#define __DOLFIN_LA_H

// DOLFIN la interface

// Note that the order is important!

#include <tanganyika/la/ublas.h>

/* #include <tanganyika/la/GenericLinearSolver.h> */
/* #include <tanganyika/la/GenericLUSolver.h> */
/* #include <tanganyika/la/GenericTensor.h> */
/* #include <tanganyika/la/GenericSparsityPattern.h> */
/* #include <tanganyika/la/GenericSparsityPattern.h> */

/* #include <tanganyika/la/uBLASFactory.h> */

#include <tanganyika/la/uBLASMatrix.h>
#include <tanganyika/la/uBLASKrylovMatrix.h>

#include <tanganyika/la/uBLASVector.h>

/* #include <tanganyika/la/LinearAlgebraFactory.h> */

#include <tanganyika/la/uBLASSparseMatrix.h>
#include <tanganyika/la/uBLASDenseMatrix.h>
#include <tanganyika/la/uBLASPreconditioner.h>
#include <tanganyika/la/uBLASKrylovSolver.h>
#include <tanganyika/la/uBLASILUPreconditioner.h>

#endif
