// Copyright (C) 2011 Benjamin Kehlet
//
// This file is part of DOLFIN.
//
// DOLFIN is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DOLFIN is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with DOLFIN.  If not, see <http://www.gnu.org/licenses/>.
//
// First added:  2011-02-16
// Last changed: 2011-09-27


#include <cfloat>
#include <vector>
#include <iostream>
#include <boost/scoped_array.hpp>
#include <tanganyika/log/LogStream.h>
#include <tanganyika/la/uBLASDenseMatrix.h>
#include "HighPrecision.h"

using namespace tanganyika;

//-----------------------------------------------------------------------------
void HighPrecision::real_mat_exp(uint n, real* E, const real* A, const uint p)
{
  // This implementation is not very fast (using Taylor
  // approximation), but should be relatively numerically stable. It
  // is simple and well suited as a reference implementation and
  // usefull with extended precision since it can iterate until the
  // required precision is obtained.  
  // TODO: Fix the faster implementation based on Padé approximation
  // with scaling/squaring

  real_identity(n, E);

  boost::scoped_array<real> a(new real[n*n]);
  real_identity(n, a.get());

  uint j = 0;
  while (real_max_abs(n*n, a.get()) > real_epsilon())
  {
    j++;

    real_mat_prod_inplace(n, a.get(), A);
    real_div(n*n, a.get(), j);

    real_add(n*n, E, a.get());
  }

//   boost::scoped_array<real> _A(new real[n*n]);
//   boost::scoped_array<real> A2(new real[n*n]);
//   boost::scoped_array<real> P(new real[n*n]);
//   boost::scoped_array<real> Q(new real[n*n]);

//   // A is const. Use _A instead
//   real_set(n*n, &_A[0], A);

//   // Calcuate Pade coefficients  (1-based instead of 0-based as in litterature)
//   boost::scoped_array<real> c(new real[p + 2]);
//   c[1] = 1.0;
//   for (uint k = 1; k <= p; ++k)
//     c[k+1] = c[k]*((p + 1.0 - k)/(k*(2*p + 1 - k)));

//   real norm = 0.0;

//   // Scaling

//   // compute infinty norm of A
//   for (uint i = 0; i < n; ++i)
//   {
//     real tmp = 0.0;
//     for(uint j = 0; j < n; j++)
//       tmp += real_abs(A[i + n*j]);
//     norm = real_max(norm, tmp);
//   }

//   uint s = 0;
//   if (norm > 0.5)
//   {
//     s = std::max(0, static_cast<int>(std::log(to_preal(norm))/std::log(2.0)) + 2);
//     real_mult(n*n, &_A[0], 1.0/real_pow(2, s));
//   }

//   // Horner evaluation of the irreducible fraction
//   real_mat_prod(n, &A2[0], &_A[0], &_A[0]);
//   real_identity(n, &Q[0], c[p + 1]);
//   real_identity(n, &P[0], c[p]);

//   bool odd = true;
//   for (uint k = p - 1; k > 0; --k)
//   {
//     if (odd)
//     {
//       //Q = Q*A2
//       real_mat_prod_inplace(n, &Q[0], &A2[0]);

//       // Q += c(k)*I
//       for (uint i = 0; i < n; i++)
//         Q[i + n*i] += c[k];
//     }
//     else
//     {
//       // P = P*A2
//       real_mat_prod_inplace(n, &P[0], &A2[0]);

//       // P += c(k)*I
//       for (uint i = 0; i < n; i++)
//         P[i + n*i] += c[k];
//     }
//     odd = !odd;
//   }

//   if (odd)
//   {
//     // Q = Q*A
//     real_mat_prod_inplace(n, &Q[0], &_A[0]);

//     // Q = Q - P
//     real_sub(n*n, &Q[0], &P[0]);
//     // E = -(I + 2*(Q\P));

//     // find Q\P
//     real_solve_mat(n, n, &Q[0], E, &P[0], real_epsilon());
//     real_mult(n*n, E, -2);

//     for (uint i = 0; i < n; ++i)
//       E[i + n*i] -= 1.0;
//   }
//   else
//   {
//     real_mat_prod_inplace(n, &P[0], &_A[0]);
//     real_sub(n*n, &Q[0], &P[0]);

//     real_solve_mat(n, n, &Q[0], E, &P[0], real_epsilon());
//     real_mult(n*n, E, 2);
//     for (uint i = 0; i < n; ++i)
//       E[i + n*i] += 1.0;
//   }

//   // Squaring
//   for(uint i = 0; i < s; ++i)
//   {
//     //use _A as temporary matrix
//     real_set(n*n, &_A[0], E);
//     real_mat_prod(n, E, &_A[0], &_A[0]);
//   }
}

//-----------------------------------------------------------------------------
void HighPrecision::real_mat_prod(uint n, real* res, const real* A, const real* B)
{
  for (uint i = 0; i < n; ++i)
  {
    for (uint j = 0; j < n; ++j)
    {
      res[i + n*j] = 0.0;
      for (uint k = 0; k < n; ++k)
        res[i+n*j] += A[i + n*k]* B[k + n*j];
    }
  }
}
//-----------------------------------------------------------------------------
void HighPrecision::real_mat_prod_inplace(uint n, real* A, const real* B)
{
  boost::scoped_array<real> tmp(new real[n*n]);
  real_set(n*n, tmp.get(), A);
  real_mat_prod(n, A, tmp.get(), B);
}
//-----------------------------------------------------------------------------
void HighPrecision::real_mat_vector_prod(uint n, real* y, const real* A,
                                         const real* x)
{
  for (uint i = 0; i < n; ++i)
  {
    y[i] = 0.0;
    for (uint j = 0; j < n; ++j)
      y[i] += A[i + n*j]*x[j];
  }
}
//-----------------------------------------------------------------------------
void HighPrecision::real_mat_pow(uint n, real* A, const real* B, uint q)
{
  // TODO : Minimize number of matrix multiplications
  real_identity(n, A);
  for (uint i = 0; i < q; ++i)
    real_mat_prod_inplace(n, A, B);
}
//-----------------------------------------------------------------------------
void HighPrecision::real_GaussSeidel(uint n, const real* A, real* x, const real* b,
		                           const real& tol)
{
  std::vector<real> prev(n);

  uint iterations = 0;

  real diff = DBL_MAX; // some big number

  while (diff > tol)
  {
    if (iterations > MAX_ITERATIONS)
      error("SOR: System seems not to converge");

    real_set(n, &prev[0], x);

    // Do SOR iteration
    for (uint i = 0; i < n; ++i)
    {
      x[i] = b[i];

      // j < i
      for (uint j = 0; j < i; ++j)
        x[i] -= (A[i + n*j] * x[j]);

      // j > i
      for (uint j = i+1; j < n; ++j)
        x[i] -= (A[i + n*j]* prev[j]);

      x[i] /= A[i + n*i];
    }

    // Check tolerance
    real_sub(n, &prev[0], x);
    diff = real_max_abs(n, &prev[0]);

    ++iterations;
  }
}
//-----------------------------------------------------------------------------
void HighPrecision::real_solve(uint n, const real* A, real* x, const real* b, const real& tol)
{
  #ifndef HAS_GMP
    uBLASDenseMatrix A_mat(n,n);
    ublas_dense_matrix& _A_mat = A_mat.mat();

    uBLASVector _x(n);
    uBLASVector _b(n);
    ublas_vector& _b_vec = _b.vec();

    for (uint i = 0; i < n; i++)
    {
      _b_vec(i) = b[i];
      for (uint j = 0; j < n; j++)
      {
        _A_mat(i,j) = A[i + j*n];
      }
    }

    A_mat.solveInPlace(_x, _b);

    ublas_vector& _x_vec = _x.vec();
    for (uint i=0; i < n; i++)
      x[i] = _x_vec(i);
  
  #else

    uBLASDenseMatrix preconditioner(n, n);
    ublas_dense_matrix& _prec = preconditioner.mat();

    // Set the preconditioner matrix
    for (uint i = 0; i < n; i++)
    {
      for (uint j = 0; j < n; j++)
        _prec(i,j) = to_preal(A[i + n*j]);
    }

    preconditioner.invert();

    // solve the system
    real_solve_precond(n, A, x, b, preconditioner, tol);  
  #endif
}
//-----------------------------------------------------------------------------
void HighPrecision::real_solve_precond(uint n,
			    const real* A,
			    real* x,
			    const real* b,
			    const uBLASDenseMatrix& precond,
			    const real& tol)
{
  std::vector<real> A_precond(n*n);
  std::vector<real> b_precond(n);

  //Compute precond*A and precond*b with extended precision
  for (uint i = 0; i < n; ++i)
  {
    b_precond[i] = 0.0;
    for (uint j = 0; j < n; ++j)
    {
      b_precond[i] += precond(i,j)*b[j];

      A_precond[i + n*j] = 0.0;
      for (uint k = 0; k < n; ++k)
        A_precond[i + n*j] += precond(i, k)* A[k + n*j];
    }
  }

  // use precond*b as initial guess
  real_set(n, x, &b_precond[0]);

  // solve the system
  real_GaussSeidel(n, &A_precond[0], x, &b_precond[0], tol);
}
//-----------------------------------------------------------------------------
void HighPrecision::real_solve_mat(uint n, uint m, const real* A,
                                   real* X, const real* B, const real& tol)
{
  uBLASDenseMatrix preconditioner(n, n);
  ublas_dense_matrix& _prec = preconditioner.mat();

  // Set the preconditioner matrix
  for (uint i = 0; i < n; i++)
  {
    for (uint j = 0; j < n; j++)
      _prec(i,j) = to_preal(A[i + n*j]);
  }

  preconditioner.invert();

  // Solve each row as a Ax=b system
  for (uint i = 0; i < m; ++i)
    real_solve_precond(n, A, &X[i*n], &B[i*n], preconditioner, tol);
}
//-----------------------------------------------------------------------------
void HighPrecision::real_invert(uint n, real* Ainv, const real* A)
{
  boost::scoped_array<real> eye(new real[n*n]);
  real_identity(n, eye.get());

  real_solve_mat(n, n, A, Ainv, eye.get(), real_epsilon());
}
