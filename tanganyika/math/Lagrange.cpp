// Copyright (C) 2003-2008 Anders Logg
//
// This file is part of DOLFIN.
//
// DOLFIN is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DOLFIN is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with DOLFIN.  If not, see <http://www.gnu.org/licenses/>.
//
// Modified by Garth N. Wells, 2006.
// Modified by Benjamin Kehlet, 2009-2011
//
// First added:  2003-06-12
// Last changed: 2011-10-27

#include <tanganyika/common/constants.h>
#include <tanganyika/common/real.h>
#include "Lagrange.h"

namespace tanganyika
{

//-----------------------------------------------------------------------------
Lagrange::Lagrange(uint q)
  :q(q), counter(0), points(q + 1, 0.0),
   instability_detected("Warning: Lagrange polynomial is not numerically stable. The degree is too high.")
{
  // Do nothing
}
//-----------------------------------------------------------------------------
Lagrange::Lagrange(const Lagrange& p)
  : q(p.q), counter(p.counter), points(p.points),
    instability_detected(p.instability_detected)
{
  if (counter == size())
    init();
}
//-----------------------------------------------------------------------------
void Lagrange::set(uint i, real x)
{
  assert(i <= q);

  points[i] = x;

  counter++;
  if (counter == size())
    init();
}
//-----------------------------------------------------------------------------
uint Lagrange::size() const
{
  return points.size();
}
//-----------------------------------------------------------------------------
uint Lagrange::degree() const
{
  return q;
}
//-----------------------------------------------------------------------------
real Lagrange::point(uint i) const
{
  assert(i < points.size());
  return points[i];
}
//-----------------------------------------------------------------------------
real Lagrange::operator() (uint i, real x)
{
  return eval(i, x);
}
//-----------------------------------------------------------------------------
// real Lagrange::eval(uint i, real x)
// {
//   assert(i < points.size());

//   real product(constants[i]);
//   for (uint j = 0; j < points.size(); j++)
//   {
//     if (j != i)
//       product *= x - points[j];
//   }

//   return product;
// }
//-----------------------------------------------------------------------------

// This implementation is slightly slower than the other, but numerically stable
// for high degree polynomials
real Lagrange::eval(uint i, real x)
{
  assert(i < points.size());

  real product(1.0);
  for (uint j = 0; j < points.size(); j++)
  {
    if (j != i)
      product *= (x - points[j])/(points[i] - points[j]);
  }

  return product;
}


//-----------------------------------------------------------------------------
real Lagrange::ddx(uint i, real x)
{
  assert(i < points.size());

  real s(0.0);
  real prod(1.0);
  bool x_equals_point = false;

  for (uint j = 0; j < points.size(); ++j)
  {
    if (j != i)
    {
      real t = x - points[j];
      if (real_abs(t) < real_epsilon())
      {
        x_equals_point = true;
      } 
      else 
      {
        s += 1/t;
        prod *= t;
      }
    }
  }

  if (x_equals_point)
    return prod*constants[i];
  else
    return prod*constants[i]*s;
}
//-----------------------------------------------------------------------------
real Lagrange::dqdx(uint i)
{
  real product = constants[i];
  for (uint j = 1; j <= q; j++)
    product *= (real) j;

  return product;
}
//-----------------------------------------------------------------------------
std::string Lagrange::str(bool verbose) const
{
  std::stringstream s;

  if (verbose)
  {
    s << str(false) << std::endl << std::endl;
    for (uint i = 0; i < points.size(); i++)
      s << "  x[" << i << "] = " << to_preal(points[i]);
  }
  else
    s << "<Lagrange polynomial of degree " << q << " with " << points.size() << " points>";

  return s.str();
}
//-----------------------------------------------------------------------------
void Lagrange::init()
{
  // Note: this will be computed when n nodal points have been set, assuming they are
  // distinct. Precomputing the constants has a downside wrt. to numerical stability, since
  // the constants will decrease as the degree increases (and for high order be less than
  // epsilon.

  constants.resize(points.size());

  // Compute constants
  for (uint i = 0; i < points.size(); i++)
  {
    real product = 1.0;
    for (uint j = 0; j < points.size(); j++)
    {
      if (j != i)
      {
        if (real_abs(points[i] - points[j]) < real_epsilon())
          error("Lagrange points not distinct");
        product *= points[i] - points[j];
      }
    }

//     if (real_abs(product) < real_epsilon())
//       instability_detected();

    constants[i] = 1.0/product;
  }
}
//-----------------------------------------------------------------------------

}
