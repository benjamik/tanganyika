// Copyright (C) 2003-2005 Anders Logg
//
// This file is part of DOLFIN.
//
// DOLFIN is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DOLFIN is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with DOLFIN.  If not, see <http://www.gnu.org/licenses/>.
//
// Modified by Benjamin Kehlet
//
// First added:  2003-06-12
// Last changed: 2011-10-27

#ifndef __LAGRANGE_H
#define __LAGRANGE_H

#include <vector>

#include <tanganyika/log/Event.h>
#include <tanganyika/common/types.h>
#include <tanganyika/common/real.h>
#include <tanganyika/common/Variable.h>

namespace tanganyika
{
  /// Lagrange polynomial (basis) with given degree q determined by n = q + 1 nodal points.
  ///
  /// Example: q = 1 (n = 2)
  ///
  ///   Lagrange p(1);
  ///   p.set(0, 0.0);
  ///   p.set(1, 1.0);
  ///
  /// It is the callers reponsibility that the points are distinct.
  ///
  /// This creates a Lagrange polynomial (actually two Lagrange polynomials):
  ///
  ///   p(0,x) = 1 - x   (one at x = 0, zero at x = 1)
  ///   p(1,x) = x       (zero at x = 0, one at x = 1)
  ///


  class Lagrange : public Variable
  {
  public:

    /// Constructor
    Lagrange(uint q);

    /// Copy constructor
    Lagrange(const Lagrange& p);

    /// Specify point
    void set(uint i, real x);

    /// Return number of points
    uint size() const;

    /// Return degree
    uint degree() const;

    /// Return point
    real point(uint i) const;

    /// Return value of polynomial i at given point x
    real operator() (uint i, real x);

    /// Return value of polynomial i at given point x
    real eval(uint i, real x);

    /// Return derivate of polynomial i at given point x
    real ddx(uint i, real x);

    /// Return derivative q (a constant) of polynomial
    real dqdx(uint i);

    /// Return informal string representation (pretty-print)
    std::string str(bool verbose) const;

  private:

    void init();

    const uint q;

    // Counts the number of time set has been called to determine when
    // init should be called
    uint counter;

    std::vector<real> points;
    std::vector<real> constants;

    Event instability_detected;
  };

}

#endif
