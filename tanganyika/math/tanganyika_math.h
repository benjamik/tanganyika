#ifndef __DOLFIN_MATH_H
#define __DOLFIN_MATH_H

// DOLFIN math

#include <tanganyika/math/basic.h>
#include <tanganyika/math/Lagrange.h>
#include <tanganyika/math/Legendre.h>

#endif
