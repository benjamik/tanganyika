// Copyright (C) 2009 Anders Logg
//
// This file is part of DOLFIN.
//
// DOLFIN is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DOLFIN is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with DOLFIN.  If not, see <http://www.gnu.org/licenses/>.
//
// First added:  2009-07-02
// Last changed: 2011-03-17

#include <fstream>
#include <cstdlib>
#include <tanganyika/log/tanganyika_log.h>
#include "GlobalParameters.h"

using namespace tanganyika;

//-----------------------------------------------------------------------------
GlobalParameters::GlobalParameters() : Parameters("tanganyika")
{
  // Set default parameter values
  *static_cast<Parameters*>(this) = default_parameters();

  // FIXME: Consider adding the default parameter sets for all
  // FIXME: classes as nested parameter sets here.

  // Search paths to parameter files in order of increasing priority
  std::vector<std::string> parameter_files;
#ifdef _WIN32
  std::string home_directory(std::getenv("USERPROFILE"));
  parameter_files.push_back(home_directory + "\\.tanganyika\\parameters.xml.gz");
  parameter_files.push_back(home_directory + "\\.tanganyika\\parameters.xml");
#else
  std::string home_directory(std::getenv("HOME"));
  parameter_files.push_back(home_directory + "/.tanganyika/parameters.xml.gz");
  parameter_files.push_back(home_directory + "/.tanganyika/parameters.xml");
#endif
  parameter_files.push_back("parameters.xml.gz");
  parameter_files.push_back("parameters.xml");

  // Try reading parameters from files
  for (uint i = 0; i < parameter_files.size(); ++i)
  {
    // Check if file exists
    std::ifstream f;
    f.open(parameter_files[i].c_str());
    if (!f.is_open())
      continue;
    f.close();
  }
}
//-----------------------------------------------------------------------------
GlobalParameters::~GlobalParameters()
{
  // Do nothing
}
//-----------------------------------------------------------------------------
// void GlobalParameters::parse(int argc, char* argv[])
// {
//   log(TRACE, "Parsing command-line arguments.");

//   // Extract DOLFIN and PETSc arguments
//   std::vector<std::string> args_tanganyika;
//   std::vector<std::string> args_petsc;
//   std::vector<std::string>* current = 0;
//   args_tanganyika.push_back(argv[0]);
//   args_petsc.push_back(argv[0]);
//   for (int i = 1; i < argc; ++i)
//   {
//     std::string arg(argv[i]);

//     if (arg.size() > 2 && arg.substr(0, 2) == "--")
//     {
//       if (arg.size() > 8 && arg.substr(0, 8) == "--petsc.")
//       {
//         current = &args_petsc;
//         current->push_back("-" + arg.substr(8));
//       }
//       else
//       {
//         current = &args_tanganyika;
//         current->push_back(arg);
//       }
//     }
//     else
//     {
//       if (current)
//         current->push_back(arg);
//       else
//         error("Illegal command-line options.");
//     }
//   }

//   // Copy to argv lists
//   char** argv_tanganyika = new char*[args_tanganyika.size()];
//   for (uint i = 0; i < args_tanganyika.size(); ++i)
//   {
//     argv_tanganyika[i] = new char[args_tanganyika[i].size() + 1];
//     sprintf(argv_tanganyika[i], "%s", args_tanganyika[i].c_str());
//   }
//   char** argv_petsc = new char*[args_petsc.size()];
//   for (uint i = 0; i < args_petsc.size(); ++i)
//   {
//     argv_petsc[i] = new char[args_petsc[i].size() + 1];
//     sprintf(argv_petsc[i], "%s", args_petsc[i].c_str());
//   }

//   // Debugging
//   const bool debug = false;
//   if (debug)
//   {
//     cout << "DOLFIN args:";
//     for (uint i = 0; i < args_tanganyika.size(); i++)
//       cout << " " << args_tanganyika[i];
//     cout << endl;
//     cout << "PETSc args: ";
//     for (uint i = 0; i < args_petsc.size(); i++)
//       cout << " " << args_petsc[i];
//     cout << endl;
//   }

//   // Parse DOLFIN and PETSc options
//   parse_tanganyika(args_tanganyika.size(), argv_tanganyika);
//   parse_petsc(args_petsc.size(), argv_petsc);

//   // Cleanup
//   for (uint i = 0; i < args_tanganyika.size(); ++i)
//     delete [] argv_tanganyika[i];
//   for (uint i = 0; i < args_petsc.size(); ++i)
//     delete [] argv_petsc[i];
//   delete [] argv_tanganyika;
//   delete [] argv_petsc;
// }
//-----------------------------------------------------------------------------
