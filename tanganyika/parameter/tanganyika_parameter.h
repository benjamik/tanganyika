#ifndef __DOLFIN_PARAMETER_H
#define __DOLFIN_PARAMETER_H

/// DOLFIN parameter interface

#include <tanganyika/parameter/Parameter.h>
#include <tanganyika/parameter/Parameters.h>
#include <tanganyika/parameter/GlobalParameters.h>

#endif
