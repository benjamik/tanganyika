#ifndef __DOLFIN_QUADRATURE_H
#define __DOLFIN_QUADRATURE_H

// DOLFIN quadrature interface

#include <tanganyika/quadrature/Quadrature.h>
#include <tanganyika/quadrature/GaussianQuadrature.h>
#include <tanganyika/quadrature/GaussQuadrature.h>
#include <tanganyika/quadrature/RadauQuadrature.h>
#include <tanganyika/quadrature/LobattoQuadrature.h>
#endif
