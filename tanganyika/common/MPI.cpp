// Copyright (C) 2007 Magnus Vikstrøm
//
// This file is part of TANGANYIKA.
//
// TANGANYIKA is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// TANGANYIKA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with TANGANYIKA. If not, see <http://www.gnu.org/licenses/>.
//
// Modified by Garth N. Wells 2007-20!2
// Modified by Anders Logg 2007-2011
// Modified by Ola Skavhaug 2008-2009
// Modified by Niclas Jansson 2009
// Modified by Joachim B Haga 2012
// Modified by Benjamin Kehlet, 2013
//
// First added:  2007-11-30
// Last changed: 2013-05-14

#include "MPI.h"
#include <numeric>
#include <tanganyika/log/tanganyika_log.h>
#include "SubSystemsManager.h"


#ifdef HAS_MPI

//-----------------------------------------------------------------------------
tanganyika::MPIInfo::MPIInfo()
{
  MPI_Info_create(&info);
}
//-----------------------------------------------------------------------------
tanganyika::MPIInfo::~MPIInfo()
{
  MPI_Info_free(&info);
}
//-----------------------------------------------------------------------------
MPI_Info& tanganyika::MPIInfo::operator*()
{
  return info;
}
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
tanganyika::MPICommunicator::MPICommunicator()
{
  MPI_Comm_dup(MPI_COMM_WORLD, &communicator);
}
//-----------------------------------------------------------------------------
tanganyika::MPICommunicator::~MPICommunicator()
{
  MPI_Comm_free(&communicator);
}
//-----------------------------------------------------------------------------
MPI_Comm& tanganyika::MPICommunicator::operator*()
{
  return communicator;
}
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
void tanganyika::MPINonblocking::wait_all()
{
  if (!reqs.empty())
  {
    boost::mpi::wait_all(reqs.begin(), reqs.end());
    reqs.clear();
  }
}
#endif

//-----------------------------------------------------------------------------
// class MPI
//-----------------------------------------------------------------------------
tanganyika::MPI::MPI()
  : m_process_number(0), m_num_processes(1), controls_mpi(false)
{
  #ifdef HAS_MPI
  std::cout << "Initializing MPI" << std::endl;

  {
    int mpi_initialized;
    MPI_Initialized(&mpi_initialized);
    if (mpi_initialized)
      return;
  }

  // Initialise MPI and take responsibility
  char **c;
  MPI_Init_thread(0, &c, MPI_THREAD_MULTIPLE, &mpi_threads_support_provided);
  controls_mpi = true;

  // get process number
  MPI_Comm_rank(MPI_COMM_WORLD, &m_process_number);

  // get number of processes
  MPI_Comm_size(MPI_COMM_WORLD, &m_num_processes);  

  // TODO: Can't print from this constructor, but save the thread support int
  // and make it available through a member function

  // const bool print_thread_support = parameters()["print_mpi_thread_support_level"];
  // if (print_thread_support)
  // {
  //   switch (provided)
  //     {
  //     case MPI_THREAD_SINGLE:
  //       printf("MPI_Init_thread level = MPI_THREAD_SINGLE\n");
  //       break;
  //     case MPI_THREAD_FUNNELED:
  //       printf("MPI_Init_thread level = MPI_THREAD_FUNNELED\n");
  //       break;
  //     case MPI_THREAD_SERIALIZED:
  //       printf("MPI_Init_thread level = MPI_THREAD_SERIALIZED\n");
  //       break;
  //     case MPI_THREAD_MULTIPLE:
  //       printf("MPI_Init_thread level = MPI_THREAD_MULTIPLE\n");
  //       break;
  //     default:
  //       printf("MPI_Init_thread level = unkown\n");
  //     }
  // }

  #endif
}
//-----------------------------------------------------------------------------
tanganyika::MPI::~MPI()
{
  // Finalize mpi  
  #ifdef HAS_MPI
  int mpi_initialized;
  MPI_Initialized(&mpi_initialized);

  // Finalise MPI if required
  if (mpi_initialized && controls_mpi)
  {
    // Check in MPI has already been finalised (possibly incorrectly by a
    // 3rd party libary). Is it hasn't, finalise as normal.
    int mpi_finalized;
    MPI_Finalized(&mpi_finalized);
    if (!mpi_finalized)
      MPI_Finalize();
    else
    {
      // Use std::cout since log system may fail because MPI has been shut down.
      std::cout << "TANGANYIKA is responsible for MPI, but it has been finalized elsewhere prematurely." << std::endl;
      std::cout << "This is usually due to a bug in a 3rd party library, and can lead to unpredictable behaviour." << std::endl;
      std::cout << "If using PyTrilinos, make sure that PyTrilinos modules are imported before the TANGANYIKA module." << std::endl;
    }

    controls_mpi = false;
  }
  #endif
}
//-----------------------------------------------------------------------------
bool tanganyika::MPI::is_broadcaster()
{
  // Always broadcast from processor number 0
  return num_processes() > 1 && process_number() == 0;
}
//-----------------------------------------------------------------------------
bool tanganyika::MPI::is_receiver()
{
  // Always receive on processors with numbers > 0
  return num_processes() > 1 && process_number() > 0;
}
#ifdef HAS_MPI
//-----------------------------------------------------------------------------
void tanganyika::MPI::barrier()
{
  MPICommunicator comm;
  MPI_Barrier(*comm);
}
//-----------------------------------------------------------------------------
std::size_t tanganyika::MPI::global_offset(std::size_t range, bool exclusive)
{
  MPICommunicator mpi_comm;
  boost::mpi::communicator comm(*mpi_comm, boost::mpi::comm_duplicate);

  // Compute inclusive or exclusive partial reduction
  std::size_t offset = boost::mpi::scan(comm, range, std::plus<std::size_t>());
  if (exclusive)
    offset -= range;

  return offset;
}
//-----------------------------------------------------------------------------
std::pair<std::size_t, std::size_t> tanganyika::MPI::local_range(std::size_t N)
{
  return local_range(process_number(), N);
}
//-----------------------------------------------------------------------------
std::pair<std::size_t, std::size_t> tanganyika::MPI::local_range(unsigned int process,
                                                             std::size_t N)
{
  return local_range(process, N, num_processes());
}
//-----------------------------------------------------------------------------
std::pair<std::size_t, std::size_t> tanganyika::MPI::local_range(unsigned int process,
                                                             std::size_t N,
                                                             unsigned int num_processes)
{
  // Compute number of items per process and remainder
  const std::size_t n = N / num_processes;
  const std::size_t r = N % num_processes;

  // Compute local range
  std::pair<std::size_t, std::size_t> range;
  if (process < r)
  {
    range.first = process*(n + 1);
    range.second = range.first + n + 1;
  }
  else
  {
    range.first = process*n + r;
    range.second = range.first + n;
  }

  return range;
}
//-----------------------------------------------------------------------------
unsigned int tanganyika::MPI::index_owner(std::size_t index, std::size_t N)
{
  assert(index < N);

  // Get number of processes
  const unsigned int _num_processes = num_processes();

  // Compute number of items per process and remainder
  const std::size_t n = N / _num_processes;
  const std::size_t r = N % _num_processes;

  // First r processes own n + 1 indices
  if (index < r * (n + 1))
    return index / (n + 1);

  // Remaining processes own n indices
  return r + (index - r * (n + 1)) / n;
}
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
#else
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
void tanganyika::MPINonblocking::wait_all()
{
  error("MPI.h",
               "call MPINonblocking::wait_all",
               "TANGANYIKA has been configured without MPI support");
}
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
void tanganyika::MPI::barrier()
{
  error("MPI.cpp",
               "call MPI::barrier",
               "Your TANGANYIKA installation has been built without MPI support");
}
//-----------------------------------------------------------------------------
std::size_t tanganyika::MPI::global_offset(std::size_t range, bool exclusive)
{
  return 0;
}
//-----------------------------------------------------------------------------
std::pair<std::size_t, std::size_t> tanganyika::MPI::local_range(std::size_t N)
{
  return std::make_pair(0, N);
}
//-----------------------------------------------------------------------------
std::pair<std::size_t, std::size_t> tanganyika::MPI::local_range(unsigned int process,
                                                             std::size_t N)
{
  if (process != 0 || num_processes() > 1)
  {
    error("MPI.cpp",
                 "access local range for process",
                 "TANGANYIKA has not been configured with MPI support");
  }
  return std::make_pair(0, N);
}
//-----------------------------------------------------------------------------
std::pair<std::size_t, std::size_t>
  tanganyika::MPI::local_range(unsigned int process, std::size_t N,
                           unsigned int num_processes)
{
  if (process != 0 || num_processes > 1)
  {
    error("MPI.cpp",
                 "access local range for process",
                 "TANGANYIKA has not been configured with MPI support");
  }
  return std::make_pair(0, N);
}
//-----------------------------------------------------------------------------
unsigned int tanganyika::MPI::index_owner(std::size_t i, std::size_t N)
{
  assert(i < N);
  return 0;
}
//-----------------------------------------------------------------------------
#endif
