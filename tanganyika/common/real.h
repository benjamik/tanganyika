// Copyright (C) 2008-2011 Anders Logg
//
// This file is part of DOLFIN.
//
// DOLFIN is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DOLFIN is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with DOLFIN.  If not, see <http://www.gnu.org/licenses/>.
//
// Modified by Benjamin Kehlet, 2008.
//
// First added:  2008-10-02
// Last changed: 2015-03-07
//
// This file provides utilities for working with variable-precision
// floating-point numbers. It defines a datatype "real" which defaults
// to preal, but may be changed to higher-precision representations,
// for example using GMP.

#ifndef __REAL_H
#define __REAL_H


#include <cmath>

#include <tanganyika/log/log.h>
#include "types.h"

namespace tanganyika
{
  // Store the epsilon value
  extern real _real_epsilon;
  extern real _real_epsilon_sqrt;

  // Set precision and initialize extended precision
  void set_precision(uint prec);

  //Store the epsilon value
  extern real _real_epsilon;
  extern bool _real_initialized;

  /// Get computed epsilon
  inline const real& real_epsilon()
  { return _real_epsilon; }

  inline const real& real_epsilon_sqrt()
  { return _real_epsilon_sqrt; }

  // Convert to preal (if necessary)
  inline preal to_preal(real x)
  {
#ifdef HAS_GMP
    return x.get_d();
#else
    return static_cast<real>(x);
#endif
  }

  // Convert to real (if necessary)
  inline real to_real(preal x)
  { return real(x); }

  inline real to_real(const std::string& x)
  {
    #ifdef HAS_GMP
      return real(x);
    #elif TANGANYIKA_USE_QUAD_PRECISION
      return strtoflt128(x.c_str(), NULL);
    #else
      std::istringstream i(x);
      real r;
      if (!(i >> r))
        error(std::string("string '") + x + std::string("' could not be converted to real"));
      return r;
    #endif
  }

  inline real to_real(const char* x)
  {
    return to_real(std::string(x));
  }

  /// Absolute value
  inline real real_abs(real x)
  { return x >= 0.0 ? x : -1.0*x; }
  
  /// Max
  inline real real_max(const real& x, const real& y)
  { return x > y ? x : y; }

  // Min
  inline real real_min(const real& x, const real& y)
  { return x < y ? x : y; }

  /// Power function
  inline real real_pow(const real& x, uint y)
  {
    #ifdef HAS_GMP
      real res;
      mpf_pow_ui(res.get_mpf_t(), x.get_mpf_t(), y);
      return res;
    #elif TANGANYIKA_USE_QUAD_PRECISION
      return powq(x, y);
    #else
      return std::pow(to_preal(x), to_preal(y));
    #endif
  }

  // Check if two floating point numbers are equal wrt. epsilon
  inline bool real_near(real a, real b, real epsilon=real_epsilon())
  {
    return real_abs(a-b) < epsilon;
  }

  // Check if two vectors are equal wrt. epsilon
  inline bool real_near(uint n, real* a, real* b, real epsilon=real_epsilon())
  {
    for (uint i = 0; i < n; i++)
    {
      if (!real_near(a[i], b[i], epsilon))
        return false;
    }
    return true;
  }


  /* /// Power function */
  /* inline real pow(const real& x, const real& y) */
  /* { */
  /*   #ifdef HAS_GMP */
  /*   error("Multiprecision pow function not implemented."); */
  /*   return 0.0; */
  /*   #else */
  /*   return std::pow(to_preal(x), to_preal(y)); */
  /*   #endif */
  /* } */

  inline int isnormal(const real& x)
  {
    #if defined HAS_GMP || defined TANGANYIKA_USE_QUAD_PRECISION
    // NOTE: Not implemented.
    // GMP has no notion of infinity or NaN
    return 1;
    #else
    return std::isnormal(to_preal(x));
    #endif
  }

  /* /// Power function (note: not full precision!) */
  /* inline real real_pow(const real& x, const real& y) */
  /* { return std::pow(to_preal(x), to_preal(y)); } */

  /// Square root
  inline real real_sqrt(const real& a)
  { return sqrt(a); }

  // Computes a^(1/n)
  inline real real_nth_root(const real& a, std::size_t n)
  {
    #ifdef HAS_GMP
    // initial guess
    real x = a * 0.5;

    real dx;
    do
    {
      dx = (a/real_pow(x, n-1)-x)/n;
      x += dx;
    } while(real_abs(dx) > real_epsilon());

    return x;

    #elif TANGANYIKA_USE_QUAD_PRECISION
    return powq(a, 1.0q/n);
    #else
    return std::pow(a, 1.0/static_cast<real>(n));
    #endif
  }

  /// Compute pi
  real real_pi();

  /// Get significand and exponent of floating point number.
  /// See description frexp() in <cmath>
  preal real_frexp(int* exp, const real& x);


  // Print vector
  void real_print_vec(uint n, const real* x);

  // Print matrix
  void real_print_mat(uint n, const real* x);


  /// Exponential function (note: not full precision!)
  /* inline real real_exp(real x) */
  /* { return to_real(exp(to_preal(x))); } */

  /* /// Logarithmic function (note: not full precision!) */
  /* inline real real_log(const real& x) */
  /* { return to_real(std::log(to_preal(x))); } */

  /// Get precision in decimal digits, useful when writing with full precision to text (ascii) files
  int real_decimal_prec();

  /// Set array to given array (copy values)
  inline void real_set(uint n, real* x, const real* y)
  {
    for (uint i = 0; i < n; i++)
      x[i] = y[i];
  }

  /// Set array to given number
  inline void real_set(uint n, real* x, const real& value)
  {
    for (uint i = 0; i < n; i++)
      x[i] = value;
  }

  /// Set array to zero
  inline void real_zero(uint n, real* x)
  {
    for (uint i = 0; i < n; i++)
      x[i] = 0.0;
  }

  /// Add array, x += y
  inline void real_add(uint n, real* x, const real* y)
  {
    for (uint i = 0; i < n; i++)
      x[i] += y[i];
  }

  /// Subtract array, x -= y
  inline void real_sub(uint n, real* x, const real* y)
  {
    for (uint i = 0; i < n; i++)
      x[i] -= y[i];
  }

  /// Add multiple of array, x += a*y (AXPY operation)
  inline void real_axpy(uint n, real* x, const real& a, const real* y)
  {
    for (uint i = 0; i < n; i++)
      x[i] += a*y[i];
  }

  /// Multiply array with given number
  inline void real_mult(uint n, real* x, const real& a)
  {
    for (uint i = 0; i < n; i++)
      x[i] *= a;
  }

  /// Divide array with given number
  inline void real_div(uint n, real* x, const real& a)
  {
    for (uint i = 0; i < n; i++)
      x[i] /= a;
  }

  /// Compute inner products of array
  inline real real_inner(uint n, const real* x, const real* y)
  {
    real sum = 0.0;
    for (uint i = 0; i < n; i++)
      sum += x[i]*y[i];
    return sum;
  }

  /// Compute maximum absolute value of array
  inline real real_max_abs(uint n, const real* x)
  {
    real _max = 0.0;
    for (uint i = 0; i < n; i++)
      _max = real_max(real_abs(x[i]), _max);
    return _max;
  }

  /// Compute Euclidean norm
  inline real real_norm(uint n, const real* x)
  {
    real sum=0.0;
    for (uint i=0; i<n; ++i)
      sum += x[i]*x[i];
    return real_sqrt(sum);
  }

  /// Set matrix A to (a multiple of) identity
  inline void real_identity(uint n, real* A, real value=1.0)
  {
    real_zero(n*n, A);
    for (uint i=0; i < n; ++i)
      A[i*n+i] = value;
  }


}
#endif
