// Copyright (C) 2008 Garth N. Wells
//
// This file is part of TANGANYIKA.
//
// TANGANYIKA is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// TANGANYIKA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with TANGANYIKA. If not, see <http://www.gnu.org/licenses/>.
//
// Modified by Anders Logg 2008-2012
// Modified by Benjamin Kehlet 2013
//
// First added:  2008-01-07
// Last changed: 2013-05-14

#include "SubSystemsManager.h"

#ifdef HAS_MPI
#define MPICH_IGNORE_CXX_SEEK 1
#include <mpi.h>
#include <iostream>
#endif

#include <tanganyika/common/constants.h>
#include <tanganyika/common/Timer.h>
#include <tanganyika/parameter/GlobalParameters.h>
#include <tanganyika/log/tanganyika_log.h>


using namespace tanganyika;

// Return singleton instance. Do NOT make the singleton a global static
// object; the method here ensures that the singleton is initialised
// before use. (google "static initialization order fiasco" for full
// explanation)

SubSystemsManager& SubSystemsManager::globals()
{
  static SubSystemsManager the_instance;
  return the_instance;
}
//-----------------------------------------------------------------------------
SubSystemsManager::SubSystemsManager()
  : params(), mpi_singleton(), 
    logger_singleton(mpi_singleton.process_number(), mpi_singleton.num_processes())
{
}
//-----------------------------------------------------------------------------
SubSystemsManager::SubSystemsManager(const SubSystemsManager& sub_sys_manager)
  : logger_singleton(0,0)
{
  error("SubSystemsManager.cpp",
               "create subsystems manager",
               "Copy constructor should not be used");
}
//-----------------------------------------------------------------------------
SubSystemsManager::~SubSystemsManager()
{
}

//-----------------------------------------------------------------------------
void SubSystemsManager::finalize()
{

}
// //-----------------------------------------------------------------------------
// bool SubSystemsManager::mpi_initialized()
// {
//   // This function not affected if MPI_Finalize has been called. It returns
//   // true if MPI_Init has been called at any point, even if MPI_Finalize has
//   // been called.

//   #ifdef HAS_MPI
//   int mpi_initialized;
//   MPI_Initialized(&mpi_initialized);
//   return mpi_initialized;
//   #else
//   // TANGANYIKA is not configured for MPI (it might be through PETSc)
//   return false;
//   #endif
// }
// //-----------------------------------------------------------------------------
// bool SubSystemsManager::mpi_finalized()
// {
//   #ifdef HAS_MPI
//   int mpi_finalized;
//   MPI_Finalized(&mpi_finalized);
//   return mpi_finalized;
//   #else
//   // TANGANYIKA is not configured for MPI (it might be through PETSc)
//   return false;
//   #endif
// }
//-----------------------------------------------------------------------------
GlobalParameters& SubSystemsManager::parameters()
{
  return params;
}
//-----------------------------------------------------------------------------
Logger& SubSystemsManager::logger()
{
  return logger_singleton;
}
//-----------------------------------------------------------------------------
tanganyika::MPI& SubSystemsManager::mpi()
{
  return mpi_singleton;
}
