#ifndef __DOLFIN_COMMON_H
#define __DOLFIN_COMMON_H

// DOLFIN common

#include <tanganyika/common/types.h>
#include <tanganyika/common/real.h>
#include <tanganyika/common/constants.h>
#include <tanganyika/common/timing.h>
#include <tanganyika/common/Array.h>
#include <tanganyika/common/IndexSet.h>
#include <tanganyika/common/Set.h>
#include <tanganyika/common/Timer.h>
#include <tanganyika/common/Variable.h>
#include <tanganyika/common/Hierarchical.h>

#endif
