// Copyright (C) 2015 Benjamin Kehlet
//
// This file is part of Tanganyika
//
// DOLFIN is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DOLFIN is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with DOLFIN.  If not, see <http://www.gnu.org/licenses/>.
//
// First added:  2015-03-11
// Last changed: 2015-03-11
//

#include "types.h"
#include "constants.h"

//--------------------------------------------------------------------
#ifdef TANGANYIKA_USE_QUAD_PRECISION
std::ostream& operator<<(std::ostream& s, const tanganyika::real& a)
{
  char buffer [DOLFIN_LINELENGTH];

  std::ostringstream ss;
  ss << "%.";
  ss << s.precision();
  if (s.flags() & std::ios::scientific)
    ss << "Qe";
  else
    ss << "Qf";

  quadmath_snprintf (buffer, DOLFIN_LINELENGTH, ss.str().c_str(), a);

  s << buffer;

  return s;
}
//--------------------------------------------------------------------
std::istream& operator>>(std::istream& s, tanganyika::real& a)
{
  std::string str;
  s >> str;
  a = strtoflt128(str.c_str(), NULL);

  return s;
}
#endif
