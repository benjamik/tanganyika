// Copyright (C) 2011 Benjamin Kehlet
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library.  If not, see <http://www.gnu.org/licenses/>.
//
// First added:  2011-08-22
// Last changed: 2011-08-22


#ifndef __THREADPOOL_H
#define __THREADPOOL_H

#include <boost/thread/mutex.hpp>
#include <boost/thread/thread.hpp>
#include <boost/ptr_container/ptr_vector.hpp>
#include <boost/ref.hpp>
#include <boost/noncopyable.hpp>

namespace tanganyika
{

  template<class T>
  class PersistentThread : public boost::noncopyable
  {
   public:
    PersistentThread(T c, uint thread_num)
      : c(c), thread_num(thread_num), stopped(false), 
        current_lock_thread(0), current_lock_coordinator(0),
        the_thread(NULL)
    {
      lock(0);
      lock(1);
      lock(2);

      the_thread = new boost::thread(boost::ref(*this));
    }

    ~PersistentThread()
    {
      stopped = true;

      // Allow thread to complete
      start_task();

      the_thread->join();

      // Not very elegant this
      // TODO: Maybe use boost::unique_lock (which is RAII)
      mutexes[0].try_lock();
      unlock(0);

      mutexes[1].try_lock();
      unlock(1);

      mutexes[2].try_lock();
      unlock(2);

      delete the_thread;
    }

    // This is the loop running on the "thread side"
    void operator()()
    {
      while (true)
      {
        lock(current_lock_thread);

        // Check if we should stop
        if (stopped) break;

        // Do task
        c();

        unlock( (current_lock_thread+2)%3);
        current_lock_thread = (current_lock_thread+1)%3;
      }
    }


    void start_task()
    {
      // Allow thread to do work
      unlock(current_lock_coordinator);
    }

    void join()
    {
      // Wait for task to complete
      lock((current_lock_coordinator+2)%3);

      // Update current lock
      current_lock_coordinator = (current_lock_coordinator+1)%3;
    }

   private :
    T c;
    uint thread_num;
    uint current_lock_thread;
    uint current_lock_coordinator;
    bool stopped;

    boost::mutex mutexes[3];

    boost::thread* the_thread;

    inline void lock  (uint lock_num) { mutexes[lock_num].lock();   }
    inline void unlock(uint lock_num) { mutexes[lock_num].unlock(); }

  };


  template <class TaskType>
  class ThreadPool
  {
   public:
    ThreadPool()
    { }

    ~ThreadPool()
    { }

    // Run each task one time
    void run_task()
    {
      // Run task on all threads
      for (typename boost::ptr_vector<PersistentThread<TaskType> >::iterator it = threads.begin();
           it != threads.end(); it++)
      {
        it->start_task();
      }

      // "Join" threads by waiting for next lock
      for (typename boost::ptr_vector<PersistentThread<TaskType> >::iterator it = threads.begin();
           it != threads.end(); it++)
      {
        it->join();
      }
    } 

    // Add a task to this threadpool. Will not take ownership
    void add_task(TaskType c)
    {
      threads.push_back(new PersistentThread<TaskType>(c, threads.size()));
    }
  private:
    boost::ptr_vector<PersistentThread<TaskType> > threads;
  };

} //end namespace tanganyika




#endif
