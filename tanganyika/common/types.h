// Copyright (C) 2008 Anders Logg
//
// This file is part of Tanganayika
//
// DOLFIN is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DOLFIN is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with DOLFIN.  If not, see <http://www.gnu.org/licenses/>.
//
// First added:  2008-04-22
// Last changed: 2015-03-11
//
// This file provides typedefs for basic number types.

#ifndef __TANGANYIKA_TYPES_H
#define __TANGANYIKA_TYPES_H

#include <complex>
#include <iostream>

#ifdef HAS_GMP
#include <gmpxx.h>

namespace tanganyika
{
  typedef mpf_class real;
  typedef double preal;
}

#elif TANGANYIKA_USE_SINGLE_PRECISION
namespace tanganyika
{
  typedef float real;
  typedef float preal;
}

#elif TANGANYIKA_USE_QUAD_PRECISION
#include <quadmath.h>

namespace tanganyika
{
  typedef __float128 real;
  typedef __float128 preal;
}
//--------------------------------------------------------------------
//namespace std
/* { */
/*   inline ::tanganyika::real abs(const ::tanganyika::real& a) */
/*   { */
/*     return fabsq(a); */
/*   } */
/* } */


namespace std
{
  //--------------------------------------------------------------------
  inline tanganyika::real pow(tanganyika::real a, tanganyika::real b)
  {
    return powq(a,b);
  }
  //--------------------------------------------------------------------
  inline tanganyika::real max(tanganyika::real a, tanganyika::real b)
  {
    return fmaxq(a,b);
  }
  //--------------------------------------------------------------------
  /* inline tanganyika::real abs(tanganyika::real a) */
  /* { */
  /*   return fabsq(a); */
  /* } */
}
//--------------------------------------------------------------------
inline tanganyika::real abs(const tanganyika::real& a)
{
  return fabsq(a);
}

//--------------------------------------------------------------------
inline tanganyika::real pow(const tanganyika::real& a, const tanganyika::real& b)
{
  return powq(a, b);
}
//--------------------------------------------------------------------
inline tanganyika::real sqrt(const tanganyika::real& a)
{
  return sqrtq(a);
}
//--------------------------------------------------------------------
std::ostream& operator<<(std::ostream& s, const tanganyika::real& a);
std::istream& operator>>(std::istream& s, tanganyika::real& a);

#else
namespace tanganyika
{
  typedef double real;
  typedef double preal;
}
#endif


namespace tanganyika
{
  // Unsigned integers
  typedef std::size_t uint;

  // Complex numbers
  typedef std::complex<preal> complex;
}

#endif
