// Copyright (C) 2008 Anders Logg
//
// This file is part of DOLFIN.
//
// DOLFIN is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DOLFIN is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with DOLFIN.  If not, see <http://www.gnu.org/licenses/>.
//
// First added:  2008-04-22
// Last changed: 2011-02-01
//
// This file provides values for common constants.

#ifndef __TANGANYIKA_CONSTANTS_H
#define __TANGANYIKA_CONSTANTS_H

#ifdef TANGANYIKA_USE_SINGLE_PRECISION
#define DOLFIN_EPS           2.0e-7
#define DOLFIN_SQRT_EPS      4.5e-3
#define DOLFIN_PI            3.141592653589793238462

#elif TANGANYIKA_USE_QUAD_PRECISION
#include <quadmath.h>
#define DOLFIN_EPS FLT128_EPSILON
#define DOLFIN_SQRT_EPS 1.3e-17Q
#define DOLFIN_PI M_PIq
#else
#define DOLFIN_EPS           3.0e-16
#define DOLFIN_SQRT_EPS      1.0e-8
#define DOLFIN_PI            3.141592653589793238462
#endif


#define DOLFIN_LINELENGTH    256
#define DOLFIN_TERM_WIDTH    80

#endif
