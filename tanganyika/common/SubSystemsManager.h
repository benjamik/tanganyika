// Copyright (C) 2008-2011 Garth N. Wells
//
// This file is part of TANGANYIKA.
//
// TANGANYIKA is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// TANGANYIKA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with TANGANYIKA. If not, see <http://www.gnu.org/licenses/>.
//
// Modified by Benjamin Kehlet, 2013
//
// First added:  2008-01-07
// Last changed: 2013-05-14

#ifndef __SUB_SYSTEMS_MANAGER_H
#define __SUB_SYSTEMS_MANAGER_H
#include <tanganyika/parameter/GlobalParameters.h>
#include <tanganyika/log/Logger.h>
#include <tanganyika/common/MPI.h>

namespace tanganyika
{

  /// This is a singleton class which manages the initialisation and
  /// finalisation of various sub systems, such as MPI and PETSc.

  class SubSystemsManager
  {
  public:

    /// Finalize subsytems. This will be called by the destructor, but in
    /// special cases it may be necessary to call finalize() explicitly.
    static void finalize();

    /// Return global parameters
    GlobalParameters& parameters();

    /// Return global Logger object
    Logger &logger();

    /// Return global MPI object
    tanganyika::MPI &mpi();

    // Singleton instance
    static SubSystemsManager& globals();

  private:

    // Constructor (private)
    SubSystemsManager();

    // Copy constructor (private)
    SubSystemsManager(const SubSystemsManager& sub_sys_manager);

    // Destructor
    ~SubSystemsManager();

    GlobalParameters params;
    MPI mpi_singleton;
    Logger logger_singleton;
  };

}

#endif
