
#ifndef __TANGANYIKA_H
#define __TANGANYIKA_H

// TANGANYIKA interface

#include <tanganyika/common/tanganyika_common.h>
#include <tanganyika/parameter/tanganyika_parameter.h>
#include <tanganyika/log/tanganyika_log.h>
#include <tanganyika/la/tanganyika_la.h>
#include <tanganyika/math/tanganyika_math.h>
#include <tanganyika/quadrature/tanganyika_quadrature.h>
#include <tanganyika/ode/tanganyika_ode.h>
#include <tanganyika/io/PythonFile.h>

#endif
