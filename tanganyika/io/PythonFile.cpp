// Copyright (C) 2003-2005 Johan Hoffman and Anders Logg
//
// This file is part of DOLFIN.
//
// DOLFIN is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DOLFIN is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with DOLFIN.  If not, see <http://www.gnu.org/licenses/>.
//
// Modified by Garth N. Wells 2005, 2010.
// Modified by Rolv E. Bredesen 2008.
// Modified by Benjamin Kehlet 2009.

// First added:  2003-05-06
// Last changed: 2011-09-08

#include <fstream>
#include <iostream>
#include <iomanip>
#include <tanganyika/log/tanganyika_log.h>
#include <tanganyika/ode/Sample.h>
#include "PythonFile.h"

using namespace tanganyika;

//-----------------------------------------------------------------------------
PythonFile::PythonFile(const std::string filename) : 
  filename(filename), counter(0), write_r_and_k_files(false)
{
  std::string prefix = filename.substr(0, filename.rfind("."));
  filename_u = prefix + "_u.data";
  filename_k = prefix + "_k.data";
  filename_r = prefix + "_r.data";
}
//-----------------------------------------------------------------------------
PythonFile::~PythonFile()
{
  // Write the Python file
  std::ofstream fp(filename.c_str());
  if (!fp.is_open())
    error("Unable to open file %s", filename.c_str());

  fp << "import numpy as np" << std::endl;
  fp << "import os.path" << std::endl << std::endl;
  fp << "t = np.loadtxt('" << filename_u << "', usecols=(0,))" << std::endl;
  fp << "u = np.loadtxt('" << filename_u << "')[:, 1:]" << std::endl << std::endl;

  if (write_r_and_k_files)
  {
    fp << "if os.path.isfile('" << filename_k << "') :" << std::endl;
    fp << "  k = np.loadtxt('" << filename_k << "')[:, 1:]" << std::endl << std::endl;

    fp << "if os.path.isfile('" << filename_r << "') :" << std::endl;
    fp << "  r = np.loadtxt('" << filename_r << "')[:, 1:]" << std::endl << std::endl;
  }

  fp.close();
}
//-----------------------------------------------------------------------------
void PythonFile::operator<<(const Sample& sample)
{
  
  std::ios_base::openmode filemode = counter > 0 ?
    std::ios_base::out| std::ios_base::app : std::ios_base::out | std::ios_base::trunc;

  //get precision
  int prec = real_decimal_prec();

  std::ofstream fp_u(filename_u.c_str(), filemode);
  if (!fp_u.is_open())
    error("Unable to open file %s", filename_u.c_str());

  std::ofstream fp_k(filename_k.c_str(), filemode);
  if (!fp_k.is_open())
    error("Unable to open file %s", filename_k.c_str());

  std::ofstream fp_r(filename_r.c_str(), filemode);
  if (!fp_r.is_open())
    error("Unable to open file %s", filename_r.c_str());

  // Save solution
  fp_u << std::setprecision(prec) << sample.t() << " ";
  for (uint i = 0; i < sample.size(); i++)
    fp_u << std::setprecision(prec) << sample.u(i) << " ";
  fp_u << std::endl;

  // Save time steps
  fp_k << std::setprecision(prec) << sample.t() << " ";
  for (uint i = 0; i < sample.size(); i++)
    fp_k << std::setprecision(prec) << sample.k(i) << " ";
  fp_k << std::endl;

  // Save residuals
  fp_r << std::setprecision(prec) << sample.t() << " ";
  for (uint i = 0; i < sample.size(); i++)
    fp_r << std::setprecision(prec) << sample.r(i) << " ";
  fp_r << std::endl;

  // Increase frame counter
  counter++;

  last_t = sample.t();
}
//-----------------------------------------------------------------------------
void PythonFile::write(real t, const Array<real>& u)
{
  std::ios_base::openmode filemode = counter > 0 ?
    std::ios_base::out| std::ios_base::app : std::ios_base::out | std::ios_base::trunc;

  // Get precision
  const int prec = real_decimal_prec();

  // Open sub files
  std::ofstream fp_u(filename_u.c_str(), filemode);
  if (!fp_u.is_open())
    error("Unable to open file %s", filename_u.c_str());

  // Save solution
  fp_u << std::setprecision(prec) << t << " ";
  for (uint i=0; i < u.size(); ++i)
    fp_u << std::setprecision(prec) << u[i] << " ";
  fp_u << std::endl;

  last_t = t;
  counter++;
}
//-----------------------------------------------------------------------------
void PythonFile::operator<<(const std::pair<real, RealArrayRef> sample)
{
  write(sample.first, sample.second);
}
//-----------------------------------------------------------------------------
void PythonFile::operator<<(const std::pair<real, real> sample)
{
  const real& t = sample.first;
  const real& u = sample.second;
  
  std::ios_base::openmode filemode = counter > 0 ?
    std::ios_base::out| std::ios_base::app : std::ios_base::out | std::ios_base::trunc;

  // Get precision
  const int prec = real_decimal_prec();

  // Open sub files
  std::ofstream fp_u(filename_u.c_str(), filemode);
  if (!fp_u.is_open())
    error("Unable to open file %s", filename_u.c_str());

  // Save time
  fp_u << std::setprecision(prec) << t << " ";

  // Save solution
  fp_u << std::setprecision(prec) << u << std::endl;

  last_t = t;
  counter++;
}
//-----------------------------------------------------------------------------
tanganyika::real PythonFile::last() const
{
  return last_t;
}
