// Copyright (C) 2003-2005 Johan Hoffman and Anders Logg
//
// This file is part of DOLFIN.
//
// DOLFIN is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DOLFIN is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with DOLFIN.  If not, see <http://www.gnu.org/licenses/>.
//
// Modified by Benjamin Kehlet
//
// First added:  2003-07-15
// Last changed: 2011-09-22

#ifndef __PYTHON_FILE_H
#define __PYTHON_FILE_H

#include <string>
#include <tanganyika/common/types.h>
#include <tanganyika/common/real.h>
#include <tanganyika/common/Array.h>
#include <boost/ref.hpp>


namespace tanganyika
{
  typedef boost::reference_wrapper<Array<real> > RealArrayRef;

  class Sample;

  // Represents input/output of data in a format readable by Python
  // (Numpy). The data is written to several files (the variable
  // name is appended to the base file name) to enable incremental
  // output in an efficient way.

  class PythonFile
  {
  public:

    PythonFile(const std::string filename);
    virtual ~PythonFile();

    // Write sample to file
    void operator<< (const Sample& sample);

    // Write array of values to file
    void operator<< (const std::pair<real, RealArrayRef>);

    // Write single value to file
    void operator<< (const std::pair<real, real>);

    void write(real t, const Array<real> &data);

    // Get last time of last sample written 
    // (usefull to avoid duplicated samples at the end)
    real last() const;

  private :
    //uint size;
    uint counter;
    std::string filename, filename_u, filename_k, filename_r;

    bool write_r_and_k_files;
    real last_t;
  };

}

#endif
